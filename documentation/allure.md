<!---
SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

# Allure

All deployed allure results are being uploaded to the [openDesk TestOps](https://testops.opendesk.run/project/1/) page.

## Environment Variables

To distinguish different launches, we use a set of environment variables.
| Environment Variable | Key | Value |
| --- | --- | --- |
| Environment | TESTOPS_ENV | Current environment \<portal-url\> |
| Testset | TESTOPS_TESTSET | Testset [Smoke/Regression] |
| Testprofile | TESTOPS_TESTPROFILE | Testprofile [Default/2FA/Custom] |
| Branch | TESTOPS_BRANCH | Branch name |

## Epic

We are using epic for a grouping of tests into their testsets.
A list of all currently used epics:

| Epic name | Usage |
| --- | --- |
| Smoke | Testing the functionality of the applications and the basic integration. |
| Regression | Deeper testing of all functionalities. |

## Feature

We are using features for grouping test cases that use the same page.
<strong>Highlighted</strong> features have sub-features ("- \<sub-feature\>"), that can only be assigned when the parent-feature is also assigned.
A list of all currently used features:

| Feature name | Usage |
| --- | --- |
| Login | All tests on the login page. |
| Portal | All tests on the portal page. |
| Portal-Admin | All tests on the portal page. |
| Users | All tests on the users page. |
| Functional accounts | All tests on the functional accounts page. |
| <strong>OX</strong> | All tests for Open-Xchange. |
| - Mail | All tests for mail |
| - Calendar | All tests for calendar |
| Nextcloud | All tests for Nextcloud. |
| OpenProject | All tests for OpenProject. |
| XWiki | All tests for XWiki. |
| Element | All tests for Element. |
| Jitsi | All tests for Jitsi. |
| Service | All tests for Service. |
| Opentalk | All tests for Opentalk. |

Note: Password reset is part of login.

## Story

We are using stories for grouping test cases in specific functionalities.
When a feature is defined, that story can only be used when that feature is also assigned.
A list of all currently used stories:

| Feature | Story name | Usage |
| --- | --- | --- |
| <strong>-</strong> | | |
| | User | When using a normal user |
| | Admin | When using an admin user |
| | Guest | When using an unprivileged user |
| | UI | When testing UI rendering |
| | Security | When testing security features (*1) |
| | Profile | When the test case can be toggled by a profile-setting |
| | Upload | When uploading a file |
| | Download | When downloading a file |
| <strong>Login</strong> | | |
| | Login | When logging in. |
| | Password Reset | When (re-)setting a password |
| <strong>Users</strong> | | |
| | Create user | When creating a new user |
| <strong>Functional accounts</strong> | | |
| | Create functional account | When creating a new functional account |
| <strong>Nextcloud</strong> | | |
| | Create file | When creating a new file |
| | File share | When sharing a file |
| <strong>Mail</strong> | | |
| | Look for mail | When looking for a mail |
| | Read mail | When reading a mail |
| | Write mail | When writing a mail |
| | Functional accounts | Mail-tests that require a functional account |
| <strong>Calendar</strong> | | |
| | Create appointment | When creating a new appointment |
| <strong>OpenProject</strong> | | |
| | File Storage | When adding, removing or modifying file storage |
| <strong>XWiki</strong> | | |
| | Page | When opening a page |
| | Create page | When creating a new page |
| | Edit page | When editing a page |
| <strong>Element</strong> | | |
| | Security key | When creating, modifying or deleting the security key |
| | Room | When creating, using or deleting a room |
| <strong>Jitsi</strong> | | |
| | Meeting | When creating, joining or leaving a meeting |
| <strong>Service</strong> | | |

(*1): Examples: New device notification, no access to apps without permission.

