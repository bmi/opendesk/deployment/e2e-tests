# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

#!/bin/bash

# To not pollute the local system, virtualenv is harnessed.
if ! command -v pip &> /dev/null; then
  echo "pip of Python3 needs to be installed. You can easily install it:"
  echo -e "\nsudo apt-get update && sudo apt-get install -y python3-pip\n"
  exit 1
fi

if ! command -v virtualenv &> /dev/null; then
  echo -e "\ninstalling virtualenv for installation of required packages\n"
  pip install --user virtualenv
fi

VIRTUALENV=$(command -v virtualenv)
VENV="venv"
${VIRTUALENV} "${VENV}"
source "${VENV}/bin/activate"
pip install -r requirements.txt
playwright install
playwright install-deps
pip install -r requirements.txt

git clone "https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import.git" "git_clone/user-import"