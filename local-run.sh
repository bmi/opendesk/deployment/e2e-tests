# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

#!/bin/bash

if ! command -v virtualenv &> /dev/null; then
  echo -e "\nvirtualenv is required for local installations.\n"
  echo -e "Just run the install-local script before this run script.\n"
  exit 1
fi

VIRTUALENV=$(command -v virtualenv)
VENV="venv"
${VIRTUALENV} "${VENV}"
source "${VENV}/bin/activate"

# Load user-import repository
export user_importer_path="git_clone/user-import"

# use this for debugging:
#pytest -s tests/ --headed --config config.ini -v "$@"
# Param n for parallel execution.
pytest -n 0 --order-dependencies --dist=loadgroup "." --config config.ini --alluredir=allure_result "$@"
