<!---
SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

# List of E2E tests 

This is the complete list of E2E tests that we have for the SouvAP deployments.

## In this repo

The tests in this repo are currently running in the [pipeline](http://gitlab.souvap-univention.de/ssouvap/tests/end-2-end-testset/-/pipelines).

### External app tiles

The following tests start with a logged-in user who is currently on the portal home page.

| Scenario                                                                 | Test                                                                        |
|--------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| Clicking on **Mail** tile opens OX Mail in a new tab                     | `souvap_tests/tests/test_external_app_tiles.py::test_mail_tile`             |
| Clicking on **Calendar** tile opens OX Calendar in a new tab             | `souvap_tests/tests/test_external_app_tiles.py::test_calendar_tile`         |
| Clicking on **Contacts** tile opens OX Contacts in a new tab             | `souvap_tests/tests/test_external_app_tiles.py::test_contacts_tile`         |
| Clicking on **Tasks** tile opens OX Tasks in a new tab                   | `souvap_tests/tests/test_external_app_tiles.py::test_tasks_tile`            |
| Clicking on **Video Conference** tile opens Jitsi in a new tab           | `souvap_tests/tests/test_external_app_tiles.py::test_video_conference_tile` |
| Clicking on **Projects** tile opens OpenProject in a new tab             | `souvap_tests/tests/test_external_app_tiles.py::test_projects_tile`         |
| Clicking on **Files** tile opens Nextcloud in a new tab                  | `souvap_tests/tests/test_external_app_tiles.py::test_files_tile`            |
| Clicking on **Activity** tile opens Nextcloud Activity page in a new tab | `souvap_tests/tests/test_external_app_tiles.py::test_activity_tile`         |


### Nextcloud

The following tests start with a logged-in user who is currently on the Nextcloud home page, unless otherwise mentioned.

| Scenario                                                                                                            | Test                                                                                   |
|---------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------|
| User can create a new folder                                                                                        | `souvap_tests/tests/test_files.py::test_create_folder`                                 |
| User can upload a profile picture. This test starts from the Nextcloud personal info settings page.                 | `souvap_tests/tests/test_files.py::test_personal_info_settings_upload_profile_picture` |
| User can search for apps using the search button in Nextcloud's header. Currently searches for "Talk" and "Office". | `souvap_tests/tests/test_files.py::test_search`                                        |

#### Nextcloud app menu

| Scenario                                                                                                               | Test                                                                        |
|------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| User can navigate to the **OX Calendar** using the app menu in Nextcloud's header.                                     | `souvap_tests/tests/test_files.py::test_files_navigate_to_calendar`         |
| User can navigate to the **OX Contacts** using the app menu in Nextcloud's header.                                     | `souvap_tests/tests/test_files.py::test_files_navigate_to_contacts`         |
| User can navigate to **Collabora** using the app menu in Nextcloud's header (opens in new tab).                        | `souvap_tests/tests/test_files.py::test_files_navigate_to_collaboration`    |
| User can navigate to **Nextcloud** using the app menu in Nextcloud's header (opens in the same page).                  | `souvap_tests/tests/test_files.py::test_files_navigate_to_files`            |
| User can navigate to **XWiki** using the app menu in Nextcloud's header (opens in new tab).                            | `souvap_tests/tests/test_files.py::test_files_navigate_to_knowledge`        |
| User can navigate to **OX Mail** using the app menu in Nextcloud's header (opens in new tab).                          | `souvap_tests/tests/test_files.py::test_files_navigate_to_mail`             |
| User can navigate to **OpenProject** using the app menu in Nextcloud's header (opens in new tab).                      | `souvap_tests/tests/test_files.py::test_files_navigate_to_projects`         |
| User can navigate to **OX Tasks** using the app menu in Nextcloud's header (opens in new tab).                         | `souvap_tests/tests/test_files.py::test_files_navigate_to_tasks`            |
| User can navigate to the **Nextcloud Activity** page using the app menu in Nextcloud's header (opens in the same tab). | `souvap_tests/tests/test_files.py::test_files_navigate_to_activity`         |
| User can navigate to **Jitsi** using the app menu in Nextcloud's header (opens in new tab)                             | `souvap_tests/tests/test_files.py::test_files_navigate_to_video_conference` |

### OX Mail

The following tests start with a logged-in user who is on the OX Mail page.

| Scenario                           | Test                             |
|------------------------------------|----------------------------------|
| User can send a mail to themselves | `souvap_tests/tests/test_ox.py`  | 

## In the [`univention-portal`](https://git.knut.univention.de/univention/components/univention-portal) repo

These tests validate the univention portal, and does not depend on external apps like OX, Nextcloud etc.

Here is the [list of tests](https://git.knut.univention.de/univention/components/univention-portal/-/blob/develop/tests/e2e/README.md). Everything in that
list is running on the [pipeline](http://gitlab.souvap-univention.de/ssouvap/tests/end-2-end-testset/-/pipelines), expect the scenario "Non-admin user can change password".

## In the [`keycloak-extensions`](https://git.knut.univention.de/univention/components/keycloak-extensions) repo.

These tests check login brute-force prevention.

Here is the [list of tests](https://git.knut.univention.de/univention/components/keycloak-extensions/-/blob/development/tests/e2e/README.md). They are currently **not running** in the pipeline.



