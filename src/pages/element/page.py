# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
from pages.base.base import expect
from pages.base.base import BasePage
from .page_parts import *

class BaseElementPage(BasePage):
    
    def __init__(self, page):
        super().__init__(page)
    
    def open_page(self, url_portal: str):
        self.page.goto(url_portal.replace("portal.", "chat."))
    
    def validate(self):
        expect(self.page.locator("head > meta[name=\"application-name\"]")).to_be_attached()
        expect(self.page.locator("head > meta[name=\"application-name\"]")).to_have_attribute("content", "Element")

class ElementLoginPage(BaseElementPage):
    
    def __init__(self, page):
        super().__init__(page)
        
        self.login_content: Locator = self.page.locator(".mx_AuthPage > .mx_AuthPage_modal > .mx_AuthPage_modalContent")
        self.login_body: Locator = self.login_content.locator("main.mx_AuthBody")
        self.login_oidc_button: Locator = self.login_body.locator(".mx_SSOButtons > .mx_AccessibleButton.mx_SSOButton")
        self.verify_with_key_button: Locator = self.login_content.locator(".mx_CompleteSecurity_actionRow > [role=\"button\"]").nth(1)
        self.security_key_input: Locator = self.page.locator("#mx_securityKey")
        self.security_key_done_button: Locator = self.login_content.locator(".mx_CompleteSecurity_actionRow > [role=\"button\"]").nth(0)
        self.primary_button: Locator = self.page.locator(".mx_Dialog_buttons").locator(".mx_Dialog_primary")

class ElementPage(BaseElementPage):
    
    def open_home_page(self, url_portal: str):
        url: str = url_portal.replace("portal.", "chat.")
        if not url.endswith("/"): url += "/"
        url += "#/home"
        self.page.goto(url)
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.container: Locator = self.page.locator("#matrixchat")
        self.wrapper: Locator = self.container.locator("> div > .mx_MatrixChat_wrapper")
        self.static_dialog_wrapper: Locator = self.page.locator("#mx_Dialog_StaticContainer > .mx_Dialog_wrapper:visible")
        self.dialog_wrapper: Locator = self.page.locator("#mx_Dialog_Container > .mx_Dialog_wrapper:visible")
        
        self.navbar: Navbar = Navbar(self.container.locator("> nav"))
        self.user_menu: UserMenu = UserMenu(self.page.locator("#mx_ContextualMenu_Container"))
        
        self.user_settings_dialog: UserSettingsDialog = UserSettingsDialog(self.static_dialog_wrapper.locator(".mx_UserSettingsDialog"))
        self.confirm_backup_deletion_dialog: ConfirmBackupDeletionDialog = ConfirmBackupDeletionDialog(self.dialog_wrapper.locator(".mx_QuestionDialog"))

        self.create_secret_dialog: CreateSecretDialog = CreateSecretDialog(self.static_dialog_wrapper.locator(".mx_CreateSecretStorageDialog"))
        self.secret_created_success_dialog: SecretCreatedSuccessDialog = SecretCreatedSuccessDialog(self.static_dialog_wrapper.locator(".mx_CreateSecretStorageDialog.mx_SuccessDialog"))
        
        self.schedule_meeting_dialog: ScheduleMeetingDialog = ScheduleMeetingDialog(self.static_dialog_wrapper.locator(".mx_ModalWidgetDialog"))
        
        self.left_panel: Locator = self.wrapper.locator(".mx_MatrixChat > .mx_LeftPanel_outerWrapper")
        self.space_panel: SpacePanel = SpacePanel(self.left_panel.locator(".mx_SpacePanel"))
        self.room_list: RoomList = RoomList(self.left_panel.locator(".mx_LeftPanel_roomListContainer"))
        self.current_room: CurrentRoom = CurrentRoom(self.wrapper.locator(".mx_MatrixChat > .mx_RoomView_wrapper"))
        self.invite_user_dialog: InviteUserDialog = InviteUserDialog(self.static_dialog_wrapper.locator(".mx_InviteDialog_content"))
        self.new_room_dialog: NewRoomDialog = NewRoomDialog(self.dialog_wrapper.locator(".mx_CreateRoomDialog"))
        self.spotlight_dialog: SpotlightDialog = SpotlightDialog(self.static_dialog_wrapper.locator(".mx_SpotlightDialog"))
        self.request_room_access_dialog: RequestRoomAccessDialog = RequestRoomAccessDialog(self.dialog_wrapper.locator(".mx_CompoundDialog"))
        self.meeting_scheduler: MeetingScheduler = MeetingScheduler(self.page.frame_locator("#mx_PersistedElement_container > :not([id$=jitsi][id$=neoboard]) iframe"))
        self.conference_room: ConferenceRoom = ConferenceRoom(self.page.frame_locator("#mx_PersistedElement_container > [id$=jitsi] iframe"))
        
        self.error_dialog: ErrorDialog = ErrorDialog(self.dialog_wrapper.locator(".mx_ErrorDialog"))
        self.toast_container: Locator = self.page.locator(".mx_ToastContainer")
        self.current_toast: Toast = Toast(self.toast_container.locator("> .mx_Toast_toast"))
        self.session_lock_theft: Locator = self.page.locator(".mx_ConfirmSessionLockTheftView > .mx_ConfirmSessionLockTheftView_body")
        self.session_lock_theft_confirm_button: Locator = self.session_lock_theft.locator("> .mx_AccessibleButton_kind_primary")
    
    def validate(self) -> None:
        super().validate()
        expect(self.container).to_be_visible(timeout=1000*60*2)
        
        try:
            expect(self.session_lock_theft_confirm_button).to_be_visible(timeout=5000)
            self.session_lock_theft_confirm_button.click()
        except: pass
        
        expect(self.wrapper).to_be_visible(timeout=1000*30)
        
        # Removing toast from DOM
        try:
            expect(self.toast_container).to_be_attached(timeout=5000)
            self.toast_container.evaluate("(container) => container.remove()")
        except:
            pass
        
