# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Locator
from pages.base.base import BasePagePart

class Modal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.title_bar: Locator = self.locator("> [data-dojo-attach-point=\"titleBar\"]")
        self.container_node: Locator = self.locator("> [data-dojo-attach-point=\"containerNode\"]")

class NewUserModal(Modal):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.primary_button: Locator = self.container_node.locator(".umcPageFooterRight > .ucsButton.ucsPrimaryButton.dijitButton:visible")
        
        self.firstname_field: Locator = self.container_node.locator("[name=firstname]")
        self.lastname_field: Locator = self.container_node.locator("[name=lastname]")
        self.username_field: Locator = self.container_node.locator("[name=username]")
        self.email: Locator = self.container_node.locator("[name=mailPrimaryAddress]")
        self.more_button: Locator = self.container_node.locator(".umcPageFooterRight > .ucsButton.dijitButton:visible", has_text=self._("new_user_advanced_button_text"))
        
        self.mail_field: Locator = self.container_node.locator("[name=PasswordRecoveryEmail]")
        
        self.success_notification_text: Locator = self.locator(".umcText.udmNewObjectDialog__successNotification")

class AddObjectModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.property_select_hidden: Locator = self.locator("[name=objectProperty]")
        self.property_select: Locator = self.locator(".dijitReset.dijitInputField.dijitInputContainer").filter(has=self.page.locator("[name=objectProperty]")).locator("input.dijitReset.dijitInputInner")
        self.search_field: Locator = self.locator("[name=objectPropertyValue]")
        self.table: Locator = self.locator("table.dojoxGridRowTable")
        self.add_button: Locator = self.locator("div.umcContainerWidget.umcDialogActionBarRight").filter(has=self.page.locator("svg[data-iconName=check]"))
    
    def get_property_widget(self):
        id: str = self.property_select.get_attribute("id")
        return self.page.locator(f"#widget_{id}")
    
    def get_down_arrow(self):
        return self.get_property_widget().locator("> span.ucsSimpleIconButton.ucsIconButton.umcTextBox__downArrowButton")
    
    def get_property_options(self):
        id: str = self.property_select.get_attribute("id")
        return self.page.locator(f"[dijitpopupparent={id}]")
    
    def get_add_object_table_entry(self, name: str) -> Locator:
        return self.table.locator("tr").filter(has=self.page.locator("td", has_text=name))
    
    def get_object_table_entry(self, name: str):
        return self.table_contents.locator("> div[role=row]").filter(has=self.page.locator("td > div.umcGridDefaultAction", has_text=name))