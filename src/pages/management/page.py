# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import FrameLocator, Page, Locator
from pages.base.base import BasePage, expect
from .modal import *

class ManagmentPage(BasePage):

    def __init__(self, page):
        super().__init__(page)
        self.categories_container: Locator = self.page.locator(".umcContainerWidget.umcCategoryBar")
        self.favorites_category: Locator = self.categories_container.locator("> .umcCategoryButton.umcCategory-_favorites_")
        self.users_category: Locator = self.categories_container.locator("> .umcCategoryButton.umcCategory-users")
        self.devices_category: Locator = self.categories_container.locator("> .umcCategoryButton.umcCategory-devices")
        self.domain_category: Locator = self.categories_container.locator("> .umcCategoryButton.umcCategory-domain")

    def get_title(self):
        return "Portal - openDesk"

    def get_url(self, url_portal: str):

        url: str = url_portal
        if not url.endswith("/"):
            url += "/"
        url += "univention/management/"

        return url

    def open_page(self, url_portal: str):
        self.page.goto(self.get_url(url_portal))

class ManagmentListPage(ManagmentPage):

    def __init__(self, page):
        super().__init__(page)
        self.module_title: Locator = self.page.locator("[dojoattachpoint=\"contentNode\"].umcModuleTitle")
        self.search_field: Locator = self.page.locator("[name=objectPropertyValue]")
        self.toggle_filter_button: Locator = self.page.locator("[data-iconName=filter]")
        self.list: Locator = self.page.locator(".umcGrid")
        self.list_header: Locator = self.list.locator(".umcGridHeader")
        self.add_button: Locator = self.list_header.locator(".dijitButton:visible").filter(has=self.page.locator("[data-iconName=plus]"))
        self.edit_button: Locator = self.list_header.locator(".dijitButton:visible").filter(has=self.page.locator("[data-iconName=edit-2]"))
        self.delete_button: Locator = self.list_header.locator(".dijitButton:visible").filter(has=self.page.locator("[data-iconName=trash]"))
        self.more_button: Locator = self.list_header.locator(".dijitButton:visible").filter(has=self.page.locator("[data-iconName=more-horizontal]"))

        self.table_contents: Locator = self.list.locator("div.dgrid-scroller > div.dgrid-content.ui-widget-content")
        
        self.current_dialog: Locator = self.page.locator(".umcUdmNewObjectDialog.umcLargeDialog.dijitDialog:visible")

    def get_object_table_entry(self, name: str):
        return self.table_contents.locator("> div[role=row]").filter(has=self.page.locator("td > div.umcGridDefaultAction", has_text=name))

    def validate(self):
        super().validate()
        expect(self.module_title).to_be_visible(timeout=30000)
        expect(self.list).to_be_visible(timeout=30000)

class UsersPage(ManagmentListPage):

    def __init__(self, page: Page) -> None:
        super().__init__(page)

        self.new_user_modal: NewUserModal = NewUserModal(self.current_dialog)

    def get_url(self, url_portal):
        return super().get_url(url_portal) + "?menu=false#module=udm:users/user"

class EditUserPage(BasePage):

    def __init__(self, page):
        super().__init__(page)

        self.change_password_on_next_login_checkbox: Locator = self.page.locator("[name=pwdChangeNextLogin]")
        self.new_password_fields: Locator = self.page.locator("input[autocomplete=new-password]:visible")
        self.save_button: Locator = self.page.locator(".dijit.dijitReset.dijitInline.ucsButton.ucsPrimaryButton.dijitButton:visible").filter(has=page.locator("[data-iconName=save]"))

    def get_edit_user_account_tab(self):
        return self.page.locator(".dijitTabContent > .tabLabel:visible", has_text=self._("edit_user_account_tab"))

    def get_edit_user_opendesk_tab(self):
        return self.page.locator(".dijitTabContent > .tabLabel:visible", has_text=self._("edit_user_opendesk_tab"))

    def get_all_checkboxes(self):
        return self.page.locator(".umcPageMainContent").get_by_role("checkbox")

class AnnouncementsPage(ManagmentListPage):

    def __init__(self, page: Page) -> None:
        super().__init__(page)

    def get_url(self, url_portal):
        return super().get_url(url_portal) + "?menu=false#module=udm:portals/announcement"

class EditAnnouncementPage(BasePage):
    
    def __init__(self, page):
        super().__init__(page)
        self.main_content: Locator = self.page.locator("form .umcPageMainContent")
        
        self.general: Locator = self.main_content.locator("> .umcTitlePane").nth(0)
        self.internal_name_input: Locator = self.general.locator(".dijitInputInner[name=\"name\"]")
        
        self.content: Locator = self.main_content.locator("> .umcTitlePane").nth(1)
        self.title_content: Locator = self.content.locator(".umcLayoutContainer > .umcLayoutRow").nth(0)
        self.title_input_wrapper: Locator = self.title_content.locator(".umcLabelPaneContainerNode:not(.dijitDisplayNone) > .umcMultiInput > .umcMultiInputContainer")
        self.first_title_language_input: Locator = self.title_input_wrapper.first.locator("> .umcLabelPane").nth(0).locator("input.dijitInputInner")
        self.first_title_display_name_input: Locator = self.title_input_wrapper.first.locator("> .umcLabelPane").nth(1).locator("input.dijitInputInner")
        self.first_title_delete_button: Locator = self.title_input_wrapper.first.locator("> .umcLabelPane").nth(2)
        
        self.message_content: Locator = self.content.locator(".umcLayoutContainer > .umcLayoutRow").nth(1)
        
        self.options: Locator = self.main_content.locator("> .umcTitlePane").nth(3)
        self.groups_multi_select: Locator = self.options.locator(".umcMultiObjectSelect")
        self.groups_add_button: Locator = self.groups_multi_select.locator("> .umcMultiObjectSelect__buttons > .dijitButton", has=self.page.locator("[data-iconName=\"plus\"]"))
        
        self.add_group_modal: AddObjectModal = AddObjectModal(self.page.locator(".umcMultiObjectSelect__detailDialog.dijitDialog").first)

        self.save_button: Locator = self.page.locator(".dijit.dijitReset.dijitInline.ucsButton.ucsPrimaryButton.dijitButton:visible", has=page.locator("[data-iconName=save]"))


class OXResourcesPage(ManagmentListPage):

    def __init__(self, page: Page) -> None:
        super().__init__(page)

    def get_url(self, url_portal):
        return super().get_url(url_portal) + "?menu=false#module=udm:oxresources/oxresources"

class FunctionalAccountsPage(ManagmentListPage):

    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.delete_entry_dialog: DeleteEntryDialog = DeleteEntryDialog(self.locator(".dijitDialog"))

    def get_url(self, url_portal):
        return super().get_url(url_portal) + "?menu=false#module=udm:oxmail/functional_account"

class DeleteEntryDialog(BasePagePart):
    def __init__(self, locator: Locator | FrameLocator) -> None:
        super().__init__(locator)
        self.buttons: Locator = self.locator(".umcLayoutRow--buttonsright")
        self.delete_button: Locator = self.buttons.locator("> span").nth(1)

class EditFunctionalAccountPage(BasePage):

    def __init__(self, page):
        super().__init__(page)
        self.name_field: Locator = self.page.locator("[name=name]")
        self.email: Locator = self.page.locator("[name=mailPrimaryAddress]")
        self.email_field: Locator = self.page.locator(".dijitReset.dijitInputField.dijitInputContainer:not(.dijitIncomplete)").filter(has=self.email).locator("input.dijitReset.dijitInputInner")

        self.users_add_button: Locator = self.page.locator(".umcContainerWidget.umcMultiObjectSelect__buttons").locator(".dijit.dijitReset.dijitInline.ucsButton.ucsTextButton.dijitButton:visible").filter(has=self.page.locator("[data-iconName=plus]"))

        self.add_user_modal: AddObjectModal = AddObjectModal(self.page.locator(".umcMultiObjectSelect__detailDialog.dijitDialog").first)

        self.save_button: Locator = self.page.locator(".dijit.dijitReset.dijitInline.ucsButton.ucsPrimaryButton.dijitButton:visible", has=page.locator("[data-iconName=save]"))

class EditOXResourcePage(BasePage):

    def __init__(self, page):
        super().__init__(page)
        self.name_field: Locator = self.page.locator("[name=name]")
        self.displayname_field: Locator = self.page.locator("[name=displayname]")
        self.resoruce_email_field: Locator = self.page.locator("[name=\"resourceMailAddress\"]")

        self.resource_manager_hidden_input: Locator = self.page.locator("[name=\"resourceadmin\"]")
        self.incomplete_div: Locator = self.page.locator(".syntaxLDAP_Search.dijitTextBoxIncomplete", has=self.resource_manager_hidden_input)
        self.resoruce_manager_input: Locator = self.page.locator(".dijitInputField", has=self.resource_manager_hidden_input).locator("input[type=\"text\"]")

        self.save_button: Locator = self.page.locator(".dijit.dijitReset.dijitInline.ucsButton.ucsPrimaryButton.dijitButton:visible", has=page.locator("[data-iconName=save]"))
