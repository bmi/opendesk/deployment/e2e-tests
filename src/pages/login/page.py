# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import re
from playwright.sync_api import Page, Locator
from pages.base.base import BasePage

class LoginPage(BasePage):
    
    def get_title(self) -> str | re.Pattern[str] | None:
        return self._("page_title")
    
    def open_page(self, url_portal):
        url: str = url_portal
        if not url.endswith("/"):
            url += "/"
        url += "univention/saml/?location=/univention/portal"
        self.page.goto(url)
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.login_form: Locator = self.page.locator("form#kc-form-login")
        self.username_field: Locator = self.login_form.locator('#username')
        self.password_field: Locator = self.login_form.locator('#password')
        self.submit_form_button: Locator = self.login_form.locator('[type="submit"]')
        self.error_box: Locator = self.page.locator(".alert-error.pf-c-alert.pf-m-inline.pf-m-danger")
        self.content_wrapper: Locator = self.page.locator("#kc-content-wrapper")
        self.forgot_password_link: Locator = self.page.locator("#umcLoginLinks > a")    
        
    def input_username(self, username: str):
        self.username_field.type(username)
        
    def input_password(self, password: str):
        self.password_field.type(password)
        
    def fill_login(self, username: str, password: str):
        self.input_username(username)
        self.input_password(password)
        
class Login2FAPage(BasePage):
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.otp_input: Locator = self.page.locator("#otp")
        self.login_button: Locator = self.page.locator("#kc-login")
        self.otp_manual_button: Locator = self.page.locator("#mode-manual")
        self.otp_secret_key: Locator = self.page.locator("#kc-totp-secret-key")
        self.totp_input: Locator = self.page.locator("#totp")
        self.save_otp_button: Locator = self.page.locator("#saveTOTPBtn")

    def get_secret_key_text(self) -> str:
        return self.otp_secret_key.inner_text()

class PasswordRenewalPage(BasePage):
    
    def __init__(self, page):
        super().__init__(page)
        
        self.password_renewal_form: Locator = self.page.locator("#kc-passwd-update-form")
        self.renew_password_field: Locator = self.password_renewal_form.locator("#password")
        self.new_password_field: Locator = self.password_renewal_form.locator("#password-new")
        self.confirm_password_field: Locator = self.password_renewal_form.locator("#password-confirm")
        
        self.confirm_password_button: Locator = self.password_renewal_form.locator("[type=\"submit\"]")