# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from re import Pattern
from playwright.sync_api import Page, Locator
from pages.base.base import expect
from pages.base.base import BasePage
from .self_service import *
from .header import *
from .sidebar import *
from .page_parts import *

class PortalPage(BasePage):

    def get_title(self) -> str | Pattern[str] | None:
        return "Portal - openDesk"

    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.app_container: Locator = page.locator("#app")
        self.portal_container: Locator = self.app_container.locator("> .portal")
        self.quick_select: Locator = self.app_container.locator(".portal-quick-draft")

        self.header: Header = Header(page.locator("#portal-header"))
        self.sidebar: Sidebar = Sidebar(page.locator("nav.portal-sidenavigation"))
        self.visible_modal_wrapper: Locator = self.page.locator(".modal-wrapper--isVisible:not(.modal-wrapper--isSecondLayer):visible")
        self.visible_second_layer_modal_wrapper: Locator = self.page.locator(".modal-wrapper--isVisible.modal-wrapper--isSecondLayer:visible")
        self.self_service_modal_wrapper: Locator = self.page.locator(".modal-wrapper--selfservice")
        self.user_settings_modal: UserSettingsModal = UserSettingsModal(self.self_service_modal_wrapper)
        self.forgot_password_modal: ForgotPasswordModal = ForgotPasswordModal(self.self_service_modal_wrapper)
        self.new_password_modal: NewPasswordModal = NewPasswordModal(self.self_service_modal_wrapper)
        self.alert_modal: AlertModal = AlertModal(self.self_service_modal_wrapper)
        self.password_change_successful_modal: PasswordChangeSuccessfulModal = PasswordChangeSuccessfulModal(self.self_service_modal_wrapper)

        self.notification_wrapper: Locator = self.page.locator("#notifications-visible")
        self.notification: Notification = Notification(self.notification_wrapper.locator(".notification"))
        self.success_notification: SuccessNotification = SuccessNotification(self.notification_wrapper.locator(".notification.notification--success"))
        
        self.announcement_container: Locator = self.page.locator("#announcement-container")
    
    def find_announcement(self, title: str, announcement_type: Literal["info", "warn", "success", "danger"] | None = None, with_message: bool | None = None):
        announcement_class: str = ".announcement"
        if announcement_type:
            announcement_class += f".announcement--{announcement_type}"
        if with_message != None:
            if with_message:
                announcement_class += ".announcement--withMessage"
            else:
                announcement_class += ":not(.announcement--withMessage)"
        
        return self.announcement_container.locator(f"{announcement_class}", has=self.page.locator(".announcement__title", has_text=title))

    def open_page(self, url_portal: str):
        self.page.goto(url_portal)

    def toggle_sidebar(self):
        self.header.sidebar_button.click()

    def get_tile_by_name(self, name: str | Pattern[str]) -> Locator:
        return self.page.locator(".portal-category__tiles > div.portal-tile__root-element", has=self.page.locator("span.portal-tile__name", has_text=name))

    def get_folder_by_name(self, name: str | Pattern[str]) -> Locator:
        return self.page.locator(".portal-category__tiles > div.portal-folder", has=self.page.locator("span.portal-folder__name", has_text=name))

    def validate(self) -> None:
        super().validate()
        expect(self.portal_container).to_be_visible()

class PortalPageNotLoggedIn(PortalPage):
    def __init__(self, page):
        super().__init__(page)
        self.sidebar = SidebarNotLoggedIn(self.sidebar.page_part_locator)

    def get_login_tile(self):
        return self.get_tile_by_name(self._("login_tile_label"))

    def get_login_button(self):
        return self.page.locator("#loginButton")

class PortalPageLoggedIn(PortalPage):
    def __init__(self, page):
        super().__init__(page)
        self.create_new_in_files_quick_draft: CreateNewInFilesQuickDraft = CreateNewInFilesQuickDraft(self.quick_select)
        self.sidebar: SidebarLoggedIn = SidebarLoggedIn(self.sidebar.page_part_locator)
        self.newsfeed: Newsfeed = Newsfeed(self.page.locator(".newsfeed"))

    def get_logout_button(self):
        return self.page.locator("#logoutButton")

    def get_user_button(self):
        return self.page.locator("#header-button-user")

    def get_user_settings_button(self):
        return self.sidebar.user_settings_button

    def get_manage_profile_button(self):
        return self.sidebar.user_settings_menu.manage_profile_button

    def get_email_tile(self):
        return self.get_tile_by_name(self._("email_tile_label"))

    def get_calendar_tile(self):
        return self.get_tile_by_name(self._("calendar_tile_label"))

    def get_contacts_tile(self):
        return self.get_tile_by_name(self._("contacts_tile_label"))

    def get_tasks_tile(self):
        return self.get_tile_by_name(self._("tasks_tile_label"))

    def get_files_tile(self):
        return self.get_tile_by_name(self._("files_tile_label"))

    def get_projects_tile(self):
        return self.get_tile_by_name(self._("projects_tile_label"))

    def get_knowledge_tile(self):
        return self.get_tile_by_name(self._("knowledge_tile_label"))

    def get_chat_tile(self):
        return self.get_tile_by_name(self._("chat_tile_label"))

    def get_videoconference_tile(self):
        return self.get_tile_by_name(self._("videoconference_tile_label"))

    def get_notes_tile(self):
        return self.get_tile_by_name(self._("notes_tile_label"))

class PortalPageAdminLoggedIn(PortalPageLoggedIn):

    def __init__(self, page):
        super().__init__(page)
        self.sidebar: SidebarAdminLoggedIn = SidebarAdminLoggedIn(self.sidebar.page_part_locator)

    def get_users_tile(self):
        return self.get_tile_by_name(self._("users_tile_label"))

    def get_groups_tile(self):
        return self.get_tile_by_name(self._("groups_tile_label"))

    def get_functional_mailboxes_tile(self):
        return self.get_tile_by_name(self._("functional_mailboxes_tile_label"))

    def get_resources_tile(self):
        return self.get_tile_by_name(self._("resources_tile_label"))

    def get_announcements_tile(self):
        return self.get_tile_by_name(self._("announcements_tile_label"))

class EditPortalPage(PortalPage):
    
    def __init__(self, page):
        super().__init__(page)
        
        self.header: EditModeHeader = EditModeHeader(self.header.page_part_locator)
        self.sidebar: EditModeSidebar = EditModeSidebar(self.sidebar.page_part_locator)
        
        self.portal_categories: Locator = self.page.locator("#portalCategories")
        self.last_portal_category: Locator = self.portal_categories.locator(".portal-category").last
        self.last_portal_category_add_tile_tile: Locator = self.last_portal_category.locator(".portal-category__tiles > div.tile-add")
        
        self.add_entry_modal: AddEntryModal = AddEntryModal(self.visible_modal_wrapper)
        self.create_new_entry_modal: CreateNewEntryModal = CreateNewEntryModal(self.visible_modal_wrapper)
        self.translation_modal: TranslationModal = TranslationModal(self.visible_second_layer_modal_wrapper)
    
    def validate(self):
        super().validate()
        expect(self.header.close_edit_mode_button).to_be_visible()