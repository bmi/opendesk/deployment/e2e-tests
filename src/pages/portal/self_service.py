# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from typing import Literal
from playwright.sync_api import Locator
from pages.base.base import BasePagePart
from .page_parts import *

class SelfServiceModal(Modal):
    
    def __init__(self, locator: Locator, role: Literal["dialog", "alertDialog"]):
        super().__init__(locator.filter(has=locator.page.locator(f".dialog.dialog--selfservice[role=\"{role}\"]")))
        self.form = self.locator("form")

class UserSettingsModal(SelfServiceModal):

    def __init__(self, locator):
        super().__init__(locator, "dialog")
        self.upload_button = self.form.locator("[data-test^=imageUploadButton--]")
        self.upload_input = self.form.locator("[data-test^=imageUploadFileInput--]")
        self.save_button = self.form.locator("[type=submit]")


class ForgotPasswordModal(SelfServiceModal):
    
    def __init__(self, locator):
        super().__init__(locator, "dialog")
        self.page_part_locator = self.page_part_locator.filter(has=locator.page.locator(".dialog__header > h3", has_text=self._("title")))
        self.username_field = self.form.locator("[name=username]")
        self.next_button = self.form.locator("[type=submit]")
        self.method_email_radio = self.form.locator("#method--email")
    
    def input_forgot_password_username(self, username: str):
        self.username_field.type(username)
    
    def click_next(self):
        self.next_button.click()
        
class NewPasswordModal(SelfServiceModal):
    
    def __init__(self, locator):
        super().__init__(locator, "dialog")
        self.page_part_locator = self.page_part_locator.filter(has=locator.page.locator(".dialog__header > h3", has_text=self._("title")))
        self.new_password_input: Locator = self.form.locator("[data-testid=new-password-box]")
        self.retype_new_password_input: Locator = self.form.locator("[data-testid=retype-password-box]")
        self.change_password_button: Locator = self.form.locator("[type=submit]")
        self.input_error_messages: Locator = self.form.locator(".input-error-message", has_text=self._("passwords_dont_match"))

class PasswordChangeSuccessfulModal(SelfServiceModal):
    
    def __init__(self, locator):
        super().__init__(locator, "dialog")
        
    def get_password_changed_response_success(self):
        return self._("password_changed_success_title")

class AlertModal(SelfServiceModal):
    
    def __init__(self, locator):
        super().__init__(locator, "alertDialog")
    
    def get_password_changed_response_fail(self):
        return self._("password_changed_fail_title")
