# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
from pages.base.base import expect
from pages.base.base import BasePage
from .page_parts import *

class OpenProjectPage(BasePage):
    
    def get_timeout(self) -> float:
        return 1000*60*2
    
    def open_page(self, url_portal):
        self.page.goto(url_portal.replace("portal.", "projects."))
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.wrapper: Locator = self.page.locator("#wrapper")
        self.main: Locator = self.wrapper.locator("#main")
        self.current_spot_modal: Locator = self.page.locator(".spot-drop-modal-portal > .spot-drop-modal--body.spot-container:visible").first
        
        self.header: AppHeader = AppHeader(self.wrapper.locator("> header.op-app-header"))
        self.sidebar: Sidebar = Sidebar(self.main.locator("#menu-sidebar"))
        
        self.first_login_modal: FirstLoginModal = FirstLoginModal(self.page.locator(".onboarding-modal"))
        self.project_list_modal: ProjectListModal = ProjectListModal(self.current_spot_modal)
        
        self.skip_intro_button: Locator = self.page.locator(".enjoyhint_skip_btn")
        
        self.flash_messages: Locator = self.page.locator("#primerized-flash-messages")
        self.success_flash_message: Locator = self.flash_messages.locator(".Banner--success.flash-success")

class ProjectPage(OpenProjectPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.sidebar: ProjectSidebar = ProjectSidebar(self.main.locator("#menu-sidebar"))  
   
class StoragePage(OpenProjectPage):
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.new_storage_button: Locator = self.main.locator(".PageHeader-actions > a", has=page.locator(".Button-visual.Button-leadingVisual > .octicon.octicon-plus"))
        self.storage_select_nextcloud: Locator = self.main.locator("#storages_project_storage_storage_id")
        self.storage_submit_button: Locator = self.main.locator(".-primary.button[type=\"submit\"]")
        
        self.storage_mode_automatic_radio: Locator = self.main.locator("#storages_project_storage_project_folder_mode_automatic")
        
        self.request_access_button: Locator = self.page.locator("[data-storages--oauth-access-grant-nudge-modal-target=\"requestAccessButton\"]")
        
        self.file_storages_table: Locator = self.main.locator("[data-test-selector=\"Storages::ProjectStorages::TableComponent\"]")
        self.table_list: Locator = self.file_storages_table.locator("table.generic-table > tbody")
        self.table_entry: Locator = self.table_list.locator("> tr")
        
        self.table_nextcloud_entry: Locator = self.table_entry.filter(has=self.page.locator("td.provider_type", has_text="Nextcloud"))

class NewProjectPage(OpenProjectPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.project_name_input: Locator = self.page.locator("[data-qa-field-name=\"name\"]").locator("op-text-input > input")
        self.clear_parent_button: Locator = self.page.locator("[data-qa-field-name=\"parent\"]").locator(".ng-clear-wrapper")
        self.current_parent_project: Locator = self.page.locator("[data-qa-field-name=\"parent\"]").locator(".ng-value-container > .ng-value")
        
        self.create_button: Locator = self.page.locator(".op-form--submit > [type=\"submit\"]")

class WorkPackagePage(OpenProjectPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.edit_form: WorkPackageEditForm = WorkPackageEditForm(self.page.locator(".work-packages--new"))
        
        self.close_button: Locator = self.page.locator("button[data-test-selector=\"op-back-button\"]")

class WorkPackagesPage(OpenProjectPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.create_button: Locator = self.page.locator("wp-create-button button.add-work-package")
        
        self.create_button_dropdown: Locator = self.page.locator("#types-context-menu")
        self.create_button_dropdown_item: Locator = self.create_button_dropdown.locator(".dropdown-menu > li")
        self.create_button_dropdown_item_task: Locator = self.create_button_dropdown_item.locator("> a.__hl_inline_type_1")
        
        self.work_packages: Locator = self.page.locator("tbody.results-tbody.work-package--results-tbody")
        self.work_package_entry: Locator = self.work_packages.locator("tr.wp-table--row")
        
    def get_work_package_by_id(self, work_package_id: str):
        return self.work_package_entry.filter(has=self.page.locator(f"[data-work-package-id=\"{work_package_id}\"]"))

    def get_work_package_by_subject(self, subject: str):
        return self.work_package_entry.filter(has=self.page.locator(f"[data-field-name=\"subject\"][title=\"{subject}\"]"))

class AdminPage(OpenProjectPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.sidebar: AdminSidebar = AdminSidebar(self.main.locator("#menu-sidebar"))
    
    def open_page(self, url_portal):
        url: str = url_portal.replace("portal.", "projects.")
        if not url.endswith("/"): url += "/"
        self.page.goto(url + "admin")

class ExternalFileStoragePage(AdminPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.nextcloud_storage_entry: Locator = self.page.locator("#storages_nextcloud_storage_1")
        self.nextcloud_storage_name: Locator = self.nextcloud_storage_entry.locator("[data-test-selector=\"storage-name\"]")
    
    def validate(self):
        super().validate()
        expect(self.nextcloud_storage_entry).to_be_visible(timeout=10000)

class ExternalFileStorageEntryPage(AdminPage):

    def __init__(self, page):
        super().__init__(page)
        
        self.success_label: Locator = self.page.locator(".Label.Label--success")
        
        self.general_info_section: Locator = self.page.locator("#storage_general_info_section")
        self.oauth_section: Locator = self.page.locator("#storage_openproject_oauth_section")
        self.oauth_client_section: Locator = self.page.locator("#storage_oauth_client_section")
        self.automatically_managed_project_folders_section: Locator = self.page.locator("#automatically_managed_project_folders_section")
        self.health_status_component: Locator = self.page.locator("#storages-admin-side-panel-health-status-component")

class LdapSourcesPage(AdminPage):
    
    def __init__(self, page):
        super().__init__(page)
        
        self.table: Locator = self.page.locator("table.generic-table")
        self.table_entry: Locator = self.table.locator("> tbody > tr")
    
    def get_entry_by_name(self, name: str) -> LdapSourceEntry:
        return LdapSourceEntry(self.table_entry.filter(has=self.page.locator("td.name", has_text=name)))

class MailNotificationsPage(AdminPage):
    
    def __init__(self, page):
        super().__init__(page)
        
        self.send_test_mail_button: Locator = self.page.locator("[data-method=post][href=\"/admin/test_email\"]")

class GrantAccessPage(BasePage):
    
    def __init__(self, page):
        super().__init__(page)
        self.login_button: Locator = self.page.locator("#login-form > [type=\"submit\"]")
        self.grant_button: Locator = self.page.locator("#submit-wrapper > .login.primary.icon-confirm-white")
        self.success_dialog: Locator = self.page.locator("#storages-project-storages-oauth-access-granted-modal-component-dialog-id").locator(".octicon.octicon-check-circle.color-fg-success")

class OpenProjectPageNoPermission(BasePage):
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
    
    def open_page(self, url_portal):
        self.page.goto(url_portal.replace("portal.", "projects."))
    
    def validate(self) -> None:
        super().validate()
        
        expect(self.page.locator("#primerized-flash-messages").locator(".Banner--error.flash-error")).to_be_visible(timeout=30000)