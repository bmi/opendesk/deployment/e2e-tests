# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
import re
from pages.base.base import BasePage, expect
from .page_parts import *

class XWikiPage(BasePage):
    
    def get_title(self) -> str | re.Pattern[str] | None:
        return re.compile(self._("page_title"))

    def get_timeout(self) -> float:
        return 1000*60*2 
    
    def open_page(self, url_portal):
        self.page.goto(url_portal.replace("portal.", "wiki."))
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.container: Locator = self.page.locator("#xwikimaincontainerinner")
        self.content_container: Locator = self.container.locator("#contentcontainer")
        self.left_panels: Locator = self.content_container.locator("#leftPanels")
        self.right_panels: Locator = self.content_container.locator("#rightPanels")
        
        self.bootstrap_tour_close_button: Locator = self.page.locator("#bootstrap_tour_close")
        self.navbar: Navbar = Navbar(self.container.locator("#menuview > header"))
    
    def validate(self):
        super().validate()
        # Hier optional die Dialogbox "Bootstrap_Tour_Close" wegklicken (https://gitlab.opencode.de/bmi/opendesk/deployment/e2e-tests/-/issues/56)
        try:
            expect(self.bootstrap_tour_close_button).to_be_visible(timeout=5000)
            self.bootstrap_tour_close_button.click()
        except:
            pass

class XWikiLeftPanelsPage(XWikiPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.page_navigation: PageNavigation = PageNavigation(self.left_panels.locator("> [role=\"navigation\"]"))

class XWikiViewPage(XWikiLeftPanelsPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.document_title: Locator = self.content_container.locator("#document-title > h1")
        
        self.create_page_button: Locator = self.content_container.locator("#tmCreate > .btn.btn-default")
        
class XWikiViewEditPage(XWikiViewPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.document_title_input: Locator = self.document_title.locator("> #document-title-input")

class XWikiCreatePage(XWikiLeftPanelsPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.form: Locator = self.container.locator("form#create")
        self.title_field: Locator = self.form.locator("#targetTitle")
        self.create_button: Locator = self.form.locator("[type=\"submit\"]")
        
class XWikiEditPage(XWikiPage):
    
    def __init__(self, page):
        super().__init__(page)
        
        self.text_editor: Locator = self.container.locator("#cke_content")
        self.text_edit_container: Locator = self.text_editor.locator("> div > .cke_contents")
        self.save_and_view_button: Locator = self.container.locator("[name=\"action_save\"][type=\"submit\"]")

class XWikiNewsfeedPage(XWikiLeftPanelsPage):
    
    def get_title(self):
        return self._("page_title")
    
    def open_page(self, url_portal):
        url: str = url_portal.replace("portal.", "wiki.")
        if not url.endswith("/"): url += "/"
        url += "bin/view/openDesk/Newsfeed/"
                
        self.page.goto(url)
    
    def __init__(self, page):
        super().__init__(page)
        self.new_newsfeed_form: Locator = self.page.locator("form.newBlogPostForm")
        self.new_newsfeed_blog_input: Locator = self.new_newsfeed_form.locator("#entryTitle")
        self.new_newsfeed_blog_submit: Locator = self.new_newsfeed_form.locator(".btn[type=\"submit\"]")

class XWikiEditBlogPostPage(XWikiLeftPanelsPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.title_input: Locator = self.page.locator("[id=\"Blog.BlogPostClass_0_title\"]")
        self.text_editor: Locator = self.container.locator("[id=\"cke_Blog.BlogPostClass_0_content\"]")
        self.text_edit_container: Locator = self.text_editor.locator("> div > .cke_contents")
        self.save_and_view_button: Locator = self.container.locator("[name=\"action_save\"][type=\"submit\"]")
        