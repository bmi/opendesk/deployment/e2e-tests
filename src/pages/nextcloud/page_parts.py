# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Locator, FrameLocator
import re
from typing import Literal
from pages.base.base import BasePagePart

class Header(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.left: Locator = self.locator("> .header-left")
        self.right: Locator = self.locator("> .header-right")
        self.user_menu_button: Locator = self.right.locator("#user-menu")
    
class UserMenu(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.profile_button: Locator = self.locator("#profile")
        self.accessibility_settings_button: Locator = self.locator("#accessibility_settings")
        self.settings_button: Locator = self.locator("#settings")
        self.admin_settings_button: Locator = self.locator("#admin_settings")
        self.core_apps_button: Locator = self.locator("#core_apps")
        self.core_users_button: Locator = self.locator("#core_users")
        self.help_button: Locator = self.locator("#help")
        self.logout_button: Locator = self.locator("#logout")

class SidebarLeft(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.entries: Locator = self.locator("ul.app-navigation-list.app-navigation__list")
        self.settings_entries: Locator = self.locator("ul.app-navigation-entry__settings")
        
        self.file_settings_entry: Locator = self.settings_entries.locator("li[data-cy-files-navigation-settings-button]")
        self.settings_entries: Locator = self.locator("ul.app-navigation-entry__settings")
        
        self.file_settings_entry: Locator = self.settings_entries.locator("li[data-cy-files-navigation-settings-button]")

        self.shares_tab_button: Locator = self.locator("[data-cy-files-navigation-item=\"shareoverview\"]")
    
    def get_sidebar_elements(self) -> list[Locator]:
        return self.entries.locator(f"> li")
    
    def get_sidebar_element(self, navigation_item: Literal["files", "recent", "favorites", "shareoverview", "groupfolders", "trashbin"]) -> Locator:
        return self.entries.locator(f"> li[data-cy-files-navigation-item={navigation_item}]")
    
    def get_sidebar_element_children(self, element: Locator) -> list[Locator]:
        element.locator("ul.app-navigation-entry__children").locator(" > li")
    
    def get_sidebar_element_child(self, element: Locator, child_navigation_item: Literal["sharingin", "sharingout", "sharinglinks", "deletedshares", "pendingshares"]) -> Locator:
        element.locator("ul.app-navigation-entry__children").locator(f"> li[data-cy-files-navigation-item={child_navigation_item}]")
    
class SidebarRight(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.sharing_tab_button: Locator = self.locator("#tab-button-sharing")
        self.activity_tab_button: Locator = self.locator("#tab-button-activity")
        self.chat_tab_button: Locator = self.locator("#tab-button-chat")
        self.open_project_tab_button: Locator = self.locator("#tab-button-open-project")
        
        self.sharing_tab: SharingSidebarTab = SharingSidebarTab(self.locator("#tab-sharing:visible"))
        self.activity_tab: ActivitySidebarTab = ActivitySidebarTab(self.locator("#tab-activity:visible"))
        self.chat_tab: ChatSidebarTab = ChatSidebarTab(self.locator("#tab-chat:visible"))
        self.open_project_tab: OpenProjectSidebarTab = OpenProjectSidebarTab(self.locator("#tab-open-project:visible"))
        
        self.close_button: Locator = self.locator("button.app-sidebar__close.button-vue.button-vue--icon-only.button-vue--vue-tertiary")

class SidebarRightTab(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)

class ActivitySidebarTab(SidebarRightTab):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.activity_comment: Locator = self.locator(".activity__actions > .comment.comments-action")
        self.comment_editor: Locator = self.activity_comment.locator("form.comment__editor")
        self.comment_input: Locator = self.comment_editor.locator("div.rich-contenteditable__input")
        
        self.comment_entry: Locator = self.locator("ul > li.comment.comments-activity")
        
        self.autocomplete_list: Locator = self.page.locator("div[role=listbox][class*=\"_tribute-container-autocomplete\"] > ul")
        self.autocomplete_entry: Locator = self.autocomplete_list.locator("> li")
        
        self.emoji_list: Locator = self.page.locator("div[role=listbox][class*=\"_tribute-container-emoji\"] > ul")
        self.emoji_entry: Locator = self.emoji_list.locator("> li")
    
    def get_emoji_entry(self, emoji_name: str):
        return self.emoji_entry.filter(has=self.page.locator("div", has_text=emoji_name))
    
    def get_autocomplete_entry(self, firstname: str, lastname: str):
        return self.autocomplete_entry.filter(has=self.page.locator("span.autocomplete-result__title", has_text=f"{firstname} {lastname}"))
    
    def get_comment(self, firstname: str | None = None, lastname: str | None = None, message: str | None = None, mentions: list[str] | None = None):
        comment: Locator = self.comment_entry
        if firstname and lastname:
            comment = comment.filter(has=self.page.locator(f"div.comment__header > span.comment__author", has_text=f"{firstname} {lastname}"))
        if message:
            comment = comment.filter(has=self.page.locator(f"div.comment__message", has_text=message))
        if mentions:
            for mention in mentions:
                comment = comment.filter(has=self.page.locator("div.comment__message > .mention-bubble").locator(f".mention-bubble__title[title*=\"{mention}\"]"))
        return comment

class ChatSidebarTab(SidebarRightTab):

    def __init__(self, locator):
        super().__init__(locator)
        self.join_conversation_button: Locator = self.locator(".button-vue--vue-primary")
        
        self.chat_message_form: Locator = self.locator(".new-message-form")
        self.chat_message_input: Locator = self.chat_message_form.locator(".rich-contenteditable__input[contenteditable=true]")
        self.chat_message_send_button: Locator = self.chat_message_form.locator("[type=\"submit\"]")

        self.chat_messages = self.locator(".message")

    def get_chat_message(self, message: str):
        return self.chat_messages.filter(has_text=message)
        
class SharingSidebarTab(SidebarRightTab):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.sharing_tab_details: Locator = self.locator("div.sharingTabDetailsView:visible")
        self.share_input_field: Locator = self.locator("#sharing-search-input:visible")
        self.share_spinner: Locator = self.locator(".material-design-icon.loading-icon")
        self.details_advanced: Locator = self.sharing_tab_details.locator(".sharingTabDetailsView__wrapper > .sharingTabDetailsView__advanced")
        
        self.set_password_checkbox: Locator = self.details_advanced.locator(".checkbox-radio-switch-checkbox", has_text=self._("set_password_label")).locator("span.checkbox-content")
        self.set_password_checkbox_checked: Locator = self.details_advanced.locator(".checkbox-radio-switch-checkbox.checkbox-radio-switch--checked", has_text=self._("set_password_label")).locator("span.checkbox-content")
        self.set_password_checkbox_disabled: Locator = self.details_advanced.locator(".checkbox-radio-switch-checkbox.checkbox-radio-switch--disabled", has_text=self._("set_password_label")).locator("span.checkbox-content")
        self.set_password_input: Locator = self.sharing_tab_details.locator(".input-field.input-field--trailing-icon > .input-field__main-wrapper > input[type=\"password\"]")
        
        self.expiry_checkbox: Locator = self.details_advanced.locator(".checkbox-radio-switch-checkbox", has_text=re.compile(self._("expiry_label_re"))).locator("span.checkbox-content")
        self.expiry_checkbox_checked: Locator = self.details_advanced.locator(".checkbox-radio-switch-checkbox.checkbox-radio-switch--checked", has_text=re.compile(self._("expiry_label_re"))).locator("span.checkbox-content")
        self.expiry_checkbox_disabled: Locator = self.details_advanced.locator(".checkbox-radio-switch-checkbox.checkbox-radio-switch--disabled", has_text=re.compile(self._("expiry_label_re"))).locator("span.checkbox-content")
        self.expiry_input: Locator = self.sharing_tab_details.locator("#share-date-picker")
        
        self.share_save: Locator = self.sharing_tab_details.locator(".sharingTabDetailsView__footer > div.button-group > button.button-vue.button-vue--text-only.button-vue--vue-primary")
        self.sharing_tab_sharee_list: Locator = self.locator("ul.sharing-sharee-list")
        self.sharing_tab_link_list: Locator = self.locator("ul.sharing-link-list")
       
    def get_sharing_search_list(self):
        area_controls = self.share_input_field.get_attribute("aria-controls")
        return self.page.locator(f"#{area_controls}")
    
    def get_sharing_search_list_entry(self, user: str):
        return self.get_sharing_search_list().locator("> li", has=self.page.locator(f'div.option__details > span', has_text=re.compile(f"({user})")))
    
    def get_sharing_shared_entry(self, user: str):
        return self.sharing_tab_sharee_list.locator("li.sharing-entry", has=self.page.locator(f'span', has_text=re.compile(f"({user})")))
    
    def get_sharing_shared_extern_entry(self, email: str):
        return self.sharing_tab_link_list.locator("li.sharing-entry", has=self.page.locator(f"span[title=\"{email}\"]")) 

class OpenProjectSidebarTab(SidebarRightTab):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.connect_to_openproject_button: Locator = self.locator("button.oauth-connect--button")
        
        self.create_link_to_work_package_button: Locator = self.locator("button.create-workpackage--button")
        
        self.linked_work_packages_list: Locator = self.locator(".linked-workpackages")
        self.linked_work_package_entry: Locator = self.linked_work_packages_list.locator(".linked-workpackages--workpackage")
    
    def get_work_package_by_subject(self, subject: str):
        return self.linked_work_package_entry.filter(has=self.page.locator(".row__subject", has_text=subject))

class Popper(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)

class FileDropdownPopper(Popper):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.details_entry: Locator = self.locator("[data-cy-files-list-row-action=details]")
        self.move_copy_entry: Locator = self.locator("[data-cy-files-list-row-action=move-copy]")
        self.rename_file_entry: Locator = self.locator("[data-cy-files-list-row-action=rename]")
        self.delete_file_entry: Locator = self.locator("[data-cy-files-list-row-action=delete]")
        self.open_cryptpad_entry: Locator = self.locator("[data-cy-files-list-row-action=edit-cryptpad-file]")
        self.open_office_entry: Locator = self.locator("[data-cy-files-list-row-action=office-open-pdf]")

class NewFilePopper(Popper):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.upload_file: Locator = self.locator("[data-cy-upload-picker-add]")
        self.create_folder: Locator = self.locator("ul > li.action.upload-picker__menu-entry[data-cy-upload-picker-menu-entry=\"newFolder\"]")
        self.create_cryptpad_file: Locator = self.locator("ul > li.action.upload-picker__menu-entry[data-cy-upload-picker-menu-entry=\"add-drawio-file\"]")
        self.create_document: Locator = self.locator("ul > li.action.upload-picker__menu-entry[data-cy-upload-picker-menu-entry=\"template-new-richdocuments-1\"]")
        self.create_presentation: Locator = self.locator("ul > li.action.upload-picker__menu-entry[data-cy-upload-picker-menu-entry=\"template-new-richdocuments-3\"]")
        self.create_diagram: Locator = self.locator("ul > li.action.upload-picker__menu-entry[data-cy-upload-picker-menu-entry=\"template-new-richdocuments-4\"]")
        self.create_spreadsheet: Locator = self.locator("ul > li.action.upload-picker__menu-entry[data-cy-upload-picker-menu-entry=\"template-new-richdocuments-2\"]")
        self.create_text_file: Locator = self.locator("ul > li.action.upload-picker__menu-entry[data-cy-upload-picker-menu-entry=\"template-new-text-0\"]")

class BatchActionsPopper(Popper):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.link_to_work_package: Locator = self.locator("ul > li.action.files-list__row-actions-batch-open-project-multiple")
        self.move_or_copy: Locator = self.locator("ul > li.action.files-list__row-actions-batch-move-copy")
        self.download: Locator = self.locator("ul > li.action.files-list__row-actions-batch-download")
        self.zip_files: Locator = self.locator("ul > li.action.files-list__row-actions-batch-files_zip")
        self.delete: Locator = self.locator("ul > li.action.files-list__row-actions-batch-delete")

class FileEntry(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.checkbox: Locator = self.locator(".files-list__row-checkbox > .checkbox-radio-switch-checkbox")
        self.checkbox_checked: Locator = self.locator(".files-list__row-checkbox > .checkbox-radio-switch-checkbox.checkbox-radio-switch--checked")
        
        self.name_button: Locator = self.locator(".files-list__row-name-link")
        self.name_text: Locator = self.locator(".files-list__row-name-text")
        self.name: Locator = self.locator(".files-list__row-name-")
        self.name_ext: Locator = self.locator(".files-list__row-name-ext")
        
        self.rename_file_form: Locator = self.locator("form.files-list__row-rename")
        self.rename_file_input: Locator = self.rename_file_form.locator(".input-field__input")
        
        self.share_button: Locator = self.locator("button.action-item[data-cy-files-list-row-action=sharing-status]")
        self.drop_down_button: Locator = self.locator("div.action-item", has=self.page.locator(".v-popper.v-popper--theme-dropdown"))
        
        self.last_modified: Locator = self.locator("td[data-cy-files-list-row-mtime]")

class FileList(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.header: Locator = self.locator(".files-list__header")
        self.breadcrumb_list: Locator = self.header.locator(".breadcrumb__crumbs")
        self.breadcrumb: Locator = self.breadcrumb_list.locator("> li")
        self.breadcrumb_home: Locator = self.breadcrumb.first
        self.new_file_button: Locator = self.header.locator(".upload-picker.files-list__header-upload-button")
        self.upload_file_input: Locator = self.new_file_button.locator("[data-cy-upload-picker-input]")
        
        self.file_list: Locator = self.locator("tbody[data-cy-files-list-tbody]")
        self.action_items: Locator = self.locator(".files-list__thead-overlay > .files-list__column.files-list__row-actions-batch > .action-items")
        self.add_to_favorites_button: Locator = self.action_items.locator("> button.files-list__row-actions-batch-favorite")
        self.batch_actions_dropdown_button: Locator = self.action_items.locator(".action-item > .v-popper.v-popper--theme-dropdown > .action-item__menutoggle")
    
    def get_file_entry(self, filename: str) -> FileEntry:
        return FileEntry(self.file_list.locator(f"tr[data-cy-files-list-row][data-cy-files-list-row-name=\"{filename}\"]"))
    
    def get_modified_seconds_ago(self):
        return self._("modified_seconds_ago")

class CompressFilesModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.dialog: Locator = self.locator(".dialog__content.zip-dialog")
        self.filename_input: Locator = self.dialog.locator("input.input-field__input")
        self.compress_button: Locator = self.locator(".dialog > .dialog__actions > button.button-vue.button-vue--text-only.button-vue--vue-primary")

class NewEntryModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.name_input: Locator = self.locator(".dialog__content > form > .input-field > .input-field__main-wrapper > input.input-field__input")
        self.create_button: Locator = self.locator(".dialog > .dialog__actions > button.button-vue.button-vue--text-only.button-vue--vue-primary")
        
class OfficeViewer(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.loading_overlay: Locator = self.locator(".office-viewer__loading-overlay")
        
        self.frame: FrameLocator = self.frame_locator(".office-viewer__iframe")
        self.user_welcome_modal: UserWelcomeModal = UserWelcomeModal(self.frame.frame_locator("[name=iframe-welcome-form]"))
        
        self.font_combobox: Locator = self.frame.locator("#fontnamecombobox")
        self.font_combobox_dropdown_button: Locator = self.font_combobox.locator("> .ui-combobox-button")
        self.font_combobox_dropdown: Locator = self.frame.locator("#fontnamecombobox-dropdown")
        self.font_combobox_dropdown_entry: Locator = self.font_combobox_dropdown.locator("#fontnamecombobox-entries > .ui-combobox-entry")
        
        self.content_block: Locator = self.frame.locator("#map")
        
        self.close_button: Locator = self.frame.locator("#closebutton")
    
    def find_font(self, font_name: str):
        return self.font_combobox_dropdown_entry.filter(has=self.page.locator(f"> img[title=\"{font_name}\"], span:text(\"{font_name}\")"))

class UserWelcomeModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.last_slide_indicator: Locator = self.locator("#slide-3-indicator")
        self.last_slide_close_button: Locator = self.locator("#slide-3-button")

class AdminGroupfolderGroupEntry(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.name: Locator = self.locator("> td:not(.permissions)")
        self.permission: Locator = self.locator("> td.permissions > input")
        self.write_permission: Locator = self.permission.nth(0)
        self.share_permission: Locator = self.permission.nth(1)
        self.delete_permission: Locator = self.permission.nth(2)

class AdminGroupfolderEntry(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.mount_point: Locator = self.locator("> td.mountpoint")
        self.groups: Locator = self.locator("> td.groups")
        self.quota_select: Locator = self.locator("> td.quota > div.quotabar-holder > select")
        self.advanced_permissions: Locator = self.locator("> td.acl")
        self.remove: Locator = self.locator("> td.remove")
        
        self.group_edit_button: Locator = self.groups.locator("a")
        self.group_edit: Locator = self.groups.locator("> .group-edit")
        self.group_input: Locator = self.group_edit.locator("input[aria-autocomplete=list]")
    
    def get_group_entry(self, group_name: str):
        return AdminGroupfolderGroupEntry(
            self.group_edit.locator("tbody > tr")
            .filter(has=self.page.locator("> td:not(.permissions)", has_text=group_name))
            .filter(has=self.page.locator("> .permissions"))
        )

class MoveCopyModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.close_button: Locator = self.locator(".modal-container__close.button-vue")
        self.modal_actions: Locator = self.locator(".dialog__actions")
        self.copy_button: Locator = self.modal_actions.locator(".button-vue--vue-primary")
        self.move_button: Locator = self.modal_actions.locator(".button-vue--vue-secondary")
        self.breadcrumbs: Locator = self.locator("ul.breadcrumb__crumbs")
        self.breadcrumb: Locator = self.breadcrumbs.locator("> li")
        self.home_button: Locator = self.breadcrumb.first
        
    def get_folder(self, folder_name: str) -> Locator:
        return self.locator(f"tr[data-testid=\"file-list-row\"][data-filename='{folder_name}']")

class AppPasswordModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.name_input: Locator = self.locator(".token-dialog__name > .input-field > .input-field__main-wrapper > input")
        self.password_input: Locator = self.locator(".token-dialog__password > .input-field > .input-field__main-wrapper > input")

class FileSettingsModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.close_button: Locator = self.locator("button.modal-container__close")
        self.webdav_url_input: Locator = self.locator("#webdav-url-input")

class CreateWorkPackageModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.content: Locator = self.locator(".modal-container__content")
        self.form_wrapper: Locator = self.content.locator(".create-workpackage-form-wrapper")
        
        self.projects_wrapper: Locator = self.form_wrapper.locator("[data-test-id=\"available-projects\"]")
        self.project_input: Locator = self.projects_wrapper.locator("#createWorkPackageInput")
        self.project_select_list: Locator = self.projects_wrapper.locator("ul.vs__dropdown-menu")
        self.project_select_entry: Locator = self.project_select_list.locator("> li.vs__dropdown-option")
        
        self.subject_input: Locator = self.locator(".input-field__input.workpackage-subject")
        
        self.cancel_button: Locator = self.locator("button.create-workpackage-form--button--cancel")
        self.create_button: Locator = self.locator("button.create-workpackage-form--button--create")
    
    def get_project_entry(self, project_name: str):
        return self.project_select_entry.filter(has=self.page.locator("span", has_text=project_name))

class FileConflictModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.dialog: Locator = self.locator(".dialog__modal.conflict-picker")
        self.modal_header: Locator = self.dialog.locator(".modal-header")
        self.modal_content: Locator = self.dialog.locator(".modal-container__content")
        self.modal_description: Locator = self.modal_content.locator(".conflict-picker__description")
        self.select_all_new_files_checkbox: Locator = self.dialog.locator("[data-cy-conflict-picker-input-incoming='all']")
        self.select_all_existing_files_checkbox: Locator = self.dialog.locator("[data-cy-conflict-picker-input-existing='all']")
        self.cancel_button: Locator = self.dialog.locator("[data-cy-conflict-picker-cancel]")
        self.skip_button: Locator = self.dialog.locator("[data-cy-conflict-picker-skip]")
        self.continue_button: Locator = self.dialog.locator("[data-cy-conflict-picker-submit]")

    def get_file_conflict_entry(self, filename: str):
        return self.dialog.locator(f"[data-cy-conflict-picker-fieldset='{filename}']")
    
    def get_incoming_file_checkbox(self, filename: str):
        return self.get_file_conflict_entry(filename).locator(f"[data-cy-conflict-picker-input-incoming='{filename}']")
    
    def get_existing_file_checkbox(self, filename: str):
        return self.get_file_conflict_entry(filename).locator(f"[data-cy-conflict-picker-input-existing='{filename}']")
