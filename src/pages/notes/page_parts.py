# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Locator
from pages.base.base import BasePagePart

class Header(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.menu_wrapper: Locator = self.locator("#central-menu-wrapper")
        self.logo: Locator = self.menu_wrapper.locator("> a[href]")
        self.navigation_button: Locator = self.page.locator("#nav-button")
        self.navigation: Locator = self.page.locator("#nav-content")

class NoteListEntry(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.cell: Locator = self.locator("> td")
        self.name: Locator = self.cell.nth(1)
        self.created_at: Locator = self.cell.nth(2)
        self.updated_at: Locator = self.cell.nth(3)
        self.your_role: Locator = self.cell.nth(4)
        self.members: Locator = self.cell.nth(5)
        self.delete_button: Locator = self.cell.nth(6).locator("button.c__button--icon-only")

class NoteList(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.table: Locator = self.locator(".c__datagrid__table__container > table")
        self.table_entry: Locator = self.table.locator("> tbody > tr")
    
    def get_note_by_name(self, note_name: str) -> NoteListEntry:
        return NoteListEntry(self.table_entry.filter(has=self.page.locator("> td").nth(1).filter(has_text=note_name)))

class Modal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.title: Locator = self.locator(".c__modal__title")
        self.content: Locator = self.locator(".c__modal__content")
        self.footer: Locator = self.locator(".c__modal__footer")
        
        self.primary_button: Locator = self.footer.locator(".c__button--primary")
        self.secondary_button: Locator = self.footer.locator(".c__button--secondary")

class DeleteNoteModal(Modal):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.delete_button: Locator = self.primary_button

class ShareModal(Modal):
    
    def __init__(self, locator):
        super().__init__(locator)
        
        self.share_link_section: Locator = self.content.locator("> div > div:nth-child(1) > div:nth-child(1)")
        
        self.share_visibility_section: Locator = self.content.locator("> div > div:nth-child(1) > div:nth-child(2)")
        self.visibility_select: Locator = self.share_visibility_section.locator(".c__select")
        self.visibility_select_menu: Locator = self.share_visibility_section.locator(".c__select__menu")
        self.visibility_select_item: Locator = self.visibility_select_menu.locator("li.c__select__menu__item")
        self.visibility_select_item_private: Locator = self.visibility_select_item.nth(0)
        self.visibility_select_item_public: Locator = self.visibility_select_item.nth(1)
        self.visibility_select_item_intern: Locator = self.visibility_select_item.nth(2)
        
        self.visibility_type: Locator = self.share_visibility_section.locator(".c__radio__group > .c__checkbox__group__list")
        self.visibility_type_read_only: Locator = self.visibility_type.locator("label.c__checkbox.c__radio").nth(0)
        self.visibility_type_read_write: Locator = self.visibility_type.locator("label.c__checkbox.c__radio").nth(1)
        
        self.share_mail_section: Locator = self.content.locator("> div > div:nth-child(1) > div:nth-child(3)")
        
        self.share_groups_section: Locator = self.content.locator("> div > div:nth-child(2) > div:nth-child(1)")