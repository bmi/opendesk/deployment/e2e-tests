# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Locator
from pages.base.base import BasePage
from .page_parts import *

class NotesPage(BasePage):
    
    def open_page(self, url_portal):
        return self.page.goto(url_portal.replace("portal.", "notes."))
    
    def __init__(self, page):
        super().__init__(page)
        self.container: Locator = self.page.locator("div", has=self.page.locator("> header"))
        self.header: Header = Header(self.container.locator("> header"))
        self.body: Locator = self.container.locator("> div > main")
        
        self.modal_wrapper: Locator = self.page.locator("#c__modals-portal")
        self.current_modal: Locator = self.modal_wrapper.locator("> .ReactModalPortal > div > .ReactModal__Content--after-open")

class NotesListPage(NotesPage):
    
    def get_title(self):
        return self._("page_title")
    
    def __init__(self, page):
        super().__init__(page)
        self.new_note_button: Locator = self.body.locator(".c__button--primary", has_text=self._("new_note_label"))
        self.note_list: NoteList = NoteList(self.body.locator(".c__datagrid"))
        
        self.delete_note_modal: DeleteNoteModal = DeleteNoteModal(self.current_modal)

class EditNotePage(NotesPage):
    
    def get_title(self):
        return self._("page_title_re")
    
    def __init__(self, page):
        super().__init__(page)
        self.warning_alert: Locator = self.page.locator("main").locator(".c__alert.c__alert--warning")
        self.error_alert: Locator = self.page.locator("main").locator(".c__alert.c__alert--error")
        
        self.note_information: Locator = self.page.locator("main > div").first
        self.home_button: Locator = self.note_information.locator("a[href=\"/\"]")
        self.note_name: Locator = self.note_information.locator("h2[contenteditable=\"plaintext-only\"]")
        self.share_button: Locator = self.note_information.locator("button.c__button--primary")
        self.options_button: Locator = self.note_information.locator("button[id^=\"react-aria\"]")
        
        self.note_content: Locator = self.page.locator("main > div").nth(1)
        self.content_editor: Locator = self.note_content.locator(".bn-editor[contenteditable=true]")
        
        self.share_modal: ShareModal = ShareModal(self.current_modal)
        