# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Locator
from pages.base.base import BasePagePart

class Participant(BasePagePart):
    def __init__(self, locator: Locator) -> None:
        super().__init__(locator)

        self.name: Locator = self.locator(".participant-name")

    def get_name(self) -> str | None:
        return self.name.text_content()

class CreateTaskDialog(BasePagePart):
    def __init__(self, locator: Locator) -> None:
        super().__init__(locator)

        self.window_body: Locator = self.locator(".window-body")

        self.extend_form_button: Locator = self.window_body.locator(".expand-link")

        self.contact_row: Locator = self.window_body.locator(".add-participants-view")
        self.pick_contact_button: Locator = self.contact_row.locator(".input-group-btn button")

        self.participants_row: ParticipantsRow = ParticipantsRow(self.window_body.locator(".participantsrow"))


class ParticipantsRow(BasePagePart):
    def __init__(self, locator: Locator) -> None:
        super().__init__(locator)
        self.participants: Locator = self.locator(".participant-wrapper")

    def get_participant_by_name(self, firstname: str, lastname: str) -> Participant:
        return Participant(self.participants.filter(has_text=f"{lastname}, {firstname}"))

