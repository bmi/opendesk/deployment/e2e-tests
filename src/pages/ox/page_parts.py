# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Locator
from pages.base.base import BasePagePart

class Modal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.content: Locator = self.locator(".modal-content")
        self.header: Locator = self.content.locator(".modal-header")
        self.title: Locator = self.header.locator(".modal-title")
        
        self.body: Locator = self.content.locator(".modal-body")
        
        self.footer: Locator = self.content.locator(".modal-footer")

class TopLeftBar(BasePagePart):
    def __init__(self, locator: Locator) -> None:
        super().__init__(locator)
        self.logo: Locator = self.locator("#io-ox-top-logo")
        self.navigation_button: Locator = self.locator("#io-ox-launcher")
        
        self.navigation_dropdown: Locator = self.page.locator(".smart-dropdown-container.dropdown.open.launcher")
        self.navigation_tile: Locator = self.navigation_dropdown.locator("li[role=presentation]")
        self.calendar_navigation_tile: Locator = self.navigation_tile.filter(has=self.page.locator(".lcell > .title", has_text=self._("navigation_calendar_label")))
        self.mail_navigation_tile: Locator = self.navigation_tile.filter(has=self.page.locator(".lcell > .title", has_text=self._("navigation_mail_label")))
        
class SearchBar(BasePagePart):
    def __init__(self, locator: Locator) -> None:
        super().__init__(locator)
        self.container: Locator = self.locator("> .search-container.search-view:visible")
        self.search_button: Locator = self.container.locator("> button.submit-button")
        self.search_input: Locator = self.container.locator("> form.search-field-wrapper > input.search-field")
        self.cancel_search_button: Locator = self.container.locator("> button.cancel-button")
        self.search_drop_down_button: Locator = self.container.locator("> button.dropdown-toggle")
        self.search_drop_down_form: Locator = self.container.locator("> form.dropdown")

class TopRightBar(BasePagePart):
    def __init__(self, locator: Locator) -> None:
        super().__init__(locator)
        self.toggle_notifications_button: Locator = self.locator("#io-ox-notifications-toggle")
        self.address_list_button: Locator = self.locator("> ul > li.launcher", has=self.page.locator("#io-ox-enterprise-picker-icon"))
        self.refresh_button: Locator = self.locator("#io-ox-refresh-icon")
        self.help_button: Locator = self.locator("#io-ox-topbar-help-dropdown-icon")
        self.settings_button: Locator = self.locator("#io-ox-topbar-settings-dropdown-icon")
        self.account_button: Locator = self.locator("#io-ox-topbar-account-dropdown-icon")

class FirstStartModal(Modal):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.no_button: Locator = self.body.locator(".btn.btn-default")

class SaveDraftModal(Modal):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.delete_button: Locator = self.footer.locator("button[data-action=delete]")
        self.cancel_button: Locator = self.footer.locator("button[data-action=cancel]")
        self.save_button: Locator = self.footer.locator("button[data-action=savedraft]")

class FloatingWindow(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.header: Locator = self.locator(".floating-window-content > .floating-header")
        self.tools: Locator = self.header.locator(".controls")
        self.minimize_button: Locator = self.tools.locator("[data-action=\"minimize\"]")
        self.normalize_button: Locator = self.tools.locator("[data-action=\"normalize\"]")
        self.maximize_button: Locator = self.tools.locator("[data-action=\"maximize\"]")
        self.close_button: Locator = self.tools.locator("[data-action=\"close\"]")
        
        self.body: Locator = self.locator(".floating-window-content > .floating-body")     

class Sidebar(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.add_button: Locator = self.locator("> button.btn.btn-primary").first
    
    def is_visible(self):
        return self.page_part_locator.is_visible()

class Sidepanel(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.add_button: Locator = self.locator("> .primary-action > .btn-group > button.btn.btn-primary:not(.dropdown-toggle)")

class Folder(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.node: Locator = self.locator("> .folder-node")
        self.expand_arrow: Locator = self.node.locator("> .folder-arrow") 
        self.subfolders: Locator = self.locator("> ul.subfolders")
    
    def is_expanded(self):
        return self.page_part_locator.get_attribute("aria-expanded") == "true"
    
    def expand(self):
        if not self.is_expanded():
            self.expand_arrow.click()
    
    def collapse(self):
        if self.is_expanded():
            self.expand_arrow.click()

    def get_mail_subfolder(self, name: str):
        return MailFolder(self.subfolders.locator(f"> .folder[aria-label=\"{name}\"]"))
    
    def get_subfolder(self, name: str):
        return Folder(self.subfolders.locator(f"> .folder[aria-label=\"{name}\"]"))

    def open(self):
        self.node.click()

class Dropdown(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.list: Locator = self.locator("ul.dropdown-menu")
        self.list_item: Locator = self.list.locator("li[role=presentation]")

class MailFolder(Folder):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.subfolder: Locator = self.subfolders.locator(".folder")
        
        self.inbox: Folder = Folder(self.subfolders.locator("> .folder[data-id$=\"/INBOX\"]"))
        self.drafts: Folder = Folder(self.subfolder.filter(has=self.page.locator(".folder-icon > .bi.bi-file-earmark-text")))
        self.sent: Folder = Folder(self.subfolder.filter(has=self.page.locator(".folder-icon > .bi.bi-cursor")))
        self.spam: Folder = Folder(self.subfolder.filter(has=self.page.locator(".folder-icon > .bi.bi-bag-x")))
        self.trash: Folder = Folder(self.subfolder.filter(has=self.page.locator(".folder-icon > .bi.bi-trash")))