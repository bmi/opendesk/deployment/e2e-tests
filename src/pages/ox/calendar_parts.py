# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Locator
from pages.base.base import BasePagePart, expect
from pages.ox.page_parts import *
import re

class EditAppointmentFloatingWindow(FloatingWindow):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.title_field: Locator = self.locator("[data-extension-id=\"title\"] > label > input[name=\"summary\"]")
        
        self.start_fieldset: Locator = self.locator("fieldset[data-attribute=\"startDate\"] > .input-group")
        self.start_time_field: Locator = self.start_fieldset.locator("input.time-field")
        
        self.end_fieldset: Locator = self.locator("fieldset[data-attribute=\"endDate\"] > .input-group")
        self.end_time: Locator = self.end_fieldset.locator("input.time-field")
        
        self.save_button: Locator = self.locator(".window-footer > .header.container > [data-action=\"save\"]")
        
        self.attachment_button: Locator = self.locator("[data-section=\"attachments\"]").locator("[data-add=\"attachment\"] > button")
        self.attachment_input: Locator = self.locator("[data-section=\"attachments\"]").locator("[data-add=\"attachment\"] > input")
        self.files_attachment_button: Locator = self.locator("[data-section=\"attachments\"]").locator("button[data-action=\"add-internal\"]")
        self.attachment_list: Locator = self.locator("[data-section=\"attachments\"]").locator("> ul.attachment-list")
        self.attachment_entry: Locator = self.attachment_list.locator("> li.attachment")
        
        self.conference_type_select: Locator = self.locator("[name=\"conference-type\"]")
        self.conference_view: Locator = self.locator("div.conference-view.element")
        self.conference_link: Locator = self.conference_view.locator(".ellipsis > a[rel=\"noopener\"]")
        self.conference_error: Locator = self.conference_view.locator(".bi.bi-exclamation")
        
        self.participants_section: Locator = self.locator(".row > .add-participants-view")
        self.participants_and_resources_input: Locator = self.participants_section.locator(".add-participant.tt-input")
        self.participants_suggestions_list: Locator = self.participants_section.locator(".tt-dropdown-menu > div > .tt-suggestions")
        self.attendee_container: Locator = self.locator(".row > .attendee-container")
        
        self.ignore_conflicts_button: Locator = self.page.locator(".modal-content > .modal-footer > [data-action=\"ignore\"]")
    
    def get_conference_none_type(self) -> str:
        return self.conference_type_select.locator("option[value=\"none\"]").text_content()
    
    def get_conference_videoconference_type(self) -> str:
        return self.conference_type_select.locator("option[value=\"element\"]").text_content()

    def get_suggested_resource(self, resource_name: str):
        return self.participants_suggestions_list.locator("> .tt-suggestion").filter(has=self.page.locator(".avatar.participant-image")).filter(has=self.page.locator(".participant-name", has_text=resource_name))
    
    def get_suggested_participant(self, email: str):
        return self.participants_suggestions_list.locator("> .tt-suggestion").filter(has=self.page.locator(".avatar.participant-image")).filter(has=self.page.locator(".participant-email", has_text=email))
    
    def get_attendee_by_name(self, name: str):
        return self.attendee_container.locator("li", has=self.page.locator(".attendee-name.truncate", has_text=re.compile(f"^{re.escape(name)}")))

    def get_attendee_by_mail(self, email: str):
        return self.attendee_container.locator("li", has=self.page.locator(".attendee-name.truncate", has_text=re.compile(f"<{re.escape(email)}>")))
    
    def get_attachment(self, file: str):
        return self.attachment_entry.filter(has=self.page.locator(f".file > .filename[title=\"{file}\"]"))

class CalendarSidebar(Sidebar):
    
    def __init__(self, locator):
        super().__init__(locator)
        
class CalendarSidepanel(Sidepanel):
    
    def __init__(self, locator):
        super().__init__(locator)

class LayoutDropdownMenu(Dropdown):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.day_view_button: Locator = self.list_item.filter(has=self.page.locator("a[data-value=\"week:day\"]"))
        self.work_week_view_button: Locator = self.list_item.filter(has=self.page.locator("a[data-value=\"week:workweek\"]"))
        self.week_view_button: Locator = self.list_item.filter(has=self.page.locator("a[data-value=\"week:week\"]"))
        self.month_view_button: Locator = self.list_item.filter(has=self.page.locator("a[data-value=\"month\"]"))
        self.year_view_button: Locator = self.list_item.filter(has=self.page.locator("a[data-value=\"year\"]"))
        self.list_view_button: Locator = self.list_item.filter(has=self.page.locator("a[data-value=\"list\"]"))

class AppointmentEntry(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.content: Locator = self.locator(".appointment-content")
        self.title: Locator = self.content.locator("> .title")
        self.attachment_flag: Locator = self.content.locator(".attachments-flag")
        self.participants_flag: Locator = self.content.locator(".participants-flag")
    
    def check_appointment_accepted(self):
        expect(self.page_part_locator).to_have_class(re.compile(r"accepted"))
    
    def check_appointment_declined(self):
        expect(self.page_part_locator).to_have_class(re.compile(r"declined"))
    
    def check_appointment_needs_action(self):
        expect(self.page_part_locator).to_have_class(re.compile(r"needs-action"))
    
    def check_appointment_tentative(self):
        expect(self.page_part_locator).to_have_class(re.compile(r"tentative"))
    
    def get_data_timestamp(self) -> str:
        return self.page_part_locator.get_attribute("data-timestamp")
    
    def get_start_timestamp(self) -> int:
        timestamp: str = self.get_data_timestamp()
        timestamp_length = len(timestamp)
        return int(timestamp[:timestamp_length//2])

    def get_end_timestamp(self) -> int:
        timestamp: str = self.get_data_timestamp()
        timestamp_length = len(timestamp)
        return int(timestamp[timestamp_length//2:])        

class Calendar(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.calendar_header: Locator = self.locator(".calendar-header")
        self.layout_dropdown_button: Locator = self.calendar_header.locator(".layout-dropdown")
        self.layout_dropdown_menu: LayoutDropdownMenu = LayoutDropdownMenu(self.page.locator(".smart-dropdown-container.dropdown.open.layout-dropdown"))
        
        self.appointment_container: Locator = self.locator(".appointment-container > .scrollpane")
        self.day_div: Locator = self.appointment_container.locator(".day")
        
    def get_appointment(self, title: str) -> AppointmentEntry:
        return AppointmentEntry(self.day_div.locator(".appointment", has=self.page.locator(f".appointment-content[title=\"{title}\"]")))

class AppointmentDetailParticipantEntry(BasePagePart):
    def __init__(self, locator):
        super().__init__(locator)
        self.status_icon: Locator = self.locator(".status > svg")
        self.email_link: Locator = self.locator("a[data-detail-popup=\"halo\"]")
        self.last_name: Locator = self.email_link.locator(".last_name")
        self.first_name: Locator = self.email_link.locator(".first_name")
        self.comment: Locator = self.locator(".comment")

    def check_participant_accepted(self) -> bool:
        expect(self.page_part_locator).to_have_class(re.compile(r"accepted"))

    def check_participant_declined(self) -> bool:
        expect(self.page_part_locator).to_have_class(re.compile(r"declined"))

    def check_participant_tentative(self) -> bool:
        expect(self.page_part_locator).to_have_class(re.compile(r"tentative"))

    def check_participant_needs_action(self) -> bool:
        expect(self.page_part_locator).to_have_class(re.compile(r"needs-action"))

class AppointmentDetailPopup(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.header: Locator = self.locator(".popup-header")
        self.delete_button: Locator = self.header.locator("button[data-action=\"io.ox/calendar/detail/actions/delete\"]")
        self.close_button: Locator = self.header.locator("button[data-action=close]")
        
        self.body: Locator = self.locator(".popup-body")
        self.participant_entry: Locator = self.body.locator(".participant-list > li > ul > li.participant")
        
        self.footer: Locator = self.locator(".popup-footer")
        self.accept_button: Locator = self.footer.locator("button[data-action=\"participate/yes\"]")
        self.decline_button: Locator = self.footer.locator("button[data-action=\"participate/no\"]")
        self.tentative_button: Locator = self.footer.locator("button[data-action=\"participate/maybe\"]")

    def get_participant(self, email: str) -> AppointmentDetailParticipantEntry:
        return AppointmentDetailParticipantEntry(self.participant_entry.filter(has=self.page.locator(f"a[title=\"{email}\"]")))
