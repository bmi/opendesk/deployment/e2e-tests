# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
from pages.base.base import expect
import re
from pages.base.base import HybridBasePage

class JitsiPage(HybridBasePage):
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.topbar: Locator = self.locator("#opendeskTopbar")
        self.topbar_logo: Locator = self.topbar.locator("> a[target=\"_blank\"]")
        self.topbar_navigation: Locator = self.locator("#opendeskMenuButton")
        self.topbar_navigation_menu: Locator = self.locator("#opendeskMenu")
    
    def open_page(self, url_portal):
        self.page.goto(url_portal.replace("portal.", "meet."))

class JitsiHomePage(JitsiPage):
    
    def get_title(self) -> str | re.Pattern[str] | None:
        return self._("page_title")
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.settings_button: Locator = self.locator("#welcome_page").locator(".welcome-page-settings")
        self.enter_room: Locator = self.locator("#enter_room")
        self.enter_room_input: Locator = self.enter_room.locator("#enter_room_field")
        self.enter_room_button: Locator = self.enter_room.locator("#enter_room_button")

class JitsiJoinPage(JitsiPage):
    
    def get_title(self) -> str | re.Pattern[str] | None:
        return re.compile(" | openDesk Videoconference$")
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.display_name_field: Locator = self.locator("#premeeting-name-input")
        self.toolbox: Locator = self.locator("#new-toolbox")
        self.toolbox_items: Locator = self.toolbox.locator(".toolbox-content-items")
        self.audio_preview: Locator = self.toolbox_items.locator("> .audio-preview")
        self.audio_preview_button: Locator = self.audio_preview.locator(".settings-button-container > [role=\"button\"]")
        self.audio_preview_button_enabled: Locator = self.audio_preview.locator(".settings-button-container > [role=\"button\"][aria-pressed=\"false\"]")
        self.audio_preview_button_disabled: Locator = self.audio_preview.locator(".settings-button-container > [role=\"button\"][aria-pressed=\"true\"]")
        self.audio_preview_settings: Locator = self.audio_preview.locator(".settings-button-container > div > .settings-button-small-icon")
        self.video_preview: Locator = self.toolbox_items.locator("> .video-preview")
        self.video_preview_button: Locator = self.video_preview.locator(".settings-button-container > [role=\"button\"]")
        self.video_preview_button_enabled: Locator = self.video_preview.locator(".settings-button-container > [role=\"button\"][aria-pressed=\"false\"]")
        self.video_preview_button_disabled: Locator = self.video_preview.locator(".settings-button-container > [role=\"button\"][aria-pressed=\"true\"]")
        self.video_preview_settings: Locator = self.video_preview.locator(".settings-button-container > div > .settings-button-small-icon")
        self.join_button: Locator = self.locator("[data-testid=\"prejoin.joinMeeting\"]")
        self.join_options: Locator = self.join_button.locator("[data-testid=\"prejoin.joinOptions\"]")
        self.join_option_no_audio: Locator = self.locator("[data-testid=\"prejoin.joinWithoutAudio\"]")
        wait_for_moderator_dialog_title: str = self._("waiting_for_moderator_dialog_title")
        self.wait_for_moderator_dialog: Locator = self.locator(f"[role=\"dialog\"][aria-label=\"{wait_for_moderator_dialog_title}\"]")

    def get_videoconference_name(self):
        return self.page.title().split(" | ")[0]

    def validate(self) -> None:
        super().validate()
        expect(self.display_name_field).to_be_visible(timeout=20000)
        expect(self.toolbox_items).to_be_visible(timeout=20000)
        expect(self.join_button).to_be_visible(timeout=20000)

class JitsiRoomPage(JitsiPage):
    
    def get_title(self) -> str | re.Pattern[str] | None:
        return re.compile(" | openDesk Videoconference$")
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.toolbox: Locator = self.locator("#new-toolbox")
        self.toolbox_items: Locator = self.toolbox.locator(".toolbox-content-items")
        self.video_space: Locator = self.locator("#videospace")
        self.dominant_speaker: Locator = self.locator("#dominantSpeaker")
        self.large_video_wrapper: Locator = self.locator("#largeVideoWrapper")
        self.filmstrip_videos: Locator = self.locator("#remoteVideos")
        self.subject_info: Locator = self.locator(".subject > .subject-info-container")
        self.subject_info_name: Locator = self.subject_info.locator(".subject-text--content")
        self.subject_info_timer: Locator = self.subject_info.locator("> span")
        self.resolution_settings: Locator = self.locator("#videoResolutionLabel")
        self.participants_button: Locator = self.toolbox_items.locator(".toolbar-button-with-badge").filter(has=self.page.locator("[aria-label*=\"" + self._("participants_label_re") + "\"]"))
        self.participants_pane: Locator = self.locator(".participants_pane")
        self.participants_pane_header: Locator = self.participants_pane.locator("> [class$=-header]")
        self.participants_pane_close_button: Locator = self.participants_pane_header.locator("> button")
        self.participants_pane_container: Locator = self.participants_pane.locator("> [class$=-container]")
        self.participants_pane_footer: Locator = self.participants_pane.locator("> [class$=-footer]")
    
    def get_filmstrip_by_user(self, displayname: str):
        return self.filmstrip_videos.locator(".videocontainer", has=self.page.locator("span.displayname", has_text=displayname))
    
    def get_participant(self, name: str | re.Pattern[str]):
        return self.participants_pane_container.locator(".list-item-container", has=self.page.locator("[class$=-nameContainer] > div", has_text=name))
    
    def get_videoconference_url(self):
        return self.page.url
    
    def get_videoconference_name(self):
        return self.page.title().split(" | ")[0]
    
    def validate(self) -> None:
        super().validate()
        expect(self.locator(".premeeting-screen")).not_to_be_visible(timeout=30000)
        expect(self.toolbox_items).to_be_visible()
    
    def move_mouse(self):
        box = self.video_space.bounding_box()
        x, y, w, h = box["x"], box["y"], box["width"], box["height"]
        x1, y1 = x + (w/2) - 10, y + (h/2) - 10
        x2, y2 = x1 + 20, y1+20
        self.page.mouse.move(x=x1, y=y1)
        self.page.mouse.move(x=x2, y=y2, steps=50)
