<!---
SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

**Content / Quick navigation**

[[_TOC_]]

# Test Plan

This is the system test suite of the [openDesk](https://gitlab.opencode.de/bmi/souveraener_arbeitsplatz). openDesk integrates multiple open-source products called _apps_ within this document.

## Tests structure

For further information about the structure of the tests see the README.md in the root of this repository.

## What to test

System tests are end-to-end tests and therefore are only performed through the frontend, in a web browser. They mimic the behaviour of a user and can be modeled in classical use cases.

Scenarios to be at least tested by this test suite for each app:

* Most apps use provisioned services like databases, caches, etc..

   __Are the apps reachable and able to use the provisioned services without fault?__

* Some apps use direct or non-direct communication to other apps in order to accomplish some tasks.

  __Are the interoperability scenarios functional?__

* Some features are specific to openDesk, so we want to ensure they are enabled and working as expected on a smoke-test level.

  __Are openDesk specific features enabled and functional?__

Depending on its technical character there may be additional test cases that are vital to ensure the proper deployment and integration of an app and its required surroundings.

### List of apps

#### Directly testable apps

These apps have an accessible frontend that provides functionality which can be tested directly.

* Collabora
* Element
* Jitsi
* Keycloak
* Nextcloud
* OX AppSuite
* Openproject
* Univention Management Stack with Portal and Admin
* XWiki

#### Indirect testable apps

These apps provide services which are not directly accessible via a dedicated frontend. They are tested implicitly.

* Intercom-service
* Dovecot
* Services

## What not to test

### Producer/ Vendor Tests

Regarding the apps it is assumed that the respective producers test them as part of their development and quality assurance process.
So it is not the task of this test suite to go into detail of the apps, but to test the results of the integration process implemented by openDesk.
Due to this the apps are not tested exhaustively in full depth.

### Security

Security- and vulnerability-tests and -scans are performed on another level of the integration and deployment process.

### Performance

The performance of the deployment is heavily dependent on the infrastructure and on the properties of the Kubernetes cluster. Also performance tests ideally do not use the browser to run the testcases. Therefore performance testing is out of scope of this test suite.

### Destructive tests

As well as regarding performance tests it is not in the scope of this test suite to detect boundaries in any kind of behaviour apart from normal use cases.

# Common information for all test cases

## Acknowledgements

Every test should:
* Ensure that it is idempotent. That means:
  * Clean up: every bit that was created on a system has to get removed after the test.
  * Clean up: no matter how the test resulted.
* Be independent of other tests.
* Use random names / labels to avoid conflicts in case multiple tests are ran at the same time against the same system.

**openDesk is currently in development state. Therefore, it is considered more important to reach solid test coverage than to fulfill this cleanup ruless.**

### URL

The `URL` of an app is composed of these parts: `https://<app_alias>.<domain>`

The `Login-URL` is always `https://portal.<domain>`

### Language

openDesk is tested in German.

### Credentials

Further details on how to retrieve the passwords can be found in the [README.md of the deployment-automation](https://gitlab.opencode.de/bmi/souveraener_arbeitsplatz/deployment/sovereign-workplace/-/blob/main/README.md#logging-in).

The credentials consist of these two pairs:
* regular user name: `default.user`
* regular user password: `<...>`
* admin user name: `default.admin`
* admin user password: `<...>`

### Login

The login process always contains these steps

1. Go to [Login-URL](#url) and check if it has been loaded without error
2. Accept the `Cookie Consent`
3. Click `Login`
4. Check if the login form is shown
5. Log in with `default.user` or `default.admin`
6. Check if login was successful
7. Change language to German

### Logout

Logout includes also cleaning up if some resources have been created or changed, [see](#acknowledgements).

# Test cases per component 

* [Collabora](doc/component_plans/collabora.md)
* [Element](doc/component_plans/element.md)
* [Jitsi](doc/component_plans/jitsi.md)
* [Nextcloud](doc/component_plans/nextcloud.md)
* [OpenProject](doc/component_plans/openproject.md)
* [OX Appsuite](doc/component_plans/ox_appsuite.md)
* [UMS](doc/component_plans/ums.md)
* [XWiki](doc/component_plans/xwiki.md)
