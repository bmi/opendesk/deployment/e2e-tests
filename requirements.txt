# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

playwright==1.50.0
PyYAML==6.0.2
pyright==1.1.394
pytest==8.3.4
pytest-playwright==0.7.0
pytest-html==4.1.1
pytest-dependency==0.6.0
pytest-order==1.3.0
pytest-xdist==3.6.1
allure-pytest==2.13.5
typing==3.7.4.3
imap-tools==1.10.0
pyotp==2.9.0
odsgenerator==1.11.1
dill==0.3.9
filelock==3.17.0
webdavclient3==3.14.6
