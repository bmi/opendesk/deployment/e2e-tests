:: SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
:: SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
:: SPDX-License-Identifier: Apache-2.0

@echo off

:: VIRTUALENV=$(command -v virtualenv)
SET VENV=venv
python -m venv %VENV%
call "%VENV%/Scripts/activate.bat"

:: use this for debugging:
:: pytest -s tests/ --headed --config config.ini -v "."

pytest --headed --tracing on "." --order-dependencies --dist=loadfile --config config.ini -k "test_login_18_login_invalid_username" %*
