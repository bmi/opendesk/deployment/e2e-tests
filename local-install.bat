:: SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
:: SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
:: SPDX-License-Identifier: Apache-2.0

@echo off
:: To not pollute the local system, virtualenv is harnessed.
WHERE pip
IF %ERRORLEVEL% NEQ 0 (
  echo "pip of Python3 needs to be installed. You can easily install it:"
  echo -e "\nsudo apt-get update && sudo apt-get install -y python3-pip\n"
  exit 1
)
WHERE virtualenv
IF %ERRORLEVEL% NEQ 0 (
  echo "Virtualenv needs to be installed. You can easily install it:"
  echo -e "\npip install virtualenv\n"
  exit 1
)

:: VIRTUALENV=$(command -v virtualenv)
SET VENV=venv
virtualenv %VENV%
call "%VENV%/Scripts/activate.bat"
pip install -r requirements.txt
playwright install
playwright install-deps
pip install -r requirements.txt

git clone "https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import.git" "git_clone/user-import"