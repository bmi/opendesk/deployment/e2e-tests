# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

#!/bin/bash

rm -rf assets/ geckodriver.log report.html screenshots/ .pytest_cache/

# this removes the __pycache__s:
find . -type d -name __pycache__ -exec rm -r {} \+

rm -rf .pytest_cache/
rm -rf venv/
rm -rf src/pages/.mypy_cache/
