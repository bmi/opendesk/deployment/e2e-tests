:: SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
:: SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
:: SPDX-License-Identifier: Apache-2.0

@echo off

rmdir /s /q assets geckodriver.log report.html screenshots .pytest_cache

:: this removes the __pycache__s:
:: find . -type d -name __pycache__ -exec rm -r {} \+

rmdir /s /q .pytest_cache
rmdir /s /q venv
rmdir /s /q src\pages\.mypy_cache\
