# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import re
from playwright.sync_api import Page
from pages.nextcloud.page import NextcloudPage
from tests.config.config import Config
from tests.steps import steps_mail, steps_nextcloud, steps_webdav
from tests.steps import steps_mail, steps_nextcloud, steps_webdav
import allure
import pytest
import os
from imap_tools import MailBox
from webdav3.client import Client
from webdav3.client import Client

from tests.utils import retry

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "UI")
@allure.title("Nextcloud: General availability")
@pytest.mark.dependency(name="check_logged_in_page")
@pytest.mark.user("component_admin")
def test_nextcloud_logged_in_page(logged_in_page: Page, url_portal: str):
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_general_availability(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "Upload")
@allure.title("Nextcloud: Upload and open office files")
@pytest.mark.dependency(name="check_files_upload")
@pytest.mark.user("component_admin")
@pytest.mark.files({key: 1 for key in [".docx", ".pptx", ".xlsx", ".odp", ".ods", ".odt"]})
def test_nextcloud_office_files_upload(logged_in_page: Page, url_portal: str, temp_files: tuple[str, list]):
    temp_folder, files = temp_files
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    exceptions: list[Exception] = []
    fallback_url: str = logged_in_page.url
    
    for temp_demo_file in files:
        try:
            with allure.step(f"Test file upload: '{temp_demo_file}'"):
                steps_nextcloud.step_upload_file(page=logged_in_page, folder=temp_folder, filename=temp_demo_file)
                steps_nextcloud.step_check_file_exists(page=logged_in_page, filename=temp_demo_file)
                steps_nextcloud.step_open_file(page=logged_in_page, filename=temp_demo_file)
                steps_nextcloud.step_check_office_viewer_opened(page=logged_in_page)
                steps_nextcloud.step_close_document(page=logged_in_page)
                steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=temp_demo_file)
                steps_nextcloud.step_click_delete(page=logged_in_page)
                steps_nextcloud.step_check_file_not_exists(page=logged_in_page, filename=temp_demo_file)
        except Exception as exception:
            exceptions.append(exception)
            logged_in_page.goto(fallback_url)
            NextcloudPage(logged_in_page).validate()
    
    if exceptions:
        pytest.fail("Exceptions were thrown while upload files.")

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "Upload")
@allure.title("Nextcloud: Upload and open cryptpad file")
@pytest.mark.user("component_admin")
@pytest.mark.files({".drawio": 1})
def test_nextcloud_drawio_file_upload(logged_in_page: Page, url_portal: str, temp_files: tuple[str, list]):
    temp_folder, files = temp_files
    filename: str = files[0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_upload_file(page=logged_in_page, folder=temp_folder, filename=filename)
    steps_nextcloud.step_check_file_exists(page=logged_in_page, filename=filename)
    steps_nextcloud.step_open_file(page=logged_in_page, filename=filename)
    steps_nextcloud.step_check_cryptpad_editor_opened(page=logged_in_page)
    steps_nextcloud.step_close_cryptpad(page=logged_in_page)
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=filename)
    steps_nextcloud.step_click_delete(page=logged_in_page)
    steps_nextcloud.step_check_file_not_exists(page=logged_in_page, filename=filename)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "Create file")
@allure.title("Nextcloud: Create cryptpad file")
@pytest.mark.dependency(name="check_create_cryptpad_file")
@pytest.mark.user("component_admin")
def test_nextcloud_create_cryptpad_file(logged_in_page: Page, url_portal: str):
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_create_new_cryptpad_file(page=logged_in_page)
    filename: str = steps_nextcloud.step_check_cryptpad_editor_opened(page=logged_in_page)
    steps_nextcloud.step_close_cryptpad(page=logged_in_page)
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=filename)
    steps_nextcloud.step_click_delete(page=logged_in_page)
    steps_nextcloud.step_check_file_not_exists(page=logged_in_page, filename=filename)

@allure.epic("Smoke", "Load")
@allure.feature("Nextcloud")
@allure.story("User", "Create file")
@allure.title("Nextcloud: Create document")
@pytest.mark.dependency(name="check_create_document")
@pytest.mark.user("component_admin")
def test_nextcloud_create_document(logged_in_page: Page, url_portal: str, run_id: str):
    filename: str = f"{run_id}-new-doc.odt"
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_create_document(page=logged_in_page, filename=filename)
    steps_nextcloud.step_check_office_viewer_opened(page=logged_in_page)
    steps_nextcloud.step_close_document(page=logged_in_page)
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=filename)
    steps_nextcloud.step_click_delete(page=logged_in_page)
    steps_nextcloud.step_check_file_not_exists(page=logged_in_page, filename=filename)
    
@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share")
@allure.title("Nextcloud: Share file (Internal)")
@pytest.mark.dependency(name="check_internal_file_sharing")
@pytest.mark.user("component_admin")
@pytest.mark.users_dependency(["user"])
@pytest.mark.files({re.compile(r"\.od[tps]$"): 1})
def test_nextcloud_internal_file_sharing(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], user: Config.User):
    _, files = nextcloud_files
    
    file_1 = files[0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=user.username, mail=user.email)
    steps_nextcloud.step_click_save_share_button(page=logged_in_page)
    steps_nextcloud.step_intern_shared_entry_visible(page=logged_in_page, mail=user.email)
    steps_nextcloud.step_close_sidebar_right(page=logged_in_page)
    
@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share", "Profile")
@allure.title("Nextcloud: Share file (External)")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.user("user")
@pytest.mark.files({re.compile(r"\.od[tps]$"): 1})
def test_nextcloud_external_file_sharing(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str):
    _, files = nextcloud_files
    
    file_1 = files[0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)

    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=autotest_mail, mail=autotest_mail)
    steps_nextcloud.step_click_save_share_button(page=logged_in_page)
    steps_nextcloud.step_extern_shared_entry_visible(page=logged_in_page, mail=autotest_mail)
    steps_nextcloud.step_close_sidebar_right(page=logged_in_page)
    
@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share", "Profile")
@allure.title("Nextcloud: Share recommendation (External)")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.user("user")
@pytest.mark.files({re.compile(r"\.od[tps]$"): 3})
def test_nextcloud_external_file_sharing_recommend(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str):
    _, files = nextcloud_files
    
    file_1 = files[0]
    file_2 = files[1]
    file_3 = files[2]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)

    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=autotest_mail, mail=autotest_mail)
    steps_nextcloud.step_click_save_share_button(page=logged_in_page)
    steps_nextcloud.step_extern_shared_entry_visible(page=logged_in_page, mail=autotest_mail)
    steps_nextcloud.step_close_sidebar_right(page=logged_in_page)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_2)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=autotest_mail, mail=autotest_mail)
    steps_nextcloud.step_click_save_share_button(page=logged_in_page)
    steps_nextcloud.step_extern_shared_entry_visible(page=logged_in_page, mail=autotest_mail)
    steps_nextcloud.step_close_sidebar_right(page=logged_in_page)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_3)
    steps_nextcloud.step_check_share_file_recommend(page=logged_in_page, mail=autotest_mail)
    
@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share", "Profile")
@allure.title("Nextcloud: Share file (External, Disabled)")
@pytest.mark.dependency(name="check_no_external_file_sharing")
@pytest.mark.env_setting("env_external_sharing", False)
@pytest.mark.user("user")
@pytest.mark.files({re.compile(r"\.od[tps]$"): 1})
def test_nextcloud_no_external_file_sharing(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str): # TODO Use mail domain to generate a new email address per test
    _, files = nextcloud_files
    
    file_1: str = files[0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_no_share_file(page=logged_in_page, mail=autotest_mail)
    
@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "Edit file")
@allure.title("Nextcloud: Edit file")
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_nextcloud_edit_file(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list]):
    _, files = nextcloud_files
    
    file_1: str = files[0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_file(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_check_office_viewer_opened(page=logged_in_page)
    steps_nextcloud.step_edit_office_text_file(page=logged_in_page)
    steps_nextcloud.step_close_document(page=logged_in_page)
    steps_nextcloud.step_check_file_edit_seconds_ago(page=logged_in_page, filename=file_1)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "Upload", "Security")
@allure.title("Nextcloud: Upload malware positive file (Upload blocked)")
@pytest.mark.user("user")
@pytest.mark.files({"eicar.txt": 1})
def test_nextcloud_no_malware_upload(logged_in_page: Page, url_portal: str, temp_files: tuple[str, list]):
    temp_folder, files = temp_files
    
    file_1: str = files[0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_upload_file(page=logged_in_page, folder=temp_folder, filename=file_1)
    steps_nextcloud.step_check_file_upload_error(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share", "Profile")
@allure.title("Nextcloud: Share file with password (Share mail received, Password mail received)")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.env_setting("env_send_password_mail", True)
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_nextcloud_share_file_with_password(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str, mailbox: MailBox, config: Config):
    fileshare_url: str = url_portal.replace("portal.", "files.")
    if not fileshare_url.endswith("/"): fileshare_url += "/"
    fileshare_url += "s/"
    
    _, files = nextcloud_files
    
    file_1: str = files[0]
    
    share_password: str = config.generate_password()
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=autotest_mail, mail=autotest_mail)
    steps_nextcloud.step_sharing_enable_password(page=logged_in_page)
    steps_nextcloud.step_sharing_fill_password(page=logged_in_page, password=share_password)
    steps_nextcloud.step_click_save_share_button(page=logged_in_page)
    steps_nextcloud.step_extern_shared_entry_visible(page=logged_in_page, mail=autotest_mail)
    steps_nextcloud.step_close_sidebar_right(page=logged_in_page)
    
    steps_mail.step_receive_email(mailbox=mailbox, subject=f"{file_1}", body=fileshare_url) # To contain link to file
    steps_mail.step_receive_email(mailbox=mailbox, subject=f"{file_1}", body=share_password) # To contain password

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share", "Profile")
@allure.title("Nextcloud: Share file with password (Share mail received, Password mail not received)")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.env_setting("env_send_password_mail", False)
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_nextcloud_share_file_with_password_no_mail(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str, mailbox: MailBox, config: Config):
    fileshare_url: str = url_portal.replace("portal.", "files.")
    if not fileshare_url.endswith("/"): fileshare_url += "/"
    fileshare_url += "s/"
    
    _, files = nextcloud_files
    
    file_1: str = files[0]
    
    share_password: str = config.generate_password()
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=autotest_mail, mail=autotest_mail)
    steps_nextcloud.step_sharing_enable_password(page=logged_in_page)
    steps_nextcloud.step_sharing_fill_password(page=logged_in_page, password=share_password)
    steps_nextcloud.step_click_save_share_button(page=logged_in_page)
    steps_nextcloud.step_extern_shared_entry_visible(page=logged_in_page, mail=autotest_mail)
    steps_nextcloud.step_close_sidebar_right(page=logged_in_page)
    
    steps_mail.step_receive_email(mailbox=mailbox, subject=f"{file_1}", body=fileshare_url) # To contain link to file
    steps_mail.step_not_receive_email(mailbox=mailbox, subject=f"{file_1}", body=share_password) # To contain password

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share", "Profile")
@allure.title("Nextcloud: Password enforced")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.env_setting("env_enforce_password", True)
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_nextcloud_share_file_enforce_password(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str):
    _, files = nextcloud_files
    
    file_1: str = files[0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=autotest_mail, mail=autotest_mail)
    steps_nextcloud.step_sharing_password_enforced(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share", "Profile")
@allure.title("Nextcloud: Password not enforced")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.env_setting("env_enforce_password", False)
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_nextcloud_share_file_not_enforce_password(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str):
    _, files = nextcloud_files
    
    file_1: str = files[0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=autotest_mail, mail=autotest_mail)
    steps_nextcloud.step_sharing_password_not_enforced(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share", "Profile")
@allure.title("Nextcloud: Expiry set by default")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.env_setting("env_expiry_active_default", True)
@pytest.mark.env_setting("env_expiry_enforced", False)
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_nextcloud_share_file_expiry_active(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str):
    _, files = nextcloud_files
    
    file_1: str = files[0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=autotest_mail, mail=autotest_mail)
    steps_nextcloud.step_sharing_expiry_checked(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share", "Profile")
@allure.title("Nextcloud: Expiry not set by default")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.env_setting("env_expiry_active_default", False)
@pytest.mark.env_setting("env_expiry_enforced", False)
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_nextcloud_share_file_expiry_not_active(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str):
    _, files = nextcloud_files
    
    file_1: str = files[0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=autotest_mail, mail=autotest_mail)
    steps_nextcloud.step_sharing_expiry_not_checked(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share", "Profile")
@allure.title("Nextcloud: Expiry enforced")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.env_setting("env_expiry_enforced", True)
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_nextcloud_share_file_expiry_enforced(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str):
    _, files = nextcloud_files
    
    file_1: str = files[0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=autotest_mail, mail=autotest_mail)
    steps_nextcloud.step_sharing_expiry_enforced(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share", "Profile")
@allure.title("Nextcloud: Expiry not enforced")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.env_setting("env_expiry_enforced", False)
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_nextcloud_share_file_expiry_not_enforced(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str):
    _, files = nextcloud_files
    
    file_1: str = files[0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=autotest_mail, mail=autotest_mail)
    steps_nextcloud.step_sharing_expiry_not_enforced(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User", "File share", "Profile")
@allure.title("Nextcloud: Default expiry days")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.env_setting("env_expiry_default_days", lambda value: value is not None)
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_nextcloud_share_file_default_expiry_days(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str, config: Config):
    _, files = nextcloud_files
    
    file_1: str = files[0]
    
    default_days: int = config.env_expiry_default_days
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=file_1)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=autotest_mail, mail=autotest_mail)
    steps_nextcloud.step_sharing_check_expiry(page=logged_in_page)
    steps_nextcloud.step_sharing_check_expiry_days(page=logged_in_page, days=default_days)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Upload new font")
@pytest.mark.dependency(name="font_upload")
@pytest.mark.files({"Lato.ttf": 1}) # Make sure to use full font-file name and that the name of the file is the same as the font name presented in the UI.
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("font-group")
def test_nextcloud_new_font_upload(logged_in_page: Page, url_portal: str, files_dict: dict, temp_files: tuple[str, list], config: Config):
    folder, files = temp_files
    
    font_file: str = files[0]
    font_name: str = os.path.splitext(list(files_dict.keys())[0])[0]
    
    steps_nextcloud.step_open_rich_documents_settings_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_upload_font(page=logged_in_page, folder=folder, font_file=font_file)
    steps_nextcloud.step_check_font_visible(page=logged_in_page, font_file=font_file)
    
    config.save("CustomFontFilename", font_file)
    config.save("CustomFontName", font_name)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Use new font")
@pytest.mark.dependency(depends=["font_upload"])
@pytest.mark.files({".odt": 1})
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("font-group")
def test_nextcloud_new_font_test(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], config: Config):
    _, files = nextcloud_files
    
    filename: str = files[0]
    font_name: str | None = config.load("CustomFontName")
    assert font_name != None
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_open_file(page=logged_in_page, filename=filename)
    steps_nextcloud.step_check_office_viewer_opened(page=logged_in_page)
    retry_wrapper = retry(trys=5, retry_timeout=30, on_fail=steps_nextcloud.reopen_file_wrapper(url_portal=url_portal, filename=filename))
    retry_wrapper(steps_nextcloud.step_check_office_font_exists)(page=logged_in_page, font_name=font_name)
    
@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Delete new font")
@pytest.mark.dependency(depends=["font_upload"])
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("font-group")
def no_test_nextcloud_new_font_delete(logged_in_page: Page, url_portal: str, config: Config): # Delete font is not working with multiple threads
    font_file: str | None = config.load("CustomFontFilename")
    assert font_file != None
    
    steps_nextcloud.step_open_rich_documents_settings_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_check_font_visible(page=logged_in_page, font_file=font_file)
    steps_nextcloud.step_click_delete_font(page=logged_in_page, font_file=font_file)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Groupfolder")
@pytest.mark.user("component_admin")
def test_nextcloud_create_groupfolders(logged_in_page: Page, url_portal: str, run_id: str):
    groupfolder_name: str = f"Groupfolder-{run_id}"
    test_file: str = f"test-{run_id}.odt"
    
    steps_nextcloud.step_open_admin_groupfolders_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_fill_groupfolder_name(page=logged_in_page, groupfolder_name=groupfolder_name)
    steps_nextcloud.step_click_create_groupfolder(page=logged_in_page)
    steps_nextcloud.step_check_groupfolder_exists(page=logged_in_page, groupfolder_name=groupfolder_name)
    steps_nextcloud.step_click_open_groupfolder_groups(page=logged_in_page, groupfolder_name=groupfolder_name)
    steps_nextcloud.step_add_group_to_groupfolder(page=logged_in_page, groupfolder_name=groupfolder_name, group_name="managed-by-attribute-FileshareAdmin")
    steps_nextcloud.step_remove_permission_share_from_group(page=logged_in_page, groupfolder_name=groupfolder_name, group_name="managed-by-attribute-FileshareAdmin")
    steps_nextcloud.step_remove_permission_delete_from_group(page=logged_in_page, groupfolder_name=groupfolder_name, group_name="managed-by-attribute-FileshareAdmin")
    steps_nextcloud.step_close_groups(page=logged_in_page)
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_check_folder_exists(page=logged_in_page, foldername=groupfolder_name)
    steps_nextcloud.step_open_folder(page=logged_in_page, foldername=groupfolder_name)
    steps_nextcloud.step_create_document(page=logged_in_page, filename=test_file)
    steps_nextcloud.step_check_office_viewer_opened(page=logged_in_page)
    steps_nextcloud.step_close_document(page=logged_in_page)
    steps_nextcloud.step_check_share_not_available(page=logged_in_page, filename=test_file)
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=test_file)
    steps_nextcloud.step_check_delete_not_available(page=logged_in_page)
    

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Zip files")
@pytest.mark.user("component_admin")
@pytest.mark.files({".odt": 2})
def test_nextcloud_zip_files(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], run_id: str):
    _, files = nextcloud_files
    zip_filename: str = f"zip-{run_id}.zip"
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    for file in files:
        steps_nextcloud.step_toggle_select_file(page=logged_in_page, filename=file)
        steps_nextcloud.step_check_file_selected(page=logged_in_page, filename=file)
    
    steps_nextcloud.step_click_batch_actions(page=logged_in_page)
    steps_nextcloud.step_click_zip_files(page=logged_in_page)
    steps_nextcloud.step_fill_zip_file_name(page=logged_in_page, filename=zip_filename)
    steps_nextcloud.step_click_compress_files_button(page=logged_in_page)
    
    steps_nextcloud.step_check_zip_file_created(page=logged_in_page, filename=zip_filename)
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=zip_filename)
    steps_nextcloud.step_click_delete(page=logged_in_page)
    steps_nextcloud.step_check_file_not_exists(page=logged_in_page, filename=zip_filename)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Rename file")
@pytest.mark.user("component_admin")
@pytest.mark.files({".odt": 1})
def test_nextcloud_rename_file(logged_in_page: Page, url_portal: str, run_id: str, temp_files: tuple[str, list]):
    folder, files = temp_files
    filename: str = files[0]
    new_filename: str = f"renamed-{run_id}.odt"
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_upload_file(page=logged_in_page, folder=folder, filename=filename)
    steps_nextcloud.step_check_file_exists(page=logged_in_page, filename=filename)
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=filename)
    steps_nextcloud.step_click_rename_file(page=logged_in_page)
    steps_nextcloud.step_rename_file(page=logged_in_page, old_filename=filename, filename=new_filename)
    
    steps_nextcloud.step_check_file_not_exists(page=logged_in_page, filename=filename)
    steps_nextcloud.step_check_file_exists(page=logged_in_page, filename=new_filename)
    
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=new_filename)
    steps_nextcloud.step_click_delete(page=logged_in_page)
    steps_nextcloud.step_check_file_not_exists(page=logged_in_page, filename=new_filename)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Move file")
@pytest.mark.user("component_admin")
@pytest.mark.files({".odt": 1})
def test_nextcloud_move_file(logged_in_page: Page, url_portal: str, run_id: str, temp_files: tuple[str, list]):
    folder, files = temp_files
    filename: str = files[0]
    new_folder: str = f"folder-{run_id}"
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_upload_file(page=logged_in_page, folder=folder, filename=filename)
    steps_nextcloud.step_check_file_exists(page=logged_in_page, filename=filename)
    
    steps_nextcloud.step_create_folder(page=logged_in_page, foldername=new_folder)
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=filename)
    steps_nextcloud.step_click_move_or_copy(page=logged_in_page)
    steps_nextcloud.step_open_folder_for_move_copy(page=logged_in_page, foldername=new_folder)
    steps_nextcloud.step_click_move_to_folder(page=logged_in_page)
    
    steps_nextcloud.step_open_folder(page=logged_in_page, foldername=new_folder)
    steps_nextcloud.step_check_file_exists(page=logged_in_page, filename=filename)
    
    steps_nextcloud.step_goto_home_folder(page=logged_in_page)
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=new_folder)
    steps_nextcloud.step_click_delete(page=logged_in_page)
    steps_nextcloud.step_check_file_not_exists(page=logged_in_page, filename=new_folder)
    
@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Comment on file")
@pytest.mark.user("component_admin")
@pytest.mark.files({".odt": 1})
@pytest.mark.users_dependency(["user"])
def test_nextcloud_comment_on_file(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], component_admin: Config.User, user: Config.User):
    _, files = nextcloud_files
    filename: str = files[0]
    
    firstname: str = component_admin.firstname
    lastname: str = component_admin.lastname
    
    comment: str = "This is a test comment."
    emoji_comment: str = ":+1"
    mention_comment: str = f"@{user.username}"
    
    emoji_str: str = "\U0001F44D"
    mentions: list[str] = [f"{user.firstname} {user.lastname}"]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=filename)
    steps_nextcloud.step_click_open_details(page=logged_in_page)
    steps_nextcloud.step_open_activity_tab(page=logged_in_page)
    
    steps_nextcloud.step_type_comment(page=logged_in_page, comment=comment)
    steps_nextcloud.step_send_comment(page=logged_in_page)
    steps_nextcloud.step_check_comment_exists(page=logged_in_page, firstname=firstname, lastname=lastname, message=comment)
    
    steps_nextcloud.step_type_comment(page=logged_in_page, comment=emoji_comment)
    steps_nextcloud.step_select_emoji(page=logged_in_page, emoji=emoji_comment)
    steps_nextcloud.step_send_comment(page=logged_in_page)
    steps_nextcloud.step_check_comment_exists(page=logged_in_page, firstname=firstname, lastname=lastname, message=emoji_str)
    
    steps_nextcloud.step_type_comment(page=logged_in_page, comment=mention_comment)
    steps_nextcloud.step_select_user(page=logged_in_page, firstname=user.firstname, lastname=user.lastname)
    steps_nextcloud.step_send_comment(page=logged_in_page)
    steps_nextcloud.step_check_comment_with_mentions_exists(page=logged_in_page, firstname=firstname, lastname=lastname, mentions=mentions)
    
@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Create app token")
@pytest.mark.user("component_admin")
@pytest.mark.dependency(name="create_app_token")
@pytest.mark.xdist_group("webdav-app")
def test_nextcloud_create_app_token(logged_in_page: Page, url_portal: str, run_id: str, config: Config):
    app_password_name: str = f"WebDAV-{run_id}"
    
    steps_nextcloud.step_open_security_settings_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_fill_app_password_name(page=logged_in_page, name=app_password_name)
    steps_nextcloud.step_click_create_app_password(page=logged_in_page)
    app_name: str = steps_nextcloud.step_get_app_name(page=logged_in_page)
    app_password: str = steps_nextcloud.step_get_app_password(page=logged_in_page)
    
    allure.attach(app_name, "App name", allure.attachment_type.TEXT)
    allure.attach(app_password, "App password", allure.attachment_type.TEXT)
    config.save("WebDAVAppName", app_name)
    config.save("WebDAVAppPassword", app_password)

@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: WebDAV access")
@pytest.mark.user("component_admin")
@pytest.mark.dependency(depends=["create_app_token"])
@pytest.mark.xdist_group("webdav-app")
@pytest.mark.files({".odt": 1})
def test_nextcloud_webdav_access(logged_in_page: Page, url_portal: str, config: Config, temp_files: tuple[str, list]):
    app_name: str | None = config.load("WebDAVAppName")
    app_password: str | None = config.load("WebDAVAppPassword")
    assert app_name is not None, "App name not found."
    assert app_password is not None, "App password not found."
    
    folder, files = temp_files
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_upload_file(page=logged_in_page, folder=folder, filename=files[0])
    steps_nextcloud.step_check_file_exists(page=logged_in_page, filename=files[0])
    steps_nextcloud.step_open_file_settings(page=logged_in_page)
    webdav_url: str = steps_nextcloud.step_get_webdav_url(page=logged_in_page)
    steps_nextcloud.step_close_file_settings(page=logged_in_page)
    
    client = steps_webdav.step_open_client(url=webdav_url, username=app_name, password=app_password)
    
    steps_webdav.step_check_file_exists(client=client, path=files[0])
    
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=files[0])
    steps_nextcloud.step_click_delete(page=logged_in_page)
    steps_nextcloud.step_check_file_not_exists(page=logged_in_page, filename=files[0])
    
@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: WebDAV fuctionality")
@pytest.mark.user("component_admin")
@pytest.mark.files({".odt": 1})
def test_nextcloud_webdav_functionality(webdav_client: Client, temp_files: tuple[str, list]):
    folder, files = temp_files
    filename: str = files[0]
    new_filename: str = f"new-{filename}"
    input_file: str = f"{folder}{os.sep}{filename}"
    
    test_folder: str = "test-folder"
    
    steps_webdav.step_create_directory(client=webdav_client, path=test_folder)
    steps_webdav.step_check_file_exists(client=webdav_client, path=test_folder)
    steps_webdav.step_upload_file(client=webdav_client, path=filename, input_file=input_file)
    steps_webdav.step_check_file_exists(client=webdav_client, path=filename)
    steps_webdav.step_rename_file(client=webdav_client, old_name=filename, new_name=new_filename)
    steps_webdav.step_check_file_exists(client=webdav_client, path=new_filename)
    steps_webdav.step_move_file(client=webdav_client, from_path=new_filename, to_path=f"{test_folder}/{new_filename}")
    steps_webdav.step_check_file_exists(client=webdav_client, path=f"{test_folder}/{new_filename}")
    steps_webdav.step_delete_file(client=webdav_client, path=f"{test_folder}/{new_filename}")
    steps_webdav.step_check_file_not_exists(client=webdav_client, path=f"{test_folder}/{new_filename}")
    steps_webdav.step_delete_file(client=webdav_client, path=test_folder)
    steps_webdav.step_check_file_not_exists(client=webdav_client, path=test_folder)
    
@allure.epic("Smoke")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Chat on file")
@pytest.mark.users(["user", "component_admin"])
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_nextcloud_chat_on_file(logged_in_pages: list[Page], url_portal: str, nextcloud_files: tuple[str, list], component_admin: Config.User):
    _, files = nextcloud_files
    filename: str = files[0]
    internal_user_name: str = component_admin.username
    internal_user_email = component_admin.email
    chat_message: str = "This is a test chat message."

    user_page: Page = logged_in_pages[0]
    component_admin_page: Page = logged_in_pages[1]
    
    #user starts chat
    steps_nextcloud.step_open_page(page=user_page, url_portal=url_portal)
    steps_nextcloud.step_open_share_menu(page=user_page, filename=filename)
    steps_nextcloud.step_select_share_mail(page=user_page, share_input=internal_user_name, mail=internal_user_email)
    steps_nextcloud.step_click_save_share_button(page=user_page)
    steps_nextcloud.step_intern_shared_entry_visible(page=user_page, mail=internal_user_email)
    steps_nextcloud.step_open_chat_menu(page=user_page)
    steps_nextcloud.step_join_conversation(page=user_page)
    steps_nextcloud.step_enter_chat_message(page=user_page, chat_message=chat_message)
    steps_nextcloud.step_send_chat_message(page=user_page)
    steps_nextcloud.step_check_chat_message_exists(page=user_page, chat_message=chat_message)

    #component_admin checks chat message
    steps_nextcloud.step_open_page(page=component_admin_page, url_portal=url_portal)
    steps_nextcloud.step_open_shares_tab(page=component_admin_page)
    steps_nextcloud.step_open_share_menu(page=component_admin_page, filename=filename)
    steps_nextcloud.step_open_chat_menu(page=component_admin_page)
    steps_nextcloud.step_join_conversation(page=component_admin_page)
    steps_nextcloud.step_check_chat_message_exists(page=user_page, chat_message=chat_message)

