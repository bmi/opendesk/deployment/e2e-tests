# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import re
from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_notes
import allure
import pytest
import os

@allure.epic("Smoke")
@allure.feature("Notes")
@allure.story("User", "UI")
@allure.title("Notes: General availability")
@pytest.mark.env_setting("env_notes_enabled", True)
@pytest.mark.user("user")
def test_notes_general_availabilty(logged_in_page: Page, url_portal: str):
    steps_notes.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_notes.step_general_availability(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Notes")
@allure.story("User")
@allure.title("Notes: Create note")
@pytest.mark.env_setting("env_notes_enabled", True)
@pytest.mark.user("user")
@pytest.mark.xdist_group("note")
@pytest.mark.dependency(name="create_note")
def test_notes_create_note(logged_in_page: Page, url_portal: str, run_id: str, config: Config):
    note_name: str = f"Note {run_id}"
    steps_notes.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_notes.step_click_new_note_button(page=logged_in_page)
    steps_notes.step_fill_note_name(page=logged_in_page, name=note_name)
    steps_notes.step_fill_note_content(page=logged_in_page, content=f"Content of {note_name}")
    steps_notes.step_return_to_notes_list(page=logged_in_page)
    steps_notes.step_note_exists(page=logged_in_page, note_name=note_name)
    
    config.save("note_name", note_name)

@allure.epic("Smoke")
@allure.feature("Notes")
@allure.story("User")
@allure.title("Notes: Note visiblity (Private)")
@pytest.mark.env_setting("env_notes_enabled", True)
@pytest.mark.user("user")
@pytest.mark.xdist_group("note")
@pytest.mark.dependency(depends=["create_note"])
def test_notes_note_visibility_private(logged_in_page: Page, temp_logged_in_page: Page, page: Page, url_portal: str, config: Config):
    note_name: str | None = config.load("note_name")
    assert note_name is not None, "Note was not created"
    
    steps_notes.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_notes.step_open_note(page=logged_in_page, note_name=note_name)
    steps_notes.step_click_note_share_button(page=logged_in_page)
    steps_notes.step_check_share_modal_opened(page=logged_in_page)
    steps_notes.step_open_visibility_select(page=logged_in_page)
    steps_notes.step_select_visibility_private(page=logged_in_page)
    note_link: str = steps_notes.step_get_note_link(page=logged_in_page)
    
    with allure.step("Open note with internal user"):
        steps_notes.step_open_note_with_link(page=temp_logged_in_page, note_link=note_link)
        steps_notes.step_check_note_no_permission_logged_in(page=temp_logged_in_page)
    
    with allure.step("Open note with guest user"):
        steps_notes.step_open_note_with_link(page=page, note_link=note_link)
        steps_notes.step_check_note_no_permission_not_logged_in(page=page)
    
@allure.epic("Smoke")
@allure.feature("Notes")
@allure.story("User")
@allure.title("Notes: Note visiblity (Public)")
@pytest.mark.env_setting("env_notes_enabled", True)
@pytest.mark.user("user")
@pytest.mark.xdist_group("note")
@pytest.mark.dependency(depends=["create_note"])
def test_notes_note_visibility_public(logged_in_page: Page, temp_logged_in_page: Page, page: Page, url_portal: str, config: Config):
    note_name: str | None = config.load("note_name")
    assert note_name is not None, "Note was not created"
    
    steps_notes.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_notes.step_open_note(page=logged_in_page, note_name=note_name)
    steps_notes.step_click_note_share_button(page=logged_in_page)
    steps_notes.step_check_share_modal_opened(page=logged_in_page)
    steps_notes.step_open_visibility_select(page=logged_in_page)
    steps_notes.step_select_visibility_public(page=logged_in_page)
    note_link: str = steps_notes.step_get_note_link(page=logged_in_page)
    
    steps_notes.step_set_visibility_type_read_only(page=logged_in_page)
    
    with allure.step("Open note with internal user"):
        steps_notes.step_open_note_with_link(page=temp_logged_in_page, note_link=note_link)
        steps_notes.step_check_note_read_only(page=temp_logged_in_page)
    
    with allure.step("Open note with guest user"):
        steps_notes.step_open_note_with_link(page=page, note_link=note_link)
        steps_notes.step_check_note_read_only(page=page)
    
    steps_notes.step_set_visibility_type_read_write(page=logged_in_page)
    
    
    with allure.step("Open note with internal user"):
        steps_notes.step_open_note_with_link(page=temp_logged_in_page, note_link=note_link)
        steps_notes.step_check_note_read_write(page=temp_logged_in_page)
    
    with allure.step("Open note with guest user"):
        steps_notes.step_open_note_with_link(page=page, note_link=note_link)
        steps_notes.step_check_note_read_write(page=page)
    
@allure.epic("Smoke")
@allure.feature("Notes")
@allure.story("User")
@allure.title("Notes: Note visiblity (Intern)")
@pytest.mark.env_setting("env_notes_enabled", True)
@pytest.mark.user("user")
@pytest.mark.xdist_group("note")
@pytest.mark.dependency(depends=["create_note"])
def test_notes_note_visibility_intern(logged_in_page: Page, temp_logged_in_page: Page, page: Page, url_portal: str, config: Config):
    note_name: str | None = config.load("note_name")
    assert note_name is not None, "Note was not created"
    
    steps_notes.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_notes.step_open_note(page=logged_in_page, note_name=note_name)
    steps_notes.step_click_note_share_button(page=logged_in_page)
    steps_notes.step_check_share_modal_opened(page=logged_in_page)
    steps_notes.step_open_visibility_select(page=logged_in_page)
    steps_notes.step_select_visibility_intern(page=logged_in_page)
    note_link: str = steps_notes.step_get_note_link(page=logged_in_page)
    
    steps_notes.step_set_visibility_type_read_only(page=logged_in_page)
    
    with allure.step("Open note with internal user"):
        steps_notes.step_open_note_with_link(page=temp_logged_in_page, note_link=note_link)
        steps_notes.step_check_note_read_only(page=temp_logged_in_page)
    
    with allure.step("Open note with guest user"):
        steps_notes.step_open_note_with_link(page=page, note_link=note_link)
        steps_notes.step_check_note_no_permission_not_logged_in(page=page)
    
    steps_notes.step_set_visibility_type_read_write(page=logged_in_page)
    
    with allure.step("Open note with internal user"):
        steps_notes.step_open_note_with_link(page=temp_logged_in_page, note_link=note_link)
        steps_notes.step_check_note_read_write(page=temp_logged_in_page)
    
    with allure.step("Open note with guest user"):
        steps_notes.step_open_note_with_link(page=page, note_link=note_link)
        steps_notes.step_check_note_no_permission_not_logged_in(page=page)

@allure.epic("Smoke")
@allure.feature("Notes")
@allure.story("User")
@allure.title("Notes: Delete note")
@pytest.mark.env_setting("env_notes_enabled", True)
@pytest.mark.user("user")
@pytest.mark.xdist_group("note")
@pytest.mark.dependency(depends=["create_note"])
def test_notes_delete_note(logged_in_page: Page, url_portal: str, config: Config):
    note_name: str | None = config.load("note_name")
    assert note_name is not None, "Note was not created"
    
    steps_notes.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_notes.step_click_delete_note_button(page=logged_in_page, note_name=note_name)
    steps_notes.step_click_confirm_delete_button(page=logged_in_page)
    steps_notes.step_note_not_exists(page=logged_in_page, note_name=note_name)