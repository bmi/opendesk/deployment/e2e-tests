# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_ox_mail
import allure
import pytest

from tests.utils import retry

@allure.epic("Smoke", "Integration")
@allure.feature("OX", "Mail")
@allure.story("User")
@allure.title("OXmail: New contact appears in mail contact list")
@pytest.mark.user("user")
@pytest.mark.dependency(depends=["edit_contact"], scope="session")
@pytest.mark.xdist_group("contact")
def test_ox_contact_in_contact_list(logged_in_page: Page, url_portal: str, config: Config):
    email: str = config.load("NewContactEmail")
    assert email is not None, "Contact was not edited: Email not set"
    folder_id: str = config.load("NewContactFolderId")
    assert folder_id is not None, "No contact created"
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_click_contacts_button(page=logged_in_page)
    steps_ox_mail.step_select_address_book_select_option(page=logged_in_page, value=folder_id)
    steps_ox_mail.step_check_contact_appears_in_contact_list(page=logged_in_page, email=email)


@allure.epic("Smoke", "Integration")
@allure.feature("OX", "Mail")
@allure.story("Functional accounts")
@allure.title("OXmail: Functional account available")
@pytest.mark.dependency(depends=["create_functional_account"], scope="session")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("functional_account")
def test_ox_functional_account_available(logged_in_page: Page, url_portal: str, config: Config):
    functional_account_name: str | None = config.load("FunctionalAccountName")
    assert functional_account_name is not None, "Functional account was not created"
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    
    retry_wrapper = retry(trys=5, retry_timeout=30, on_fail=lambda page, *p, **k: steps_ox_mail.step_open_page(page=page, url_portal=url_portal))
    retry_wrapper(steps_ox_mail.step_check_functional_account_visible)(page=logged_in_page, functional_account_name=functional_account_name)
    
    steps_ox_mail.step_expand_functional_account(page=logged_in_page, functional_account_name=functional_account_name)

@allure.epic("Smoke", "Integration")
@allure.feature("OX", "Mail")
@allure.story("User", "Address list")
@allure.title("OXmail: Functional account in address list")
@pytest.mark.dependency(depends=["create_functional_account"], scope="session")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("functional_account")
def test_ox_select_functional_mailbox(logged_in_page: Page, url_portal: str, config: Config):
    functional_account_mail: str | None = config.load("FunctionalAccountMail")
    assert functional_account_mail is not None, "Functional account was not created"
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_click_contacts_button(page=logged_in_page)
    steps_ox_mail.step_select_functional_mailboxes(page=logged_in_page)
    steps_ox_mail.step_check_contact_appears_in_contact_list(page=logged_in_page, email=functional_account_mail)

@allure.epic("Smoke", "Integration")
@allure.feature("OX", "Mail")
@allure.story("User", "Send mail")
@allure.title("OXmail: Send and receive mail with Nextcloud attachment link")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.env_setting("env_enforce_password", False)
@pytest.mark.files({"odp": 1})
@pytest.mark.user("component_admin")
@pytest.mark.users(["component_admin", "user"])
def test_ox_send_mail_with_nc_attachment_link(logged_in_pages: list[Page], url_portal: str, nextcloud_files: tuple[str, list], run_id: str, user: Config.User):
    logged_in_page: Page = logged_in_pages[0]
    logged_in_page_user: Page = logged_in_pages[1]
    subject: str = f"File Link {run_id}"
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_fill_internal_recipient(page=logged_in_page, username=user.username)
    steps_ox_mail.step_fill_subject(page=logged_in_page, subject=subject)
    steps_ox_mail.step_click_attachments_button(page=logged_in_page)
    steps_ox_mail.step_click_link_from_files_button(page=logged_in_page)
    steps_ox_mail.step_select_file(page=logged_in_page, filename=nextcloud_files[1][0])
    steps_ox_mail.step_save_file_links(page=logged_in_page)
    steps_ox_mail.step_check_save_success(page=logged_in_page)
    steps_ox_mail.step_send_mail(page=logged_in_page)
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page_user, url_portal=url_portal)
    steps_ox_mail.step_receive_mail(page=logged_in_page_user, subject=subject)

@allure.epic("Smoke", "Integration")
@allure.feature("OX", "Mail")
@allure.story("User", "Send mail")
@allure.title("OXmail: Send and receive mail with Nextcloud attachment")
@pytest.mark.files({"odp": 1})
@pytest.mark.user("component_admin")
@pytest.mark.users(["component_admin", "user"])
def test_ox_send_mail_with_nc_attachment(logged_in_pages: list[Page], url_portal: str, nextcloud_files: tuple[str, list], run_id: str, user: Config.User):
    logged_in_page: Page = logged_in_pages[0]
    logged_in_page_user: Page = logged_in_pages[1]
    subject: str = f"File {run_id}"
    filename: str = nextcloud_files[1][0]
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_fill_internal_recipient(page=logged_in_page, username=user.username)
    steps_ox_mail.step_fill_subject(page=logged_in_page, subject=subject)
    steps_ox_mail.step_click_attachments_button(page=logged_in_page)
    steps_ox_mail.step_click_from_files_button(page=logged_in_page)
    steps_ox_mail.step_select_file(page=logged_in_page, filename=filename)
    steps_ox_mail.step_save_file_links(page=logged_in_page)
    steps_ox_mail.step_check_save_success(page=logged_in_page)
    steps_ox_mail.step_check_attachment_preview(page=logged_in_page, filename=filename)
    steps_ox_mail.step_send_mail(page=logged_in_page)
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page_user, url_portal=url_portal)
    mail = steps_ox_mail.step_receive_mail(page=logged_in_page_user, subject=subject)
    steps_ox_mail.step_open_mail(page=logged_in_page_user, mail=mail)
    steps_ox_mail.step_check_mail_has_attachment(page=logged_in_page_user, filename=filename)

@allure.epic("Smoke", "Integration")
@allure.feature("OX", "Mail")
@allure.story("User", "Send mail")
@allure.title("OXmail: Enforce password for Nextcloud attachment link")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.env_setting("env_enforce_password", True)
@pytest.mark.files({"odp": 1})
@pytest.mark.user("component_admin")
def test_ox_nextcloud_attachment_link_password_enforced(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list]):
    filename: str = nextcloud_files[1][0]
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_click_attachments_button(page=logged_in_page)
    steps_ox_mail.step_click_link_from_files_button(page=logged_in_page)
    steps_ox_mail.step_select_file(page=logged_in_page, filename=filename)
    steps_ox_mail.step_save_file_links(page=logged_in_page)
    steps_ox_mail.step_check_save_failed(page=logged_in_page)

@allure.epic("Smoke", "Integration")
@allure.feature("OX", "Mail")
@allure.story("User", "Send mail")
@allure.title("OXmail: No password enforce for Nextcloud attachment link")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.env_setting("env_enforce_password", False)
@pytest.mark.files({"odp": 1})
@pytest.mark.user("component_admin")
def test_ox_nextcloud_attachment_link_no_password_enforced(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list]):
    filename: str = nextcloud_files[1][0]
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_click_attachments_button(page=logged_in_page)
    steps_ox_mail.step_click_link_from_files_button(page=logged_in_page)
    steps_ox_mail.step_select_file(page=logged_in_page, filename=filename)
    steps_ox_mail.step_save_file_links(page=logged_in_page)
    steps_ox_mail.step_check_save_success(page=logged_in_page)

@allure.epic("Smoke", "Integration")
@allure.feature("OX", "Mail")
@allure.story("User", "Send mail")
@allure.title("OXmail: Nextcloud attachment link (Attachment allow edit, Attachment password protect, Attachment expiration date)")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.files({"odp": 1})
@pytest.mark.user("component_admin")
def test_ox_files_procected_attachment(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], config: Config):
    password: str = config.generate_password()
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_click_attachments_button(page=logged_in_page)
    steps_ox_mail.step_click_link_from_files_button(page=logged_in_page)
    steps_ox_mail.step_select_file(page=logged_in_page, filename=nextcloud_files[1][0])
    steps_ox_mail.step_open_link_settings(page=logged_in_page)
    steps_ox_mail.step_link_type_password(page=logged_in_page, password=password)
    steps_ox_mail.step_link_allow_edit(page=logged_in_page)
    steps_ox_mail.step_link_set_expiration(page=logged_in_page)
    steps_ox_mail.step_save_file_links(page=logged_in_page)
    steps_ox_mail.step_check_save_success(page=logged_in_page)

@allure.epic("Smoke", "Integration")
@allure.feature("OX", "Mail")
@allure.story("User")
@allure.title("OXmail: Appointment invitation mail received")
@pytest.mark.user("user")
@pytest.mark.xdist_group("ox_appointment_participant")
@pytest.mark.dependency(depends=["create_appointment_participant"], scope="session")
def test_ox_receive_appointment_invitation(logged_in_page: Page, url_portal: str, config: Config):
    appointment_title: str | None = config.load("ParticipantAppointmentTitle")
    assert appointment_title is not None, "Appointment was not created"
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_receive_mail(page=logged_in_page, contains_subject=appointment_title)
