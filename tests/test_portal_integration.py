# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_change_password, steps_login, steps_mail, steps_portal, steps_portal_not_logged_in
import allure
import pytest
from imap_tools import MailBox, MailMessage

# Integration tests for creating a new user:

@allure.epic("Smoke")
@allure.feature("Login")
@allure.story("Password Reset")
@allure.title("Portal: Reset password (Invitation mail received)")
@pytest.mark.dependency(name="new_user_invitation_mail", depends=["create_user"], scope="session")
@pytest.mark.xdist_group("new_user-group")
def test_new_user_invitation_mail(page: Page, url_portal: str, new_user: Config.User, mailbox: MailBox):
    mail_message: MailMessage = steps_mail.step_receive_reset_password_email(mailbox=mailbox, username=new_user.username)
    
    steps_mail.step_open_new_password_link(page=page, url_portal=url_portal, username=new_user.username, mail_message=mail_message)
    
    steps_change_password.step_fill_new_password(page=page, password=new_user.password, retype_password=new_user.password)
    steps_change_password.step_click_change_password(page=page)
    steps_change_password.step_check_password_changed(page=page)

@allure.epic("Smoke")
@allure.feature("Login")
@allure.story("Password Reset")
@allure.title("Portal: Request new password")
@pytest.mark.dependency(name="request_new_password", depends=["new_user_invitation_mail"], scope="session")
@pytest.mark.xdist_group("new_user-group")
def test_request_password_reset(page: Page, url_portal: str, new_user: Config.User):
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_click_forgot_password(page=page)
    steps_login.step_fill_forgot_password(page=page, username=new_user.username)

@allure.epic("Smoke")
@allure.feature("Login")
@allure.story("Password Reset")
@allure.title("Portal: Receive reset password mail")
@pytest.mark.dependency(name="receive_new_password_mail", depends=["request_new_password"], scope="session")
@pytest.mark.xdist_group("new_user-group")
def test_receive_new_password_mail(new_user: Config.User, mailbox: MailBox):
    steps_mail.step_receive_reset_password_email(mailbox=mailbox, username=new_user.username)

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("Announcement")
@allure.title("Portal: Public announcement visible (Not logged in)")
@pytest.mark.dependency(depends=["create_public_announcement"], scope="session")
@pytest.mark.xdist_group("announcements_public")
@pytest.mark.env_setting("env_enforce_login", False)
def test_announcement_public_visible_not_logged_in(page: Page, url_portal: str, config: Config):
    title: str | None = config.load("AnnouncementPublicTitle")
    assert title != None, "Public announcement was not created"
    
    steps_portal_not_logged_in.step_open_page(page=page, url_portal=url_portal)
    steps_portal.step_check_announcement_visible(page=page, title=title)

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("Announcement")
@allure.title("Portal: Public announcement visible")
@pytest.mark.dependency(depends=["create_public_announcement"], scope="session")
@pytest.mark.xdist_group("announcements_public")
@pytest.mark.user("user")
def test_announcement_public_visible(logged_in_page: Page, url_portal: str, config: Config):
    title: str | None = config.load("AnnouncementPublicTitle")
    assert title != None, "Public announcement was not created"
    
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_check_announcement_visible(page=logged_in_page, title=title)

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("Announcement")
@allure.title("Portal: Private announcement not visible (User not in group)")
@pytest.mark.dependency(depends=["create_private_announcement"], scope="session")
@pytest.mark.xdist_group("announcements_private")
@pytest.mark.user("user")
def test_announcement_private_not_visible(logged_in_page: Page, url_portal: str, config: Config):
    title: str | None = config.load("AnnouncementPrivateTitle")
    assert title != None, "Private announcement was not created"
    
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_check_announcement_not_visible(page=logged_in_page, title=title)

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("Announcement")
@allure.title("Portal: Private announcement visible (User in group)")
@pytest.mark.dependency(depends=["create_private_announcement"], scope="session")
@pytest.mark.xdist_group("announcements_private")
@pytest.mark.user("user_2fa")
def test_announcement_private_visible(logged_in_page: Page, url_portal: str, config: Config):
    title: str | None = config.load("AnnouncementPrivateTitle")
    assert title != None, "Private announcement was not created"
    
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_check_announcement_visible(page=logged_in_page, title=title)

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("Tile")
@allure.title("Portal: New tile visible and working")
@pytest.mark.dependency(depends=["create_new_tile"], scope="session")
@pytest.mark.xdist_group("new_portal_tile")
@pytest.mark.user("user")
def test_portal_new_tile_visible(logged_in_page: Page, url_portal: str, config: Config):
    tile_name: str | None = config.load("NewTileName")
    tile_link: str | None = config.load("NewTileLink")
    assert tile_name is not None and tile_link is not None, "New tile was not created"
    
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_check_tile_visible(page=logged_in_page, tile_name=tile_name)
    redirect_page: Page = steps_portal.step_tile_redirect(page=logged_in_page, tile_name=tile_name)
    steps_portal.step_check_redirect_url(page=redirect_page, expected_url=tile_link)
    
    redirect_page.close()