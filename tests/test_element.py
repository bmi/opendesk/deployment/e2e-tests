# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_element, steps_service
import allure
import pytest

@allure.epic("Smoke")
@allure.feature("Element")
@allure.story("User", "UI")
@allure.title("Element: General availability")
def test_check_element_site(temp_logged_in_page: Page, url_portal: str):
    steps_element.step_open_page(page=temp_logged_in_page, url_portal=url_portal)
    steps_element.step_check_menu_functionality(page=temp_logged_in_page)
    steps_element.step_check_site_render(page=temp_logged_in_page)

@allure.epic("Smoke")
@allure.feature("Element")
@allure.story("User", "Room", "Security key")
@allure.title("Element: New conversation (External user)")
@pytest.mark.env_setting("env_self_signed_ca", False)
@pytest.mark.env_setting("env_federation_enabled", True)
@pytest.mark.dependency(name="element_new_conversation")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("element")
def test_element_external_conversation(logged_in_page: Page, url_portal: str, autotest_element_username: str, run_id: str, config: Config):
    test_message: str = f"Ping {run_id}"
    
    steps_element.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_element.step_click_new_conversation_button(page=logged_in_page)
    steps_element.step_fill_invite_username(page=logged_in_page, username=autotest_element_username)
    steps_element.step_select_user_by_username(page=logged_in_page, username=autotest_element_username)
    steps_element.step_click_invite_user(page=logged_in_page)
    steps_element.step_check_room_header_username(page=logged_in_page, username=autotest_element_username)
    steps_element.step_send_message_in_room(page=logged_in_page, message=test_message)
    steps_element.step_check_message_in_chat(page=logged_in_page, message=test_message)

@allure.epic("Smoke")
@allure.feature("Element")
@allure.story("User", "Room", "Security key")
@allure.title("Element: New conversation (Internal users)")
@pytest.mark.dependency(name="element_internal_conversation")
@pytest.mark.users(["component_admin", "user"])
@pytest.mark.xdist_group("element")
def test_element_internal_conversation(logged_in_pages: list[Page], url_portal: str, run_id: str, component_admin: Config.User, user: Config.User):
    user_1: Config.User = component_admin
    user_2: Config.User = user
    test_message_user_1: str = f"From User 1 {run_id}"
    test_message_user_2: str = f"From User 2 {run_id}"
    logged_in_page_user_1: Page = logged_in_pages[0]
    logged_in_page_user_2: Page = logged_in_pages[1]
    
    steps_element.step_open_page(page=logged_in_page_user_2, url_portal=url_portal)
    
    steps_element.step_open_page(page=logged_in_page_user_1, url_portal=url_portal)
    steps_element.step_click_new_conversation_button(page=logged_in_page_user_1)
    steps_element.step_fill_invite_fullname(page=logged_in_page_user_1, firstname=user_2.firstname, lastname=user_2.lastname)
    steps_element.step_select_user_by_fullname(page=logged_in_page_user_1, firstname=user_2.firstname, lastname=user_2.lastname)
    steps_element.step_click_invite_user(page=logged_in_page_user_1)
    steps_element.step_check_room_header_name(page=logged_in_page_user_1, firstname=user_2.firstname, lastname=user_2.lastname)
    steps_element.step_send_message_in_room(page=logged_in_page_user_1, message=test_message_user_1)
    steps_element.step_check_message_in_chat(page=logged_in_page_user_1, message=test_message_user_1)
    
    steps_element.step_open_chat_invitation(page=logged_in_page_user_2, firstname=user_1.firstname, lastname=user_1.lastname)
    steps_element.step_click_join_room(page=logged_in_page_user_2)
    steps_element.step_check_message_in_chat(page=logged_in_page_user_2, message=test_message_user_1)
    
    steps_element.step_send_message_in_room(page=logged_in_page_user_2, message=test_message_user_2)
    steps_element.step_check_message_in_chat(page=logged_in_page_user_2, message=test_message_user_2)
    steps_element.step_check_message_in_chat(page=logged_in_page_user_1, message=test_message_user_2)

@allure.epic("Smoke")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Start call")
@pytest.mark.users(["component_admin", "user"])
@pytest.mark.xdist_group("element")
@pytest.mark.dependency(depends=["element_internal_conversation"])
def test_element_start_call(logged_in_pages: list[Page], url_portal: str, user: Config.User, component_admin: Config.User):
    user_1: Config.User = component_admin
    user_2: Config.User = user
    logged_in_page_user_1: Page = logged_in_pages[0]
    logged_in_page_user_2: Page = logged_in_pages[1]
    
    steps_element.step_open_page(page=logged_in_page_user_1, url_portal=url_portal)
    steps_element.step_open_page(page=logged_in_page_user_2, url_portal=url_portal)
    
    steps_element.step_open_person_room(page=logged_in_page_user_1, person_name=f"{user_2.firstname} {user_2.lastname}")
    steps_element.step_open_person_room(page=logged_in_page_user_2, person_name=f"{user_1.firstname} {user_1.lastname}")
    
    steps_element.step_click_start_voice_call(page=logged_in_page_user_1)
    steps_element.step_click_join_call(page=logged_in_page_user_2)
    
    steps_element.step_check_voice_call_started(page=logged_in_page_user_1)
    steps_element.step_check_voice_call_started(page=logged_in_page_user_2)
    
    steps_element.step_move_mouse_to_open_call_menu(page=logged_in_page_user_1)
    steps_element.step_click_hangup_call(page=logged_in_page_user_1)
    
    steps_element.step_check_voice_call_ended(page=logged_in_page_user_1)
    steps_element.step_check_voice_call_ended(page=logged_in_page_user_2)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Change displayname")
@pytest.mark.env_setting("env_allow_changing_displayname", True)
def test_element_displayname_can_be_changed(temp_logged_in_page: Page, url_portal: str):
    new_name: str = "Olof Schulz"

    steps_element.step_open_page(page=temp_logged_in_page, url_portal=url_portal)
    steps_element.step_click_user_menu(page=temp_logged_in_page)
    steps_element.step_click_all_settings_button(page=temp_logged_in_page)
    steps_element.step_click_account_button_in_settings_dialog(page=temp_logged_in_page)
    steps_element.step_enter_new_displayname(page=temp_logged_in_page, name=new_name)
    steps_element.step_click_close_dialog_button(page=temp_logged_in_page)
    steps_element.step_click_user_menu(page=temp_logged_in_page)
    steps_element.step_check_displayname(page=temp_logged_in_page, name=new_name)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Displayname cannot be changed")
@pytest.mark.env_setting("env_allow_changing_displayname", False)
def test_element_displayname_cannot_be_changed(temp_logged_in_page: Page, url_portal: str):
    steps_element.step_open_page(page=temp_logged_in_page, url_portal=url_portal)
    steps_element.step_click_user_menu(page=temp_logged_in_page)
    steps_element.step_click_all_settings_button(page=temp_logged_in_page)
    steps_element.step_click_account_button_in_settings_dialog(page=temp_logged_in_page)
    steps_element.step_check_displayname_is_immutable(page=temp_logged_in_page)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Localpart of user's matrix id is equal to username")
@pytest.mark.env_setting("env_immutable_matrixid_localpart", False)
def test_element_localpart_is_username(temp_logged_in_page: Page, temp_user: Config.User, url_portal: str):
    steps_element.step_open_page(page=temp_logged_in_page, url_portal=url_portal)
    steps_element.step_click_user_menu(page=temp_logged_in_page)
    steps_element.step_click_all_settings_button(page=temp_logged_in_page)
    steps_element.step_click_account_button_in_settings_dialog(page=temp_logged_in_page)
    steps_element.step_check_localpart_is_username(page=temp_logged_in_page, username=temp_user.element_username)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Localpart of user's matrix id is a UUID")
@pytest.mark.env_setting("env_immutable_matrixid_localpart", True)
def test_element_localpart_is_uuid(temp_logged_in_page: Page, url_portal: str):
    steps_element.step_open_page(page=temp_logged_in_page, url_portal=url_portal)
    steps_element.step_click_user_menu(page=temp_logged_in_page)
    steps_element.step_click_all_settings_button(page=temp_logged_in_page)
    steps_element.step_click_account_button_in_settings_dialog(page=temp_logged_in_page)
    steps_element.step_check_localpart_is_uuid(page=temp_logged_in_page)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.title("Element: Federation enabled")
@pytest.mark.env_setting("env_federation_enabled", True)
def test_element_federation_enabled(base_domain: str):
    response: dict = steps_element.step_get_server_status(base_domain=base_domain)
    steps_element.step_check_federation_enabled(response=response)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.title("Element: Federation disabled")
@pytest.mark.env_setting("env_federation_enabled", False)
def test_element_federation_disabled(base_domain: str):
    response: dict = steps_element.step_get_server_status(base_domain=base_domain)
    steps_element.step_check_federation_disabled(response=response)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Create public room")
@pytest.mark.user("component_admin")
@pytest.mark.dependency(name="create_public_room")
@pytest.mark.xdist_group("element")
def test_element_create_public_room(logged_in_page: Page, url_portal: str, base_domain: str, run_id: str, config: Config):
    room_name: str = f"Public {run_id}"
    room_id: str = f"pub{run_id}"
    room_address: str = f"#{room_id}:{base_domain}"
    
    steps_element.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_element.step_open_new_room_menu(page=logged_in_page)
    steps_element.step_click_new_room(page=logged_in_page)
    steps_element.step_fill_room_name(page=logged_in_page, room_name=room_name)
    steps_element.step_select_join_rule_public(page=logged_in_page)
    steps_element.step_fill_public_room_address(page=logged_in_page, room_id=room_id)
    steps_element.step_click_create_room(page=logged_in_page)
    steps_element.step_check_room_header(page=logged_in_page, room_name=room_name)
    
    config.save("ElementPublicRoomName", room_name)
    config.save("ElementPublicRoomAddress", room_address)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Join public room")
@pytest.mark.user("user")
@pytest.mark.dependency(depends=["create_public_room"])
@pytest.mark.xdist_group("element")
def test_element_join_public_room(logged_in_page: Page, url_portal: str, config: Config):
    room_name: str | None = config.load("ElementPublicRoomName")
    room_address: str | None = config.load("ElementPublicRoomAddress")
    assert room_name != None
    assert room_address != None
    
    steps_element.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_element.step_open_new_room_menu(page=logged_in_page)
    steps_element.step_click_explore_public_rooms(page=logged_in_page)
    steps_element.step_check_spotlight_dialog_open(page=logged_in_page)
    steps_element.step_fill_spotlight_search(page=logged_in_page, search_query=room_address)
    steps_element.step_click_join_public_room(page=logged_in_page, room_address=room_address)
    steps_element.step_check_room_header(page=logged_in_page, room_name=room_name)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Join public room (Not logged in, Join rejected)")
@pytest.mark.xdist_group("element")
@pytest.mark.dependency(depends=["create_public_room"])
def test_element_no_join_public_room(page: Page, url_portal: str, run_id: str, config: Config):
    room_address: str | None = config.load("ElementPublicRoomAddress")
    assert room_address != None
    guest_name: str = f"Guest {run_id}"
    
    steps_element.step_open_room_url(page=page, url_portal=url_portal, room_address=room_address)
    steps_element.step_click_join_room(page=page)
    steps_element.step_fill_guest_join_name(page=page, name=guest_name)
    steps_element.step_click_join_as_guest(page=page)
    steps_element.step_click_join_room(page=page)
    steps_element.step_check_join_room_failed(page=page)
    
@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Create invite room")
@pytest.mark.user("component_admin")
@pytest.mark.dependency(name="create_invite_room")
@pytest.mark.xdist_group("element")
def test_element_create_invite_room(logged_in_page: Page, url_portal: str, run_id: str, config: Config):
    room_name: str = f"Invite {run_id}"
    
    steps_element.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_element.step_open_new_room_menu(page=logged_in_page)
    steps_element.step_click_new_room(page=logged_in_page)
    steps_element.step_fill_room_name(page=logged_in_page, room_name=room_name)
    steps_element.step_select_join_rule_invite(page=logged_in_page)
    steps_element.step_click_create_room(page=logged_in_page)
    steps_element.step_check_room_header(page=logged_in_page, room_name=room_name)
    invite_room_url: str = logged_in_page.url
    
    config.save("ElementInviteRoomName", room_name)
    config.save("ElementInviteRoomUrl", invite_room_url)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Join invite room (Not invited, Join rejected)")
@pytest.mark.user("user")
@pytest.mark.dependency(depends=["create_invite_room"])
@pytest.mark.xdist_group("element")
def test_element_no_join_invite_room(logged_in_page: Page, url_portal: str, run_id: str, config: Config):
    room_url: str | None = config.load("ElementInviteRoomUrl")
    assert room_url != None
    
    steps_element.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_element.step_open_room_url(page=logged_in_page, room_url=room_url)
    steps_element.step_click_join_room(page=logged_in_page)
    steps_element.step_click_join_room(page=logged_in_page)
    steps_element.step_check_join_room_failed(page=logged_in_page)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Invite process")
@pytest.mark.users(["component_admin", "user"])
@pytest.mark.dependency(name="room_invite", depends=["create_invite_room"])
@pytest.mark.xdist_group("element")
def test_element_invite_user(logged_in_pages: list[Page], url_portal: str, user: Config.User, config: Config):
    room_name: str | None = config.load("ElementInviteRoomName")
    assert room_name != None
    
    logged_in_page_inviter: Page = logged_in_pages[0]
    logged_in_page_invitee: Page = logged_in_pages[1]
    
    steps_element.step_open_page(page=logged_in_page_invitee, url_portal=url_portal)
    
    steps_element.step_open_page(page=logged_in_page_inviter, url_portal=url_portal)
    steps_element.step_open_chat_room(page=logged_in_page_inviter, room_name=room_name)
    steps_element.step_click_invite(page=logged_in_page_inviter)
    steps_element.step_fill_invite_fullname(page=logged_in_page_inviter, firstname=user.firstname, lastname=user.lastname)
    steps_element.step_select_user_by_fullname(page=logged_in_page_inviter, firstname=user.firstname, lastname=user.lastname)
    steps_element.step_click_invite_user(page=logged_in_page_inviter)
    
    steps_element.step_open_room_invitation(page=logged_in_page_invitee, room_name=room_name)
    steps_element.step_click_join_room(page=logged_in_page_invitee)
    steps_element.step_check_room_header(page=logged_in_page_invitee, room_name=room_name)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Create knock room")
@pytest.mark.user("component_admin")
@pytest.mark.dependency(name="create_knock_room")
@pytest.mark.xdist_group("element")
def test_element_create_knock_room(logged_in_page: Page, url_portal: str, run_id: str, config: Config):
    room_name: str = f"Knock {run_id}"
    
    steps_element.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_element.step_open_new_room_menu(page=logged_in_page)
    steps_element.step_click_new_room(page=logged_in_page)
    steps_element.step_fill_room_name(page=logged_in_page, room_name=room_name)
    steps_element.step_select_join_rule_knock(page=logged_in_page)
    steps_element.step_click_create_room(page=logged_in_page)
    steps_element.step_check_room_header(page=logged_in_page, room_name=room_name)
    knock_room_url: str = logged_in_page.url
    
    config.save("ElementKnockRoomName", room_name)
    config.save("ElementKnockRoomUrl", knock_room_url)

@allure.epic("Smoke", "Regression")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Knock room join process (Request join, Accept request, Join room)")
@pytest.mark.users(["component_admin", "user"])
@pytest.mark.dependency(depends=["create_knock_room"])
@pytest.mark.xdist_group("element")
def test_element_knock_room_join(logged_in_pages: list[Page], url_portal: str, user: Config.User, config: Config):
    room_url: str | None = config.load("ElementKnockRoomUrl")
    room_name: str | None = config.load("ElementKnockRoomName")
    assert room_url != None
    assert room_name != None
    
    knocking: Config.User = user
    logged_in_page_owner: Page = logged_in_pages[0]
    logged_in_page_knocking: Page = logged_in_pages[1]
    
    steps_element.step_open_page(page=logged_in_page_knocking, url_portal=url_portal)
    steps_element.step_open_room_url(page=logged_in_page_knocking, room_url=room_url)
    steps_element.step_click_join_room(page=logged_in_page_knocking)
    steps_element.step_click_join_room(page=logged_in_page_knocking)
    
    steps_element.step_open_page(page=logged_in_page_owner, url_portal=url_portal)
    steps_element.step_open_chat_room(page=logged_in_page_owner, room_name=room_name)
    steps_element.step_accept_knock_fullname(page=logged_in_page_owner, firstname=knocking.firstname, lastname=knocking.lastname)
    
    steps_element.step_click_join_room(page=logged_in_page_knocking)
    steps_element.step_check_room_header(page=logged_in_page_knocking, room_name=room_name)

@allure.epic("Smoke")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Schedule new meeting")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("element")
@pytest.mark.dependency(name="element_new_meeting")
def test_element_schedule_meeting(logged_in_page: Page, url_portal: str, run_id: str, config: Config):
    meeting_title: str = f"Meeting {run_id}"
    
    steps_element.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_element.step_open_home_page(page=logged_in_page, url_portal=url_portal)
    steps_element.step_open_appointment_scheduler(page=logged_in_page)
    steps_element.step_click_schedule_new_meeting(page=logged_in_page)
    steps_element.step_fill_meeting_title(page=logged_in_page, title=meeting_title)
    steps_element.step_click_create_meeting(page=logged_in_page)
    steps_element.step_check_meeting_exists(page=logged_in_page, title=meeting_title)
    
    config.save("ElementMeetingTitle", meeting_title)

@allure.epic("Smoke")
@allure.feature("Element")
@allure.story("User")
@allure.title("Element: Join meeting (Invite guest)")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("element")
@pytest.mark.dependency(depends=["element_new_meeting"])
def test_element_start_meeting(logged_in_page: Page, page: Page, url_portal: str, component_admin: Config.User, run_id: str, config: Config):
    meeting_title: str | None = config.load("ElementMeetingTitle")
    assert meeting_title != None, "Meeting was not created"
    display_name: str = f"{component_admin.firstname} {component_admin.lastname}"
    guest_name: str = f"Guest {run_id}"

    participants = [display_name, guest_name]
    
    steps_element.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_element.step_open_home_page(page=logged_in_page, url_portal=url_portal)
    steps_element.step_open_appointment_scheduler(page=logged_in_page)
    steps_element.step_check_meeting_exists(page=logged_in_page, title=meeting_title)
    steps_element.step_join_meeting(page=logged_in_page, title=meeting_title)
    steps_element.step_check_meeting_join_page_opened(page=logged_in_page)
    steps_element.step_click_join_meeting(page=logged_in_page)
    steps_element.step_check_meeting_room_opened(page=logged_in_page)
    meeting_url: str = steps_element.step_get_meeting_room_url(page=logged_in_page)
    
    # Guest
    steps_element.step_join_meeting_with_url(page=page, url=meeting_url)
    steps_element.step_click_join_room(page=page)
    steps_element.step_fill_guest_join_name(page=page, name=guest_name)
    steps_element.step_click_join_as_guest(page=page)
    steps_element.step_click_join_room(page=page)
    
    steps_element.step_accept_guest_knock(page=logged_in_page, guest_name=guest_name)
    
    steps_element.step_click_join_room(page=page)
    steps_element.step_check_meeting_join_page_opened(page=page)
    steps_element.step_click_join_meeting(page=page)
    steps_element.step_check_meeting_room_opened(page=page)

    #Check participants
    steps_element.step_check_meeting_participants(page=page, participants=participants)
    steps_element.step_check_meeting_participants(page=logged_in_page, participants=participants)
    

@allure.epic("Smoke")
@allure.feature("Element")
@allure.story("User", "Security key", "Download")
@allure.title("Element: Reset security key")
@pytest.mark.dependency(name="setup_security_key")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("element")
def test_element_save_encryption_key(logged_in_page: Page, url_portal: str, config: Config):
    steps_element.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_element.step_click_user_menu(page=logged_in_page)
    steps_element.step_click_user_menu_security_button(page=logged_in_page)
    steps_element.step_click_delete_backup_button(page=logged_in_page)
    steps_element.step_click_confirm_delete_backup(page=logged_in_page)
    steps_element.step_click_setup_security_key_button(page=logged_in_page)
    steps_element.step_check_security_key_selected(page=logged_in_page)
    steps_element.step_click_next(page=logged_in_page)
    security_key: str = steps_element.step_click_download_key(page=logged_in_page)
    steps_element.step_click_next(page=logged_in_page)
    steps_element.step_check_setup_successful(page=logged_in_page)

    config.save("NewUserElementSecret", security_key)

