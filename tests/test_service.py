# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_service
import allure
import pytest
    
@allure.epic("Smoke")
@allure.feature("Service")
@allure.story("User")
@allure.title("Service: Base domain redirect")
@pytest.mark.user("user")
def test_base_domain_redirect(logged_in_page: Page, url_portal: str, base_domain: str):
    steps_service.step_open_base_domain(page=logged_in_page, base_domain=base_domain)
    steps_service.step_check_portal_url(page=logged_in_page, url_portal=url_portal)

@allure.epic("Smoke")
@allure.feature("Service")
@allure.title("Service: Namespace Image templating")
def test_namespace_image_templating(config: Config):
    # cluster: str = config.cluster
    namespace: str = config.namespace
    is_enterprise: bool = config.enterprise == "true"
    
    # steps_service.step_set_kubectl_context(cluster) # Uncomment when testing manually
    images = steps_service.step_get_pod_images(namespace=namespace)
    steps_service.step_check_image_registry(images=images, is_enterprise=is_enterprise)

@allure.epic("Smoke")
@allure.feature("Service")
@allure.title("Service: Keycloak Admin unavailable")
def test_keycloak_admin_unavailable(page: Page, url_portal: str):
    response = steps_service.step_open_keycloak_admin(page=page, url_portal=url_portal)
    steps_service.step_check_page_http_404(response=response)
    response = steps_service.step_open_keycloak_admin_with_slash(page=page, url_portal=url_portal)
    steps_service.step_check_page_http_404(response=response)

@allure.epic("Smoke")
@allure.feature("Service")
@allure.title("Service: UDM API unavailable")
@pytest.mark.env_setting("env_udm_api_available", False)
def test_udm_api_unavailable(url_portal: str):
    steps_service.step_check_udm_api_not_available(url_portal=url_portal)

@allure.epic("Nightly")
@allure.feature("Service")
@allure.title("Service: Pod restart thresholds")
def test_pod_restart_thresholds(config: Config, pod_restart_thresholds: dict):
    steps_service.step_check_pods(config=config, pod_restart_thresholds=pod_restart_thresholds)