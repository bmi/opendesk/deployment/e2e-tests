# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_nextcloud, steps_openproject
import allure
import pytest

@allure.epic("Smoke", "Regression")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Open downloaded attachment")
@pytest.mark.dependency(depends=["send_mail_attachment"], scope="session")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("save-attachment")
def test_nextcloud_download_file_attachment(logged_in_page: Page, url_portal: str, config: Config):
    filename: str | None = config.load("AttachmentFileName")
    assert filename != None
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_open_file(page=logged_in_page, filename=filename)
    steps_nextcloud.step_check_office_viewer_opened(page=logged_in_page)
    steps_nextcloud.step_close_document(page=logged_in_page)
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=filename)
    steps_nextcloud.step_click_delete(page=logged_in_page)
    steps_nextcloud.step_check_file_not_exists(page=logged_in_page, filename=filename)

@allure.epic("Smoke", "Integration")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Share file (External, Recommend autotest mail)")
@pytest.mark.user("component_admin")
@pytest.mark.env_setting("env_external_sharing", True)
@pytest.mark.xdist_group("ox_nc_integration")
@pytest.mark.dependency(depends=["send_mail"], scope="session")
@pytest.mark.files({".odt": 1})
def test_nextcloud_mail_recommended(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], autotest_mail: str, config: Config):
    subject: str | None = config.load("OxNcIntegrationSubject")
    assert subject != None, "Mail was not send to autotest mail."
    filename: str = nextcloud_files[1][0]
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=filename)
    steps_nextcloud.step_check_share_file_recommend(page=logged_in_page, mail=autotest_mail)

@allure.epic("Smoke", "Integration")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Connection to OpenProject")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("nc_op_integration")
@pytest.mark.dependency(name="nc_op_connection", scope="session")
def test_nextcloud_openproject_integration(logged_in_page: Page, url_portal: str):
    steps_openproject.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_nextcloud.step_open_openproject_integration_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_click_connect_to_openproject(page=logged_in_page)
    steps_nextcloud.step_click_authorize(page=logged_in_page)
    steps_nextcloud.step_check_authorization_success(page=logged_in_page)

@allure.epic("Smoke", "Integration")
@allure.feature("Nextcloud")
@allure.story("User")
@allure.title("Nextcloud: Create OpenProject work package")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("nc_op_integration")
@pytest.mark.dependency(name="create_nextcloud_work_package", depends=["nc_op_connection", "create_integration_project"], scope="session")
@pytest.mark.files({".odt": 1})
def test_nextcloud_openproject_integration_create_work_package(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], run_id: str, config: Config):
    project_name: str | None = config.load("IntegrationProjectName")
    assert project_name is not None, "Integration project was not created"
    
    filename: str = nextcloud_files[1][0]
    subject: str = f"Work package {run_id}"
    
    steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_nextcloud.step_open_file_dropdown_menu(page=logged_in_page, filename=filename)
    steps_nextcloud.step_click_open_details(page=logged_in_page)
    steps_nextcloud.step_open_openproject_tab(page=logged_in_page)
    steps_nextcloud.step_click_create_link_work_package(page=logged_in_page)
    steps_nextcloud.step_fill_work_package_subject(page=logged_in_page, subject=subject)
    steps_nextcloud.step_fill_work_package_project(page=logged_in_page, project_name=project_name)
    steps_nextcloud.step_select_work_package_project(page=logged_in_page, project_name=project_name)
    steps_nextcloud.step_click_create_work_package(page=logged_in_page)
    steps_nextcloud.step_check_work_package_created(page=logged_in_page, subject=subject)
    
    config.save("NCWorkPackageSubject", subject)