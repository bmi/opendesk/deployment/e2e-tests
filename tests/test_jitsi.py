# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_jitsi
import allure
import pytest

@allure.epic("Smoke")
@allure.feature("Jitsi")
@allure.story("User")
@allure.title("Jitsi: General availability")
@pytest.mark.user("user")
def test_jitsi_general_availability(logged_in_page: Page, url_portal: str):
    steps_jitsi.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_jitsi.step_general_availability(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Jitsi")
@allure.story("User", "Videoconference")
@allure.title("Jitsi: New conference (Two internal users, One external user)")
@pytest.mark.dependency(name="jitsi_new_conference")
@pytest.mark.users(["user", "component_admin"])
def test_jitsi_conference(page: Page, logged_in_pages: list[Page], url_portal: str, user: Config.User, component_admin: Config.User, run_id: str):
    conference_name: str = f"c{run_id}"
    participants: list[str] = []

    user_logged_in_page = logged_in_pages[0]
    with allure.step("Start conference with default user"):
        display_name: str = f"{user.firstname} {user.lastname}"
        steps_jitsi.step_open_page(page=user_logged_in_page, url_portal=url_portal)
        steps_jitsi.step_fill_conference_name(page=user_logged_in_page, conference_name=conference_name)
        steps_jitsi.step_click_start_conference_button(page=user_logged_in_page)
        steps_jitsi.step_check_display_name(page=user_logged_in_page, display_name=display_name)
        steps_jitsi.step_disable_audio(page=user_logged_in_page)
        steps_jitsi.step_disable_video(page=user_logged_in_page)
        steps_jitsi.step_enable_audio(page=user_logged_in_page)
        steps_jitsi.step_enable_video(page=user_logged_in_page)
        steps_jitsi.step_click_join_conference(page=user_logged_in_page)
        conference_url_user: str = steps_jitsi.step_check_joined_conference_room(page=user_logged_in_page)
        participants.append(display_name)
        steps_jitsi.step_check_participants(page=user_logged_in_page, participants=participants)
        steps_jitsi.step_check_large_video_displayed(page=user_logged_in_page)
    
    component_admin_logged_in_page = logged_in_pages[1]
    with allure.step("Join conference with other user"):
        display_name: str = f"{component_admin.firstname} {component_admin.lastname}"
        steps_jitsi.step_open_page(page=component_admin_logged_in_page, url_portal=url_portal)
        steps_jitsi.step_fill_conference_name(page=component_admin_logged_in_page, conference_name=conference_name)
        steps_jitsi.step_click_start_conference_button(page=component_admin_logged_in_page)
        steps_jitsi.step_check_display_name(page=component_admin_logged_in_page, display_name=display_name)
        steps_jitsi.step_disable_audio(page=component_admin_logged_in_page)
        steps_jitsi.step_disable_video(page=component_admin_logged_in_page)
        steps_jitsi.step_enable_video(page=component_admin_logged_in_page)
        steps_jitsi.step_click_join_conference(page=component_admin_logged_in_page)
        conference_url_second_user: str = steps_jitsi.step_check_joined_conference_room(page=component_admin_logged_in_page)
        participants.append(display_name)
        steps_jitsi.step_check_participants(page=user_logged_in_page, participants=participants)
        steps_jitsi.step_check_participants(page=component_admin_logged_in_page, participants=participants)
        steps_jitsi.step_check_large_video_displayed(page=user_logged_in_page)
        steps_jitsi.step_check_large_video_displayed(page=component_admin_logged_in_page)

    with allure.step("Check conference url matches from first two users"):
        assert conference_url_user == conference_url_second_user
    
    with allure.step("Join conference via link"):
        page.goto(conference_url_user)
        display_name: str = f"e{run_id}"
        steps_jitsi.step_fill_display_name(page=page, display_name=display_name)
        steps_jitsi.step_check_display_name(page=page, display_name=display_name)
        steps_jitsi.step_disable_audio(page=page)
        steps_jitsi.step_disable_video(page=page)
        steps_jitsi.step_enable_video(page=page)
        steps_jitsi.step_click_join_conference(page=page)
        steps_jitsi.step_check_joined_conference_room(page=page)
        participants.append(display_name)
        steps_jitsi.step_check_participants(page=page, participants=participants)
        steps_jitsi.step_check_participants(page=user_logged_in_page, participants=participants)
        steps_jitsi.step_check_participants(page=component_admin_logged_in_page, participants=participants)
        for participant in participants:
            steps_jitsi.step_check_filmstrip_video(page=page, participant_name=participant)
