# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import datetime
import os
import re
from smtplib import SMTP_SSL
from playwright.sync_api import Page, Locator
from tests.config.config import Config
from tests.steps import steps_ox_mail, steps_mail
import allure
import pytest
from imap_tools.mailbox import MailBox

@allure.epic("Smoke", "Regression")
@allure.feature("OX", "Mail")
@allure.story("User", "Send mail", "Look for Mail", "Receive mail")
@allure.title("OXmail: Send and receive mail with attachment")
@pytest.mark.dependency(name="send_mail_attachment", scope="session")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("save-attachment")
@pytest.mark.files({"odt": 1})
def test_ox_send_receive_mail_with_attachment(logged_in_page: Page, url_portal: str, temp_files: tuple[str, list], run_id: str, component_admin: Config.User, config: Config):
    folder: str = temp_files[0]
    files: list = temp_files[1]
    subject: str = f"Attachment: {run_id}"
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    
    steps_ox_mail.step_fill_internal_recipient(page=logged_in_page, username=component_admin.username)
    steps_ox_mail.step_click_recipient(page=logged_in_page, username=component_admin.username)
    steps_ox_mail.step_fill_subject(page=logged_in_page, subject=subject)
    steps_ox_mail.step_click_attachments_button(page=logged_in_page)
    steps_ox_mail.step_mail_upload_attachments(page=logged_in_page, folder=folder, files=files)
    steps_ox_mail.step_send_mail(page=logged_in_page)
    attachment_mail = steps_ox_mail.step_receive_mail(page=logged_in_page, subject=subject)
    steps_ox_mail.step_open_mail(page=logged_in_page, mail=attachment_mail)
    steps_ox_mail.step_click_save_to_files(page=logged_in_page)
    steps_ox_mail.step_save_attachment_to_files(page=logged_in_page)
    
    config.save("AttachmentFileName", files[0])
    
@allure.epic("Smoke")
@allure.feature("OX")
@allure.story("User", "UI")
@allure.title("OXmail: General availability")
@pytest.mark.dependency(name="check_ox_page")
@pytest.mark.user("component_admin")
def test_ox_mail_general_availablity(logged_in_page: Page, url_portal: str):
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_check_ox_top_left_bar(page=logged_in_page)
    steps_ox_mail.step_check_ox_search_bar(page=logged_in_page)
    steps_ox_mail.step_check_ox_top_right_bar(page=logged_in_page)
    
@allure.epic("Smoke")
@allure.feature("OX", "Mail")
@allure.story("User", "Look for mail", "Read mail", "Security")
@allure.title("OXmail: New device notification received")
@pytest.mark.dependency(name="check_new_device_notification")
@pytest.mark.user("component_admin")
@pytest.mark.env_setting("env_new_device_notification", True)
def test_ox_new_device_notification_received(logged_in_page: Page, url_portal: str, test_start_timestamp: datetime.datetime):
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_open_new_device_notification(page=logged_in_page)
    steps_ox_mail.step_check_new_device_notification_timestamp(page=logged_in_page, test_start_timestamp=test_start_timestamp)

@allure.epic("Smoke")
@allure.feature("OX", "Mail")
@allure.story("User", "Look for mail", "Read mail", "Security")
@allure.title("OXmail: New device notification not received")
@pytest.mark.env_setting("env_new_device_notification", False)
@pytest.mark.dependency(name="check_new_device_notification")
@pytest.mark.user("component_admin")
def test_ox_new_device_notification_not_received(logged_in_page: Page, url_portal: str):
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_check_new_device_notification_not_received(page=logged_in_page)

@allure.epic("Smoke", "Load")
@allure.feature("OX", "Mail")
@allure.story("User", "Write mail")
@allure.title("OXmail: Send mail with attachments")
@pytest.mark.dependency(name="check_mail")
@pytest.mark.user("component_admin")
@pytest.mark.files({re.compile(r"\.od[tps]$"): 2})
def test_ox_check_mail(logged_in_page: Page, url_portal: str, component_admin: Config.User, temp_files: tuple[str, list], run_id: str):
    temp_folder, files = temp_files
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_fill_internal_recipient(page=logged_in_page, username=component_admin.username)
    steps_ox_mail.step_click_recipient(page=logged_in_page, username=component_admin.username)
    steps_ox_mail.step_fill_subject(page=logged_in_page, subject=f"Test: {run_id}")
    steps_ox_mail.step_click_attachments_button(page=logged_in_page)
    steps_ox_mail.step_mail_upload_attachments(page=logged_in_page, folder=temp_folder, files=files)
    
    for file in files:
        steps_ox_mail.step_check_attachment_preview(page=logged_in_page, filename=file)
    
    steps_ox_mail.step_send_mail(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("OX", "Mail")
@allure.story("User", "Write mail")
@allure.title("OXmail: Send mail (Send from OX, Receive externally)")
@pytest.mark.dependency(name="send_mail", scope="session")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("ox_nc_integration")
def test_ox_mail_send(logged_in_page: Page, url_portal: str, run_id: str, autotest_mail: str, mailbox: MailBox, config: Config):
    subject: str = f"Check send: {run_id}"
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_fill_recipient_email(page=logged_in_page, email=autotest_mail)
    steps_ox_mail.step_fill_subject(page=logged_in_page, subject=subject)
    steps_ox_mail.step_send_mail(page=logged_in_page)
    
    config.save("OxNcIntegrationSubject", subject)
    
    steps_mail.step_receive_email(mailbox=mailbox, subject=subject)
    
@allure.epic("Smoke")
@allure.feature("OX", "Mail")
@allure.story("User", "Read mail")
@allure.title("OXmail: Receive mail (Send externally, Receive in OX)")
@pytest.mark.dependency(name="receive_mail")
@pytest.mark.user("component_admin")
def test_ox_mail_receive(logged_in_page: Page, url_portal: str, run_id: str, component_admin: Config.User, base_domain: str, autotest_mail: str, smtp: SMTP_SSL):
    user_email: str = f"{component_admin.username}@{base_domain}"
    subject: str = f"Check receive: {run_id}"
    
    steps_mail.step_send_mail(smtp=smtp, sender=autotest_mail, receiver=user_email, subject=subject)
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_receive_mail(page=logged_in_page, subject=subject)

@allure.epic("Smoke")
@allure.feature("OX", "Mail")
@allure.story("User")
@allure.title("OXmail: Malicous attachment warning (Save draft with malicious attachment, Download attachment, Malicious file warning)")
@pytest.mark.user("user")
@pytest.mark.files({"eicar.txt": 1})
def test_ox_malicious_attachment_warning(logged_in_page: Page, url_portal: str, temp_files: tuple[str, list], user: Config.User, run_id: str):
    username: str = user.username
    subject: str = f"Malware Draft {run_id}"
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_fill_internal_recipient(page=logged_in_page, username=username)
    steps_ox_mail.step_fill_subject(page=logged_in_page, subject=subject)
    steps_ox_mail.step_click_attachments_button(page=logged_in_page)
    steps_ox_mail.step_mail_upload_attachments(page=logged_in_page, folder=temp_files[0], files=temp_files[1])
    steps_ox_mail.step_close_mail_window(page=logged_in_page)
    steps_ox_mail.step_click_save_draft_button(page=logged_in_page)
    
    steps_ox_mail.step_open_drafts_folder(page=logged_in_page)
    draft: Locator = steps_ox_mail.step_get_draft_by_subject(page=logged_in_page, subject=subject)
    steps_ox_mail.step_open_mail(page=logged_in_page, mail=draft)
    steps_ox_mail.step_download_attachments(page=logged_in_page)
    steps_ox_mail.step_check_malicious_file_modal_shown(page=logged_in_page)
    

@allure.epic("Smoke")
@allure.feature("OX", "Mail")
@allure.story("User", "Security")
@allure.title("OXmail: Receive mail with malicious attachment (Send externally, Mail not received)")
@pytest.mark.user("user")
@pytest.mark.files({"eicar.txt": 1})
def test_ox_not_receive_mail_with_malicious_attachment(logged_in_page: Page, url_portal: str, temp_files: tuple[str, list], user: Config.User, smtp: SMTP_SSL, autotest_mail: str, run_id: str):
    subject: str = f"Test Malware Receive {run_id}"
    
    steps_mail.step_send_mail_with_attachments(smtp=smtp, sender=autotest_mail, receiver=user.email, subject=subject, attachments=[f"{temp_files[0]}{os.sep}{file}" for file in temp_files[1]])
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    
    steps_ox_mail.step_mail_by_subject_not_found(page=logged_in_page, subject=subject)

@allure.epic("Smoke")
@allure.feature("OX", "Mail")
@allure.story("User", "Send mail", "Security")
@allure.title("OXmail: Send mail with malicious attachment (Send mail yourself, Mail not received)")
@pytest.mark.user("user")
@pytest.mark.files({"eicar.txt": 1})
def test_ox_send_and_not_receive_mail_with_malicious_attachment(logged_in_page: Page, url_portal: str, temp_files: tuple[str, list], user: Config.User, run_id: str):
    subject: str = f"Test Malware Send {run_id}"
    folder: str = temp_files[0]
    files: list = temp_files[1]
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_fill_internal_recipient(page=logged_in_page, username=user.username)
    steps_ox_mail.step_fill_subject(page=logged_in_page, subject=subject)
    steps_ox_mail.step_click_attachments_button(page=logged_in_page)
    steps_ox_mail.step_mail_upload_attachments(page=logged_in_page, folder=folder, files=files)
    steps_ox_mail.step_send_mail(page=logged_in_page)
    
    steps_ox_mail.step_mail_by_subject_not_found(page=logged_in_page, subject=subject)

@allure.epic("Smoke")
@allure.feature("OX", "Mail")
@allure.story("User", "Address list")
@allure.title("OXmail: Select global contact list in address picker")
@pytest.mark.user("user")
@pytest.mark.users_dependency(["component_admin"])
def test_ox_global_contact_list(logged_in_page: Page, url_portal: str, component_admin: Config.User):
    username: str = component_admin.username
    email: str = component_admin.email
    firstname: str = component_admin.firstname
    lastname: str = component_admin.lastname

    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_click_contacts_button(page=logged_in_page)
    steps_ox_mail.step_input_user_in_global_address_list(page=logged_in_page, username=username)
    entry = steps_ox_mail.step_check_contact_appears_in_contact_list(page=logged_in_page, email=re.compile(f"^{re.escape(email)}$"))
    steps_ox_mail.step_select_contact(page=logged_in_page, entry=entry)
    steps_ox_mail.step_click_select_contacts_button(page=logged_in_page)
    steps_ox_mail.step_check_if_contact_is_in_recipient_list(page=logged_in_page, firstname=firstname, lastname=lastname)

@allure.epic("Smoke")
@allure.feature("OX", "Mail")
@allure.story("User")
@allure.title("OXmail: Create folder")
@pytest.mark.user("user")
@pytest.mark.xdist_group("folder")
@pytest.mark.dependency(name="create_folder")
def test_ox_create_folder(logged_in_page: Page, url_portal: str, run_id: str, config: Config):
    folder_name: str = f"f{run_id}"
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_add_folder(page=logged_in_page)
    steps_ox_mail.step_click_add_subfolder(page=logged_in_page)
    steps_ox_mail.step_fill_folder_name(page=logged_in_page, folder_name=folder_name)
    steps_ox_mail.step_save_folder(page=logged_in_page)
    steps_ox_mail.step_check_folder_exists(page=logged_in_page, folder_name=folder_name)
    
    config.save("FolderName", folder_name)

@allure.epic("Smoke")
@allure.feature("OX", "Mail")
@allure.story("User")
@allure.title("OXmail: Move mail to folder")
@pytest.mark.user("user")
@pytest.mark.xdist_group("folder")
@pytest.mark.dependency(depends=["create_folder"])
def test_ox_move_mail_to_folder(logged_in_page: Page, url_portal: str, run_id: str, user: Config.User, config: Config):
    folder_name: str | None = config.load("FolderName")
    assert folder_name != None, "Folder was not created."
    subject: str = f"Move Mail {run_id}"
    
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_fill_internal_recipient(page=logged_in_page, username=user.username)
    steps_ox_mail.step_click_recipient(page=logged_in_page, username=user.username)
    steps_ox_mail.step_fill_subject(page=logged_in_page, subject=subject)
    steps_ox_mail.step_send_mail(page=logged_in_page)
    mail: Locator = steps_ox_mail.step_receive_mail(page=logged_in_page, subject=subject)
    steps_ox_mail.step_open_mail(page=logged_in_page, mail=mail)
    steps_ox_mail.step_click_move_to_folder(page=logged_in_page)
    steps_ox_mail.step_move_mail_expand_my_folders(page=logged_in_page)
    steps_ox_mail.step_move_mail_select_folder(page=logged_in_page, folder_name=folder_name)
    steps_ox_mail.step_move_mail_confirm(page=logged_in_page)
    steps_ox_mail.step_open_folder(page=logged_in_page, folder_name=folder_name)
    steps_ox_mail.step_receive_mail(page=logged_in_page, subject=subject, status="all")

@allure.epic("Smoke")
@allure.feature("OX", "Mail")
@allure.story("User")
@allure.title("OXmail: Send mail to functional account")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("functional_account")
@pytest.mark.dependency(name="email_to_functional_account", depends=["create_functional_account"], scope="session")
def test_ox_send_mail_to_functional_account(logged_in_page: Page, url_portal: str, component_admin: Config.User, run_id: str):
    username = component_admin.username
    functional_account_name: str = f"fa.{username}"
    subject: str = f"Mail to functional mailbox for run {run_id}"

    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_fill_internal_recipient(page=logged_in_page, username=functional_account_name)
    steps_ox_mail.step_click_recipient(page=logged_in_page, username=functional_account_name)
    steps_ox_mail.step_fill_subject(page=logged_in_page, subject=subject)
    steps_ox_mail.step_send_mail(page=logged_in_page)
    steps_ox_mail.step_expand_functional_account(page=logged_in_page, functional_account_name=functional_account_name)
    steps_ox_mail.step_open_functional_account_inbox(page=logged_in_page, functional_account_name=functional_account_name)
    steps_ox_mail.step_receive_mail(page=logged_in_page, contains_subject=subject, sender=component_admin.email)

@allure.epic("Smoke")
@allure.feature("OX", "Mail")
@allure.story("User")
@allure.title("OXmail: Send mail from functional account")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("functional_account")
@pytest.mark.dependency(name="email_from_functional_account", depends=["create_functional_account"], scope="session")
def test_ox_send_mail_from_functional_account(logged_in_page: Page, url_portal: str, component_admin: Config.User, run_id: str, base_domain: str):
    username = component_admin.username
    functional_account_name: str = f"fa.{username}"
    subject: str = f"Mail from functional mailbox for run {run_id}"
    email: str = f"{functional_account_name}@{base_domain}"

    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_click_new_mail(page=logged_in_page)
    steps_ox_mail.step_select_sender(page=logged_in_page, sender=functional_account_name)
    steps_ox_mail.step_fill_internal_recipient(page=logged_in_page, username=component_admin.username)
    steps_ox_mail.step_click_recipient(page=logged_in_page, username=component_admin.username)
    steps_ox_mail.step_fill_subject(page=logged_in_page, subject=subject)
    steps_ox_mail.step_send_mail(page=logged_in_page)
    steps_ox_mail.step_receive_mail(page=logged_in_page, contains_subject=subject, sender=email)

