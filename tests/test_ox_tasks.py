# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import re
from playwright.sync_api import Page 
import allure
import pytest

from tests.steps import steps_ox_tasks
from tests.config.config import Config

@allure.epic("Smoke")
@allure.feature("OX", "Tasks")
@allure.story("User")
@allure.title("OXtasks: Global address list in Tasks")
@pytest.mark.user("user")
@pytest.mark.users_dependency(["component_admin"])
def test_ox_tasks_global_address_list(logged_in_page: Page, url_portal: str, component_admin: Config.User):
    username: str = component_admin.username
    email: str = component_admin.email
    firstname: str = component_admin.firstname
    lastname: str = component_admin.lastname

    steps_ox_tasks.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_tasks.step_click_new_task_button(page=logged_in_page)
    steps_ox_tasks.step_click_expand_form_button(page=logged_in_page)
    steps_ox_tasks.step_check_if_contact_row_is_visible(page=logged_in_page)
    steps_ox_tasks.step_click_pick_contact_button(page=logged_in_page)
    steps_ox_tasks.step_check_if_address_book_visible(page=logged_in_page)

    steps_ox_tasks.step_input_user_in_global_address_list(page=logged_in_page, username=username)
    entry = steps_ox_tasks.step_check_contact_appears_in_contact_list(page=logged_in_page, email=re.compile(f"^{re.escape(email)}$"))
    steps_ox_tasks.step_select_contact(page=logged_in_page, entry=entry)
    steps_ox_tasks.step_click_select_contacts_button(page=logged_in_page)
    steps_ox_tasks.step_check_if_contact_is_in_participant_list(page=logged_in_page, firstname=firstname, lastname=lastname)
