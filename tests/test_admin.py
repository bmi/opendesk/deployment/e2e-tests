# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_admin_functional_accounts, steps_admin_users, steps_admin_ox_resources, steps_admin_users, steps_admin_announcements
import allure
import pytest

@allure.epic("Smoke")
@allure.feature("Users")
@allure.story("Admin", "Create user")
@allure.title("Users: Create a new user")
@pytest.mark.dependency(name="create_user", scope="session")
@pytest.mark.xdist_group("new_user-group")
@pytest.mark.user("user_admin")
def test_create_user(logged_in_page: Page, url_portal: str, autotest_mail: str, new_user: Config.User, base_domain: str):
    steps_admin_users.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_admin_users.step_users_click_add_user(page=logged_in_page)
    
    steps_admin_users.step_users_create_user(page=logged_in_page, firstname=new_user.firstname, lastname=new_user.lastname, username=new_user.username, user_email=autotest_mail, base_domain=base_domain)

@allure.epic("Smoke")
@allure.feature("Function accounts")
@allure.story("Admin", "Create functional account")
@allure.title("Function accounts: Create a functional account")
@pytest.mark.dependency(name="create_functional_account", scope="session")
@pytest.mark.user("user_admin")
@pytest.mark.users_dependency(["component_admin"])
@pytest.mark.xdist_group("functional_account")
def test_create_functional_account(logged_in_page: Page, url_portal: str, base_domain: str, component_admin: Config.User, config: Config):
    steps_admin_functional_accounts.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_admin_functional_accounts.step_click_add_object(page=logged_in_page)
    
    username = component_admin.username
    functional_account_name: str = f"fa.{username}"
    
    functional_account_email: str = steps_admin_functional_accounts.step_create_object(page=logged_in_page, name=functional_account_name, username=username, base_domain=base_domain)
    
    config.save("FunctionalAccountName", functional_account_name)
    config.save("FunctionalAccountMail", functional_account_email)

@allure.epic("Smoke")
@allure.feature("Function accounts")
@allure.story("Admin", "Delete functional account")
@allure.title("Function accounts: Create and delete a functional account")
@pytest.mark.user("user_admin")
@pytest.mark.users_dependency(["component_admin"])
@pytest.mark.xdist_group("functional_account")
def test_create_and_delete_functional_account(logged_in_page: Page, url_portal: str, base_domain: str, component_admin: Config.User, run_id: str):
    username = component_admin.username
    functional_account_name: str = f"fa_temp{run_id}.{username}"

    steps_admin_functional_accounts.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_admin_functional_accounts.step_click_add_object(page=logged_in_page)
    steps_admin_functional_accounts.step_create_object(page=logged_in_page, name=functional_account_name, username=username, base_domain=base_domain)
    steps_admin_functional_accounts.step_delete_object(page=logged_in_page, name=functional_account_name, username=username, base_domain=base_domain)

@allure.epic("Smoke")
@allure.feature("OX Resources")
@allure.story("Admin", "Create OX resource")
@allure.title("OX Resources: Create a OX resource")
@pytest.mark.dependency(name="create_ox_resource", scope="session")
@pytest.mark.user("user_admin")
@pytest.mark.users_dependency(["component_admin"])
@pytest.mark.xdist_group("ox_resource")
def test_create_ox_resource(logged_in_page: Page, url_portal: str, base_domain: str, component_admin: Config.User, config: Config):
    ox_resource_manager: str = component_admin.username
    ox_resource_name: str = f"ox.{ox_resource_manager}"
    ox_resource_mail: str = f"{ox_resource_name}@{base_domain}"
    
    steps_admin_ox_resources.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_admin_ox_resources.step_click_add_object(page=logged_in_page)
    steps_admin_ox_resources.step_create_object(page=logged_in_page, name=ox_resource_name, resource_email=ox_resource_mail, resource_manager=ox_resource_manager)
    
    config.save("OXResourceName", ox_resource_name)

@allure.epic("Smoke")
@allure.feature("Announcements")
@allure.story("Announcement")
@allure.title("Announcements: Create public announcement")
@pytest.mark.dependency(name="create_public_announcement", scope="session")
@pytest.mark.xdist_group("announcements_public")
@pytest.mark.user("user_admin")
def test_announcement_create_public_announcement(logged_in_page: Page, url_portal: str, run_id: str, locale: str, config: Config):
    name: str = f"public{run_id}"
    language_code: str = locale.replace("-", "_")
    title: str = f"Public {run_id}"
    
    steps_admin_announcements.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_admin_announcements.step_click_add_object(page=logged_in_page)
    steps_admin_announcements.step_create_object(page=logged_in_page, name=name, language_code=language_code, title=title)
    
    config.save("AnnouncementPublicTitle", title)

@allure.epic("Smoke")
@allure.feature("Announcements")
@allure.story("Announcement")
@allure.title("Announcements: Create private announcement")
@pytest.mark.dependency(name="create_private_announcement", scope="session")
@pytest.mark.xdist_group("announcements_private")
@pytest.mark.user("user_admin")
def test_announcement_create_private_announcement(logged_in_page: Page, url_portal: str, run_id: str, locale: str, config: Config):
    name: str = f"private{run_id}"
    language_code: str = locale.replace("-", "_")
    title: str = f"Private {run_id}"
    group: str = "2FA Testing"
    
    steps_admin_announcements.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_admin_announcements.step_click_add_object(page=logged_in_page)
    steps_admin_announcements.step_create_object(page=logged_in_page, name=name, language_code=language_code, title=title, group=group)
    
    config.save("AnnouncementPrivateTitle", title)
