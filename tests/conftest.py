# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import pytest

from tests.fixtures.base import *
from tests.fixtures.opendesk import *
from tests.fixtures.opentalk import *
from tests.fixtures.test_export import *


def pytest_addoption(parser: pytest.Parser):
    """
    See pytest initialization hooks: pytest_addoption.
    :param parser: argparser
    :return: nothing
    """
    config_parser_init(parser)
    
def pytest_configure(config: pytest.Config):
    config.addinivalue_line(
        "markers", "env_setting(setting, value, message): skip test profile setting does not equal value.")
    config.addinivalue_line(
        "markers", "dependency_or(list_of_depends): skips test if none of the tests in list_of_depends pass.")
    config.addinivalue_line(
        "markers", "user(key): set user for logged_in_page fixture.")
    config.addinivalue_line(
        "markers", "users(key): set users for logged_in_pages fixture.")
    config.addinivalue_line(
        "markers", "users_dependency(key): set users the test depends on beside the ones defined in user and users.")
    config.addinivalue_line(
        "markers", "files(filedict): generates files and to Nextcloud to use inside testcase. file dict of form: '{fileformat}: {amount}. E.g. {'odt': 3} for generating 3 odt files.'")
    config.addinivalue_line(
        "markers", "temp_user(user_options): set additional options for user created with test_user fixture.")
