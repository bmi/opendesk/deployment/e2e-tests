# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import datetime
from email.message import EmailMessage
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import json
from smtplib import SMTP_SSL
import time
from playwright.sync_api import Page
import pytest
from pages.base.base import expect
from pages.portal.page import PortalPage
from tests.utils import screenshot_step, retry
import allure
from imap_tools import MailBox, MailMessage, AND
from os.path import basename

@allure.step("Receive invitation email")
def step_receive_reset_password_email(mailbox: MailBox, username: str) -> MailMessage:
    return step_receive_email(mailbox=mailbox, body=f"username={username}")

@allure.step("Open new password link inside mail")
@screenshot_step
def step_open_new_password_link(page: Page, username: str, url_portal: str, mail_message: MailMessage):
    message_text: str = mail_message.text or mail_message.html
    
    assert message_text
    
    message_rows: list[str] = message_text.split("\n")
    
    link: str | None = None
    for row in message_rows:
        row = row.strip()
        if row.startswith(url_portal) and row.endswith(username):
            link = row
            break
    
    assert link != None
    
    page.goto(link)
    
    portal_page: PortalPage = PortalPage(page)
    portal_page.validate()
    expect(portal_page.new_password_modal).to_be_visible()

@allure.step("Receive email")
def step_receive_email(mailbox: MailBox, subject: str | None = None, body: str | None = None, send_after: datetime.datetime | None = None ,timeout_in_sec: int = 300) -> MailMessage:
    start: int = int(time.time())
    end: int = start + timeout_in_sec
    while time.time() < end:
        time.sleep(1)
        mails = [i for i in mailbox.fetch(mark_seen=True, criteria=AND(seen=False, body=body, subject=subject), charset="UTF-8") if not send_after or i.date > send_after.replace(tzinfo=i.date.tzinfo)]
        if len(mails) != 1:
            continue
        mail: MailMessage = mails[0]
        allure.attach(body=mail.text or mail.html, name="Mail-Body", attachment_type=allure.attachment_type.TEXT)
        return mail

@allure.step("Do not receive email")
def step_not_receive_email(mailbox: MailBox, subject: str | None = None, body: str | None = None) -> MailMessage:
    try:
        step_receive_email(mailbox=mailbox, subject=subject, body=body, timeout_in_sec=150)
        pytest.fail("Mail was received.")
    except:
        pass

@allure.step("Send mail")
def step_send_mail(smtp: SMTP_SSL, sender: str, receiver: str, subject: str, body: str | None = None):
    message: MIMEMultipart = MIMEMultipart()
    message["Subject"] = subject
    message["From"] = sender
    message["To"] = receiver
    if body:
        message.attach(MIMEText(body))
    
    send_errors = smtp.send_message(msg=message, from_addr=sender, to_addrs=receiver)
    allure.attach(message.as_string(), "SMTP Mail Message", attachment_type=allure.attachment_type.TEXT)
    allure.attach(json.dumps(send_errors or {}), "SMTP Mail Send Errors", attachment_type=allure.attachment_type.TEXT)

@allure.step("Send mail with attachments")
def step_send_mail_with_attachments(smtp: SMTP_SSL, sender: str, receiver: str, subject: str, body: str | None = None, attachments: list[str] = None):
    message: MIMEMultipart = MIMEMultipart()
    message["Subject"] = subject
    message["From"] = sender
    message["To"] = receiver
    if body:
        message.attach(MIMEText(body))
    
    for f in attachments or []:
        with open(f, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(f)
            )
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        message.attach(part)
    
    send_errors = smtp.send_message(msg=message, from_addr=sender, to_addrs=receiver)
    allure.attach(message.as_string(), "SMTP Mail Message", attachment_type=allure.attachment_type.TEXT)
    allure.attach(json.dumps(send_errors or {}), "SMTP Mail Send Errors", attachment_type=allure.attachment_type.TEXT)