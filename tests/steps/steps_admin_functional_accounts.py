# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
import pytest
from pages.base.base import expect
from pages.management.page import *
from tests.utils import screenshot_step
import allure

@allure.step("Open Functional Accounts page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    FunctionalAccountsPage(page).open_page(url_portal)
    FunctionalAccountsPage(page).validate()

@allure.step("Click add functional account button")
@screenshot_step
def step_click_add_object(page: Page):
    functional_accounts_page: FunctionalAccountsPage = FunctionalAccountsPage(page)
    
    expect(functional_accounts_page.add_button).to_be_visible()
    functional_accounts_page.add_button.click()
    
    edit_functional_account: EditFunctionalAccountPage = EditFunctionalAccountPage(page)
    
    expect(edit_functional_account.email_field).to_be_visible()

@allure.step("Fill functional account name")
@screenshot_step
def step_fill_fa_name(page: Page, name: str):
    edit_functional_account: EditFunctionalAccountPage = EditFunctionalAccountPage(page)
    expect(edit_functional_account.email_field).to_be_visible()
    edit_functional_account.email_field.clear()
    edit_functional_account.name_field.type(name)
    expect(edit_functional_account.name_field).to_have_value(name)

@allure.step("Fill functional account email")
@screenshot_step
def step_fill_fa_email(page: Page, email: str):
    edit_functional_account: EditFunctionalAccountPage = EditFunctionalAccountPage(page)
    expect(edit_functional_account.email_field).to_be_visible()
    edit_functional_account.email_field.clear()
    edit_functional_account.email_field.type(email)
    expect(edit_functional_account.email_field).to_be_visible()
    expect(edit_functional_account.email_field).to_have_value(email)
    
@allure.step("Click users add button")
@screenshot_step
def step_click_add_users(page: Page):
    edit_functional_account: EditFunctionalAccountPage = EditFunctionalAccountPage(page)
    expect(edit_functional_account.users_add_button).to_be_visible()
    edit_functional_account.users_add_button.click()
    
    edit_functional_account.add_user_modal.get_down_arrow().click()
    edit_functional_account.add_user_modal.property_select.wait_for()
    edit_functional_account.add_user_modal.get_down_arrow().click()
    
    expect(edit_functional_account.add_user_modal.property_select_hidden).to_have_value("None")

@allure.step("Fill user username")
@screenshot_step
def step_fill_user_username(page: Page, username: str):
    edit_functional_account: EditFunctionalAccountPage = EditFunctionalAccountPage(page)
    
    edit_functional_account.add_user_modal.search_field.clear()
    edit_functional_account.add_user_modal.search_field.type(username)
    edit_functional_account.add_user_modal.search_field.press("Enter")

@allure.step("Select user from list")
@screenshot_step
def step_select_user(page: Page, username: str):
    edit_functional_account: EditFunctionalAccountPage = EditFunctionalAccountPage(page)
    
    username_row: Locator = edit_functional_account.add_user_modal.get_add_object_table_entry(username)
    expect(username_row).to_be_visible(timeout=20000)
    
    username_row.locator("> td > div").click()

@allure.step("Click add user")
@screenshot_step
def step_add_user(page: Page):
    edit_functional_account: EditFunctionalAccountPage = EditFunctionalAccountPage(page)
    
    expect(edit_functional_account.add_user_modal.add_button).to_be_visible()
    edit_functional_account.add_user_modal.add_button.click()

@allure.step("Click save")
@screenshot_step
def step_click_save(page: Page):
    edit_functional_account: EditFunctionalAccountPage = EditFunctionalAccountPage(page)
    
    expect(edit_functional_account.save_button).to_be_visible()
    edit_functional_account.save_button.click()

@allure.step("Check object appears in list")
@screenshot_step
def step_check_object_created(page: Page, name: str):
    functional_accounts_page: FunctionalAccountsPage = FunctionalAccountsPage(page)
    
    functional_accounts_page.add_button.wait_for(state="visible")
    
    page.wait_for_timeout(500)
    
    functional_accounts_page.search_field.fill(name)
    functional_accounts_page.search_field.press("Enter")
    
    expect(functional_accounts_page.get_object_table_entry(name)).to_be_visible()

@allure.step("Open functional account to delete")
@screenshot_step
def step_select_fa(page: Page, account_name: str):
    functional_accounts_page: FunctionalAccountsPage = FunctionalAccountsPage(page)
    
    expect(functional_accounts_page.search_field).to_be_visible()
    functional_accounts_page.search_field.type(account_name)
    functional_accounts_page.search_field.press("Enter")

    entry = functional_accounts_page.get_object_table_entry(account_name)
    expect(entry).to_be_visible()
    entry.click()

@allure.step("Delete functional account")
@screenshot_step
def step_delete_fa(page: Page):
    functional_accounts_page: FunctionalAccountsPage = FunctionalAccountsPage(page)

    expect(functional_accounts_page.delete_button).to_be_visible()
    functional_accounts_page.delete_button.click()

    expect(functional_accounts_page.delete_entry_dialog).to_be_visible()

    expect(functional_accounts_page.delete_entry_dialog.delete_button).to_be_visible()
    functional_accounts_page.delete_entry_dialog.delete_button.click()

@allure.step("Check functional account is deleted")
@screenshot_step
def step_check_object_deleted(page: Page, name: str):
    functional_accounts_page: FunctionalAccountsPage = FunctionalAccountsPage(page)
    
    functional_accounts_page.search_field.clear()
    functional_accounts_page.search_field.fill(name)
    functional_accounts_page.search_field.press("Enter")
    
    try:
        expect(functional_accounts_page.get_object_table_entry(name)).to_be_visible(timeout=5000)
    except:
        return

    pytest.fail("Functional account was still visible")
    
@allure.step("Create functional account")
@screenshot_step
def step_create_object(page: Page, name: str, username: str, base_domain: str) -> str:
    email: str = f"{name}@{base_domain}"
    
    step_fill_fa_name(page=page, name=name)
    step_fill_fa_email(page=page, email=email)
    step_click_add_users(page=page)
    step_fill_user_username(page=page, username=username)
    step_select_user(page=page, username=username)
    step_add_user(page=page)
    step_click_save(page=page)
    step_check_object_created(page=page, name=name)
    
    return email
    
def step_delete_object(page: Page, name: str, username: str, base_domain: str) -> str:
    step_select_fa(page=page, account_name=name)
    step_delete_fa(page=page)
    step_check_object_deleted(page=page, name=name)

