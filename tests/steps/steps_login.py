# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from pyotp import TOTP
from pages.base.base import expect
from pages.login.page import *
from pages.portal.page import *
from pages.portal.self_service import *
from tests.config.config import Config
from tests.utils import retry, screenshot_step
import allure
from pages.login.page import Login2FAPage

@allure.step("Open Login page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    login_page: LoginPage = LoginPage(page)

    login_page.open_page(url_portal)

    login_page.validate()

@allure.step("Check login page and form visible")
@screenshot_step
def step_check_login_page(page: Page):
    login_page: LoginPage = LoginPage(page)

    login_page.validate()
    expect(login_page.login_form).to_be_visible()

@allure.step("Check all login form fields")
@screenshot_step
def step_check_form_fields(page: Page):
    login_page: LoginPage = LoginPage(page)

    expect(login_page.username_field).to_be_visible()
    expect(login_page.password_field).to_be_visible()
    expect(login_page.submit_form_button).to_be_visible()

@allure.step("Filling login fields")
@screenshot_step
def step_fill_login(page: Page, username: str, password: str):
    login_page: LoginPage = LoginPage(page)

    login_page.fill_login(username, password)

@allure.step("Click login")
@screenshot_step
def step_click_login(page: Page):
    login_page: LoginPage = LoginPage(page)
    expect(login_page.submit_form_button).to_be_visible()
    login_page.submit_form_button.click()

@allure.step("Check password renewal required")
@screenshot_step
def step_check_password_renewal_required(page: Page):
    password_renewal_page: PasswordRenewalPage = PasswordRenewalPage(page)

    expect(password_renewal_page.password_renewal_form).to_be_visible()
    expect(password_renewal_page.renew_password_field).to_be_visible()
    expect(password_renewal_page.new_password_field).to_be_visible()
    expect(password_renewal_page.confirm_password_field).to_be_visible()

@allure.step("Fill password renewal form")
@screenshot_step
def step_fill_password_renewal(page: Page, password: str, new_password: str):
    password_renewal_page: PasswordRenewalPage = PasswordRenewalPage(page)

    expect(password_renewal_page.renew_password_field).to_be_visible()
    password_renewal_page.renew_password_field.clear()
    password_renewal_page.renew_password_field.type(password)
    expect(password_renewal_page.new_password_field).to_be_visible()
    password_renewal_page.new_password_field.clear()
    password_renewal_page.new_password_field.type(new_password)
    expect(password_renewal_page.confirm_password_field).to_be_visible()
    password_renewal_page.confirm_password_field.clear()
    password_renewal_page.confirm_password_field.type(new_password)

@allure.step("Click submit password renewal")
@screenshot_step
def step_click_submit_password_renewal(page: Page):
    password_renewal_page: PasswordRenewalPage = PasswordRenewalPage(page)

    expect(password_renewal_page.confirm_password_button).to_be_visible()
    password_renewal_page.confirm_password_button.click()

@allure.step("Check login successful")
@screenshot_step
def step_check_login_successful(page: Page):
    expect(LoginPage(page).error_box).not_to_be_visible()
    PortalPage(page).validate()

@allure.step("Click login with otp")
@screenshot_step
def step_click_otp_login(page: Page):
    login_2fa_page = Login2FAPage(page)
    expect(login_2fa_page.login_button).to_be_visible()
    login_2fa_page.login_button.click()
    expect(login_2fa_page.login_button).not_to_be_visible()

@allure.step("Click manual mode to see secret key")
@screenshot_step
def step_click_manual_mode(page: Page):
    login_2fa_page = Login2FAPage(page)
    expect(login_2fa_page.otp_manual_button).to_be_visible()
    login_2fa_page.otp_manual_button.click()

@allure.step("Get secret key")
@screenshot_step
def step_get_secret_key(page: Page) -> str:
    login_2fa_page = Login2FAPage(page)
    expect(login_2fa_page.otp_secret_key).to_be_visible()
    return login_2fa_page.get_secret_key_text()

@allure.step("Fill totp")
@screenshot_step
def step_fill_totp(page: Page, otp: str):
    login_2fa_page = Login2FAPage(page)
    expect(login_2fa_page.totp_input).to_be_visible()
    login_2fa_page.totp_input.clear()
    login_2fa_page.totp_input.type(otp)

@allure.step("Fill otp")
@screenshot_step
def step_fill_otp(page: Page, otp: str):
    login_2fa_page = Login2FAPage(page)
    expect(login_2fa_page.otp_input).to_be_visible()
    login_2fa_page.otp_input.clear()
    login_2fa_page.otp_input.type(otp)

@allure.step("Click submit to save otp secret")
@screenshot_step
def step_click_save_totp(page: Page):
    login_2fa_page = Login2FAPage(page)
    expect(login_2fa_page.save_otp_button).to_be_visible()
    login_2fa_page.save_otp_button.scroll_into_view_if_needed()
    login_2fa_page.save_otp_button.click()
    page.wait_for_timeout(500)
    expect(login_2fa_page.save_otp_button).not_to_be_visible()

def step_login_2fa(page: Page, config: Config, user: Config.User):
    key_name: str = f"OTPKey-{user.key}"
    
    if not config.load(key_name):
        step_setup_2fa(page=page, config=config, key_name=key_name)
    else:
        step_2fa_login(page=page, config=config, key_name=key_name)

@allure.step("Generate OTP and login")
@screenshot_step
def step_2fa_login(page: Page, config: Config, key_name: str):
    key: str = config.load(key_name)
    assert key is not None, f"Key {key_name} not found in config"
    step_login_with_otp(page=page, key=key)

@allure.step("Setup 2FA")
@screenshot_step
def step_setup_2fa(page: Page, config: Config, key_name: str):
    step_click_manual_mode(page=page)
    
    key: str = step_get_secret_key(page=page)
    step_save_login_with_otp(page=page, key=key)
    
    allure.attach(key, f"{key_name}.txt", attachment_type=allure.attachment_type.TEXT)
    config.save(key_name, key)

@allure.step("Generate OTP code")
def step_generate_otp_code(key: str) -> str:
    return TOTP(key.replace(" ", "")).now()

@allure.step("Login with OTP")
@retry(trys=5, retry_timeout=20)
@screenshot_step
def step_login_with_otp(page: Page, key: str):
    otp: str = step_generate_otp_code(key=key)
    step_fill_otp(page=page, otp=otp)
    step_click_otp_login(page=page)

@allure.step("Save login with OTP")
@retry(trys=5, retry_timeout=20)
@screenshot_step
def step_save_login_with_otp(page: Page, key: str):
    otp: str = step_generate_otp_code(key=key)
    step_fill_totp(page=page, otp=otp)
    step_click_save_totp(page=page)    

@allure.step("Check login not working with invalid username")
@screenshot_step
def step_check_failed_invalid_username(page: Page):
    login_page: LoginPage = LoginPage(page)

    expect(login_page.error_box).to_be_visible()

@allure.step("Check login not working with invalid password")
@screenshot_step
def step_check_failed_invalid_password(page: Page):
    login_page: LoginPage = LoginPage(page)

    expect(login_page.error_box).to_be_visible()

@allure.step("Click forgot password button")
@screenshot_step
def step_click_forgot_password(page: Page):
    login_page: LoginPage = LoginPage(page)
    login_page.forgot_password_link.click()

    portal_page: PortalPage = PortalPage(page)
    portal_page.validate()
    expect(portal_page.forgot_password_modal).to_be_visible()

@allure.step("Fill username")
@screenshot_step
def step_fill_forgot_password_username(page: Page, username: str):
    forgot_password_modal: ForgotPasswordModal = PortalPage(page).forgot_password_modal

    forgot_password_modal.username_field.fill(username)

@allure.step("Click submit button")
@screenshot_step
def step_forgot_password_click_submit(page: Page):
    forgot_password_modal: ForgotPasswordModal = PortalPage(page).forgot_password_modal
    forgot_password_modal.next_button.click()

@allure.step("Check email method is checked")
@screenshot_step
def step_check_forgot_password_email_method_checked(page: Page):
    forgot_password_modal: ForgotPasswordModal = PortalPage(page).forgot_password_modal
    expect(forgot_password_modal.method_email_radio).to_be_checked()

@allure.step("Fill forgot password form")
@screenshot_step
def step_fill_forgot_password(page: Page, username: str):
    step_fill_forgot_password_username(page=page, username=username)
    step_forgot_password_click_submit(page=page)
    step_check_forgot_password_email_method_checked(page=page)
    step_forgot_password_click_submit(page=page)

@allure.step("Check logout successful")
@screenshot_step
def step_user_logout_successful(page: Page):
    try:
        expect(LoginPage(page).content_wrapper).to_be_visible(timeout=3000)
    except:
        return
    expect(LoginPage(page).content_wrapper).not_to_have_text("internal server error")