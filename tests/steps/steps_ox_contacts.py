# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
from pages.base.base import expect
from pages.ox.page import *
from tests.utils import screenshot_step
import allure

from .steps_ox import *

@allure.step("Open Contacts page")
@screenshot_step
def step_open_contacts_page(page: Page, url_portal: str):
    ContactsPage(page).open_page(url_portal)
    ContactsPage(page).validate()

@allure.step("Check Contacts side panel available")
@screenshot_step
def step_check_contacts_side_panel_available(page: Page):
    contacts_page: ContactsPage = ContactsPage(page)
    sidepanel: Locator = contacts_page.sidepanel
    expect(sidepanel).to_be_visible(timeout=30000)

@allure.step("Check collected addresses available")
@screenshot_step
def step_check_contacts_collected_addresses_available(page: Page):
    contacts_page: ContactsPage = ContactsPage(page)
    expect(contacts_page.sidepanel.collected_addresses).to_be_visible(timeout=30000)

@allure.step("Click collected addresses")
@screenshot_step
def step_click_collected_addresses(page: Page) -> str:
    contacts_page: ContactsPage = ContactsPage(page)
    collected_addresses: Locator = contacts_page.sidepanel.collected_addresses.page_part_locator
    expect(collected_addresses).to_be_visible(timeout=30000)
    collected_addresses.click()
    expect(collected_addresses).to_have_attribute("aria-selected", "true")
    return collected_addresses.get_attribute("data-id")

@allure.step("Click add contact")
@screenshot_step
def step_click_add_contact(page: Page):
    contacts_page: ContactsPage = ContactsPage(page)
    add_contact_button: Locator = contacts_page.sidepanel.add_button
    expect(add_contact_button).to_be_visible(timeout=30000)
    add_contact_button.click()
    edit_contact_modal = contacts_page.edit_contact_modal
    expect(edit_contact_modal).to_be_visible()
    assert edit_contact_modal.get_title() == edit_contact_modal.new_contact_modal_title

@allure.step("Fill first and last name in edit contact modal")
@screenshot_step
def step_fill_first_and_last_name(page: Page, first_name: str, last_name: str):
    contacts_page: ContactsPage = ContactsPage(page)
    edit_contact_modal = contacts_page.edit_contact_modal
    first_name_input = edit_contact_modal.first_name_input
    last_name_input = edit_contact_modal.last_name_input
    expect(first_name_input).to_be_visible()
    expect(last_name_input).to_be_visible()
    first_name_input.clear()
    first_name_input.type(first_name)
    last_name_input.clear()
    last_name_input.type(last_name)

@allure.step("Save contact")
@screenshot_step
def step_save_contact(page: Page):
    contacts_page: ContactsPage = ContactsPage(page)
    edit_contact_modal = contacts_page.edit_contact_modal
    save_button = edit_contact_modal.save_button
    expect(save_button).to_be_visible()
    save_button.click()
    expect(edit_contact_modal).not_to_be_visible()
    page.wait_for_timeout(3000)

@allure.step("Check contact exists")
@screenshot_step
def step_check_contact_exists(page: Page, first_name: str, last_name: str):
    contacts_page: ContactsPage = ContactsPage(page)
    contacts_main_window = contacts_page.contacts_main_window
    expect(contacts_main_window).to_be_visible()
    expect(contacts_main_window.contact_list).to_be_visible()
    contact_list_entry = contacts_main_window.get_contact_list_entry_by_name(first_name, last_name)
    expect(contact_list_entry).to_be_visible()

@allure.step("Check contact does not exist")
@screenshot_step
def step_check_contact_not_exists(page: Page, first_name: str, last_name: str):
    contacts_page: ContactsPage = ContactsPage(page)
    contacts_main_window = contacts_page.contacts_main_window
    expect(contacts_main_window).to_be_visible()
    expect(contacts_main_window.contact_list).to_be_visible()
    contact_list_entry = contacts_main_window.get_contact_list_entry_by_name(first_name, last_name)
    expect(contact_list_entry).not_to_be_visible()

@allure.step("Click contact list item")
@screenshot_step
def step_click_contact_list_item(page: Page, first_name: str, last_name: str):
    contacts_page: ContactsPage = ContactsPage(page)
    contacts_main_window = contacts_page.contacts_main_window
    contact_list_entry = contacts_main_window.get_contact_list_entry_by_name(first_name, last_name)
    contact_list_entry.click()
    expect(contact_list_entry).to_have_attribute("aria-selected", "true")

@allure.step("Check contact name in main window")
@screenshot_step
def step_check_contact_name_in_main_window(page: Page, first_name: str, last_name: str):
    contacts_page: ContactsPage = ContactsPage(page)
    contacts_main_window = contacts_page.contacts_main_window
    _full_name = contacts_main_window.get_detail_view_current_contact_name()
    assert _full_name == (first_name, last_name)

@allure.step("Click edit contact")
@screenshot_step
def step_click_edit_contact(page: Page):
    contacts_page: ContactsPage = ContactsPage(page)
    contacts_main_window = contacts_page.contacts_main_window
    edit_contact_button = contacts_main_window.edit_contact_button
    expect(edit_contact_button).to_be_visible()
    edit_contact_button.click()
    edit_contact_modal = contacts_page.edit_contact_modal
    expect(edit_contact_modal).to_be_visible()
    assert edit_contact_modal.get_title() == edit_contact_modal.edit_contact_modal_title

@allure.step("Check edit contact modal")
@screenshot_step
def step_check_edit_contact_modal(page: Page):
    contacts_page: ContactsPage = ContactsPage(page)
    edit_contact_modal = contacts_page.edit_contact_modal
    expect(edit_contact_modal).to_be_visible()

@allure.step("Fill email in edit contact modal")
@screenshot_step
def step_fill_email_in_edit_contact_modal(page: Page, email_address: str):
    contacts_page: ContactsPage = ContactsPage(page)
    edit_contact_modal = contacts_page.edit_contact_modal
    email_input = edit_contact_modal.email_input
    expect(email_input).to_be_visible()
    email_input.click()
    email_input.type(email_address)

@allure.step("Save edit contact modal")
@screenshot_step
def step_save_edit_contact_modal(page: Page):
    contacts_page: ContactsPage = ContactsPage(page)
    edit_contact_modal = contacts_page.edit_contact_modal
    save_button = edit_contact_modal.save_button
    expect(save_button).to_be_visible()
    save_button.click()
    expect(edit_contact_modal).not_to_be_visible()

@allure.step("Check main window email address")
@screenshot_step
def step_check_main_window_email_address(page: Page, email_address: str):
    contacts_page: ContactsPage = ContactsPage(page)
    contacts_main_window = contacts_page.contacts_main_window
    _current_contact_email_address = contacts_main_window.get_detail_view_current_email()
    assert _current_contact_email_address == email_address

@allure.step("Click delete contact")
@screenshot_step
def step_click_delete_contact(page: Page):
    contacts_page: ContactsPage = ContactsPage(page)
    contacts_main_window = contacts_page.contacts_main_window
    delete_contact_button = contacts_main_window.delete_contact_button
    expect(delete_contact_button).to_be_visible()
    delete_contact_button.click()

@allure.step("Check confirmation modal")
@screenshot_step
def step_check_confirmation_modal(page: Page):
    contacts_page: ContactsPage = ContactsPage(page)
    confirmation_modal = contacts_page.confirmation_modal
    expect(confirmation_modal).to_be_visible()

@allure.step("Click confirm delete contact")
@screenshot_step
def step_click_confirm_delete_contact(page: Page):
    contacts_page: ContactsPage = ContactsPage(page)
    confirmation_modal = contacts_page.confirmation_modal
    delete_button = confirmation_modal.delete_button
    delete_button.click()
    expect(delete_button).not_to_be_visible()

