# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
from pages.base.base import expect
from pages.ox.page import *
from tests.utils import screenshot_step, retry
import allure
import os

from .steps_ox import *

@allure.step("Open Calendar page")
@screenshot_step
def step_open_calendar_page(page: Page, url_portal: str):
    CalendarPage(page).open_page(url_portal)
    CalendarPage(page).validate()

@allure.step("Click new appointment")
@screenshot_step
def step_click_new_appointment(page: Page):
    calendar_page: CalendarPage = CalendarPage(page)
    
    new_button: Locator = calendar_page.current_new_button()
    expect(new_button).to_be_visible()
    new_button.click()
    
    expect(calendar_page.edit_appointment_floating_window).to_be_visible()

@allure.step("Fill title")
@screenshot_step
def step_fill_title(page: Page, title: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    title_field: Locator = calendar_page.edit_appointment_floating_window.title_field
    expect(title_field).to_be_visible()
    title_field.type(title)
    expect(title_field).to_have_value(title)

@allure.step("Set appointment start time")
@screenshot_step
def step_set_start_time(page: Page, hours: int, minutes: int):
    calendar_page: CalendarPage = CalendarPage(page)
    time: str = f"{hours:02d}:{minutes:02d}"
    
    start_time_field: Locator = calendar_page.edit_appointment_floating_window.start_time_field
    expect(start_time_field).to_be_visible()
    start_time_field.clear()
    start_time_field.type(time)
    expect(start_time_field).to_have_value(time)
    start_time_field.press("Enter")

@allure.step("Click attachment from files")
@screenshot_step
def step_click_attachment_from_files(page: Page):
    calendar_page: CalendarPage = CalendarPage(page)
    
    expect(calendar_page.edit_appointment_floating_window.files_attachment_button).to_be_visible()
    calendar_page.edit_appointment_floating_window.files_attachment_button.click()
    
    expect(calendar_page.nextcloud_file_picker_modal).to_be_visible()

@allure.step("Upload attachments")
@screenshot_step
def step_appointment_upload_attachments(page: Page, folder: str, files: list[str]):
    calendar_page: CalendarPage = CalendarPage(page)
    
    appointment_button: Locator = calendar_page.edit_appointment_floating_window.attachment_button
    expect(appointment_button).to_be_visible()
    appointment_input: Locator = calendar_page.edit_appointment_floating_window.attachment_input
    expect(appointment_input).to_be_attached()
    appointment_input.set_input_files([f"{folder}{os.sep}{file}" for file in files])

@allure.step("Check attachments visible")
@screenshot_step
def step_appointment_attachments_visible(page: Page, files: list[str]):
    for file in files:
        step_appointment_attachment_visible(page=page, file=file)
    
def step_appointment_attachment_visible(page: Page, file: str):
    @allure.step(f"Attachment {file} visible")
    @screenshot_step
    def inner():
        calendar_page: CalendarPage = CalendarPage(page)
        attachment: Locator = calendar_page.edit_appointment_floating_window.get_attachment(file=file)
        expect(attachment).to_be_attached()
        attachment.scroll_into_view_if_needed()
        expect(attachment).to_be_visible()
    
    inner()
        
    
@allure.step("Click create appointment")
@screenshot_step
def step_click_create_appointment(page: Page):
    calendar_page: CalendarPage = CalendarPage(page)
    
    expect(calendar_page.edit_appointment_floating_window.save_button).to_be_visible()
    calendar_page.edit_appointment_floating_window.save_button.click()
    
    expect(calendar_page.error_alert).not_to_be_visible()
    
    try:
        calendar_page.edit_appointment_floating_window.ignore_conflicts_button.click(timeout=3000)
    except:
        pass

@allure.step("Find resource")
@retry(trys=5, retry_timeout=10)
@screenshot_step
def step_find_resource(page: Page, resource_name: str):
    step_fill_participant_or_resource(page=page, name=resource_name)
    step_resource_suggestion_visible(page=page, resource_name=resource_name)

@allure.step("Fill participant or resource")
@screenshot_step
def step_fill_participant_or_resource(page: Page, name: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    expect(calendar_page.edit_appointment_floating_window.participants_and_resources_input).to_be_visible()
    calendar_page.edit_appointment_floating_window.participants_and_resources_input.clear()
    calendar_page.edit_appointment_floating_window.participants_and_resources_input.type(name)
    expect(calendar_page.edit_appointment_floating_window.participants_and_resources_input).to_have_value(name)

@allure.step("Resource suggestion visible")
@screenshot_step
def step_resource_suggestion_visible(page: Page, resource_name: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    resource_suggestion: Locator = calendar_page.edit_appointment_floating_window.get_suggested_resource(resource_name=resource_name)
    expect(resource_suggestion).to_be_visible()
    
@allure.step("Select resource from suggestions")
@screenshot_step
def step_select_resource(page: Page, resource_name: str):
    calendar_page: CalendarPage = CalendarPage(page)
    resource_suggestion: Locator = calendar_page.edit_appointment_floating_window.get_suggested_resource(resource_name=resource_name)
    expect(resource_suggestion).to_be_visible()
    resource_suggestion.click()
    
    resource_entry: Locator = calendar_page.edit_appointment_floating_window.get_attendee_by_name(resource_name)
    expect(resource_entry).to_be_visible()

@allure.step("Select participant from suggestions")
@screenshot_step
def step_select_participant(page: Page, email: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    participant_suggestion: Locator = calendar_page.edit_appointment_floating_window.get_suggested_participant(email=email)
    expect(participant_suggestion).to_be_visible()
    participant_suggestion.click()
    
    participant_entry: Locator = calendar_page.edit_appointment_floating_window.get_attendee_by_mail(email=email)
    expect(participant_entry).to_be_visible()

@allure.step("Enable VC")
@screenshot_step
def step_click_enable_vc(page: Page) -> str: # Returns conference link
    calendar_page: CalendarPage = CalendarPage(page)
    
    expect(calendar_page.edit_appointment_floating_window.conference_type_select).to_be_attached()
    calendar_page.edit_appointment_floating_window.conference_type_select.select_option(calendar_page.edit_appointment_floating_window.get_conference_videoconference_type())
    page.wait_for_timeout(10000)
    expect(calendar_page.edit_appointment_floating_window.conference_error).not_to_be_visible()
    expect(calendar_page.edit_appointment_floating_window.conference_link).to_be_visible()
    
    return calendar_page.edit_appointment_floating_window.conference_link.text_content()

@allure.step("Check appointment exists")
@screenshot_step
def step_check_appointment_exists(page: Page, title: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    expect(calendar_page.current_calendar.layout_dropdown_button).to_be_visible()
    calendar_page.current_calendar.layout_dropdown_button.click()
    expect(calendar_page.current_calendar.layout_dropdown_menu.week_view_button).to_be_visible()
    calendar_page.current_calendar.layout_dropdown_menu.week_view_button.click()
    
    appointment: AppointmentEntry = calendar_page.current_calendar.get_appointment(title=title)
    expect(appointment).to_be_visible()

@allure.step("Check appointment has attachment")
@screenshot_step
def step_check_appointment_has_attachment(page: Page, title: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    appointment: AppointmentEntry = calendar_page.current_calendar.get_appointment(title=title)
    expect(appointment).to_be_visible()
    expect(appointment.attachment_flag).to_be_visible()

@allure.step("Check appointment has participant")
@screenshot_step
def step_check_appointment_has_participant(page: Page, title: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    appointment: AppointmentEntry = calendar_page.current_calendar.get_appointment(title=title)
    expect(appointment).to_be_visible()
    expect(appointment.participants_flag).to_be_visible()

@allure.step("Check appointment is accepted")
@screenshot_step
def step_check_appointment_is_accepted(page: Page, title: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    appointment: AppointmentEntry = calendar_page.current_calendar.get_appointment(title=title)
    expect(appointment).to_be_visible()
    appointment.check_appointment_accepted()

@allure.step("Check appointment is declined")
@screenshot_step
def step_check_appointment_is_declined(page: Page, title: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    appointment: AppointmentEntry = calendar_page.current_calendar.get_appointment(title=title)
    expect(appointment).to_be_visible()
    appointment.check_appointment_declined()

@allure.step("Check appointment needs action")
@screenshot_step
def step_check_appointment_needs_action(page: Page, title: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    appointment: AppointmentEntry = calendar_page.current_calendar.get_appointment(title=title)
    expect(appointment).to_be_visible()
    appointment.check_appointment_needs_action()

@allure.step("Check appointment is tentative")
@screenshot_step
def step_check_appointment_is_tentative(page: Page, title: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    appointment: AppointmentEntry = calendar_page.current_calendar.get_appointment(title=title)
    expect(appointment).to_be_visible()
    appointment.check_appointment_tentative()

@allure.step("Open appointment details")
@screenshot_step
def step_open_appointment_details(page: Page, title: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    appointment: AppointmentEntry = calendar_page.current_calendar.get_appointment(title=title)
    expect(appointment).to_be_visible()
    appointment.page_part_locator.click()
    
    expect(calendar_page.appointment_detail_popup).to_be_visible()

@allure.step("Check appointment awaits action by user")
@screenshot_step
def step_check_appointment_awaits_action_by_user(page: Page, email: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    participant: AppointmentDetailParticipantEntry = calendar_page.appointment_detail_popup.get_participant(email=email)
    expect(participant).to_be_visible()
    participant.check_participant_needs_action()

@allure.step("Accept appointment")
@screenshot_step
def step_accept_appointment(page: Page):
    calendar_page: CalendarPage = CalendarPage(page)
    
    expect(calendar_page.appointment_detail_popup.accept_button).to_be_visible()
    calendar_page.appointment_detail_popup.accept_button.click()

@allure.step("Check appointment accepted by user")
@screenshot_step
def step_check_appointment_accepted_by_user(page: Page, email: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    participant: AppointmentDetailParticipantEntry = calendar_page.appointment_detail_popup.get_participant(email=email)
    expect(participant).to_be_visible()
    participant.check_participant_accepted()

@allure.step("Decline appointment")
@screenshot_step
def step_decline_appointment(page: Page):
    calendar_page: CalendarPage = CalendarPage(page)
    
    expect(calendar_page.appointment_detail_popup.decline_button).to_be_visible()
    calendar_page.appointment_detail_popup.decline_button.click()

@allure.step("Check appointment declined by user")
@screenshot_step
def step_check_appointment_declined_by_user(page: Page, email: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    participant: AppointmentDetailParticipantEntry = calendar_page.appointment_detail_popup.get_participant(email=email)
    expect(participant).to_be_visible()
    participant.check_participant_declined()

@allure.step("Tentative appointment")
@screenshot_step
def step_tentative_appointment(page: Page):
    calendar_page: CalendarPage = CalendarPage(page)
    
    expect(calendar_page.appointment_detail_popup.tentative_button).to_be_visible()
    calendar_page.appointment_detail_popup.tentative_button.click()

@allure.step("Check appointment tentative by user")
@screenshot_step
def step_check_appointment_tentative_by_user(page: Page, email: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    participant: AppointmentDetailParticipantEntry = calendar_page.appointment_detail_popup.get_participant(email=email)
    expect(participant).to_be_visible()
    participant.check_participant_tentative()

@allure.step("Close appointment details")
@screenshot_step
def step_close_appointment_details(page: Page):
    calendar_page: CalendarPage = CalendarPage(page)
    
    expect(calendar_page.appointment_detail_popup.close_button).to_be_visible()
    calendar_page.appointment_detail_popup.close_button.click()

@allure.step("Set appointment layout to week")
@screenshot_step
def step_set_appointment_layout_to_week(page: Page):
    calendar_page: CalendarPage = CalendarPage(page)
    
    expect(calendar_page.current_calendar.layout_dropdown_button).to_be_visible()
    calendar_page.current_calendar.layout_dropdown_button.click()
    expect(calendar_page.current_calendar.layout_dropdown_menu.week_view_button).to_be_visible()
    calendar_page.current_calendar.layout_dropdown_menu.week_view_button.click()

@allure.step("Get appointment start timestamp")
@screenshot_step
def step_get_appointment_start_timestamp(page: Page, title: str) -> int:
    calendar_page: CalendarPage = CalendarPage(page)
    
    appointment: AppointmentEntry = calendar_page.current_calendar.get_appointment(title=title)
    expect(appointment).to_be_visible()
    
    return appointment.get_start_timestamp()

@allure.step("Get appointment end timestamp")
@screenshot_step
def step_get_appointment_end_timestamp(page: Page, title: str) -> int:
    calendar_page: CalendarPage = CalendarPage(page)
    
    appointment: AppointmentEntry = calendar_page.current_calendar.get_appointment(title=title)
    expect(appointment).to_be_visible()
    
    return appointment.get_end_timestamp()

@allure.step("Drag and drop appointment one time slot down")
@screenshot_step
def step_drag_drop_appointment(page: Page, title: str):
    calendar_page: CalendarPage = CalendarPage(page)
    
    appointment: AppointmentEntry = calendar_page.current_calendar.get_appointment(title=title)
    expect(appointment).to_be_visible()
    appointment.page_part_locator.scroll_into_view_if_needed()
    
    bounding_box = appointment.page_part_locator.bounding_box()
    x, y, w, h = bounding_box["x"], bounding_box["y"], bounding_box["width"], bounding_box["height"]
    
    page.mouse.move(x + (w/2), y + 10)
    page.mouse.down()
    page.mouse.move(x + (w/2), y + h + 10)
    page.mouse.up()

@allure.step("Check appointment one hour later")
def step_check_appointment_timestamp(start_before: int, start_after: int, duration: int):
    diff_millis: int = start_after - start_before
    diff_seconds: int = diff_millis // 1000
    
    duration_seconds: int = duration // 1000
    
    assert diff_seconds == duration_seconds, f"Expected difference of {duration_seconds} seconds, but got {diff_seconds} seconds."
    
