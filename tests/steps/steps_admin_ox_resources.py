# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
from pages.base.base import expect
from pages.management.page import *
from tests.utils import screenshot_step
import allure

@allure.step("Open OXResources page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    OXResourcesPage(page).open_page(url_portal)
    OXResourcesPage(page).validate()

@allure.step("Click add OX resource button")
@screenshot_step
def step_click_add_object(page: Page):
    ox_resources_page: OXResourcesPage = OXResourcesPage(page)
    
    expect(ox_resources_page.add_button).to_be_visible()
    ox_resources_page.add_button.click()
    
    edit_ox_resource: EditOXResourcePage = EditOXResourcePage(page)
    
    expect(edit_ox_resource.resoruce_email_field).to_be_visible()
    expect(edit_ox_resource.incomplete_div).not_to_be_visible(timeout=30000)

@allure.step("Fill OX resource name")
@screenshot_step
def step_fill_resource_name(page: Page, name: str):
    edit_ox_resource: EditOXResourcePage = EditOXResourcePage(page)
    edit_ox_resource.name_field.type(name)
    expect(edit_ox_resource.name_field).to_have_value(name)
    expect(edit_ox_resource.displayname_field).to_have_value(name)

@allure.step("Fill OX resource email")
@screenshot_step
def step_fill_resource_email(page: Page, email: str):
    edit_ox_resource: EditOXResourcePage = EditOXResourcePage(page)
    edit_ox_resource.resoruce_email_field.type(email)
    expect(edit_ox_resource.resoruce_email_field).to_have_value(email)

@allure.step("Fill OX resource manager")
@screenshot_step
def step_fill_resource_manager(page: Page, manager: str):
    edit_ox_resource: EditOXResourcePage = EditOXResourcePage(page)
    edit_ox_resource.resoruce_manager_input.clear()
    edit_ox_resource.resoruce_manager_input.type(manager)
    expect(edit_ox_resource.resoruce_manager_input).to_have_value(manager)

@allure.step("Click save")
@screenshot_step
def step_click_save(page: Page):
    edit_ox_resource: EditOXResourcePage = EditOXResourcePage(page)
    
    expect(edit_ox_resource.save_button).to_be_visible()
    edit_ox_resource.save_button.click()

@allure.step("Check object appears in list")
@screenshot_step
def step_check_object_created(page: Page, name: str):
    ox_resources_page: OXResourcesPage = OXResourcesPage(page)
    
    ox_resources_page.add_button.wait_for(state="visible")
    
    page.wait_for_timeout(500)
    
    ox_resources_page.search_field.fill(name)
    ox_resources_page.search_field.press("Enter")
    
    expect(ox_resources_page.get_object_table_entry(name)).to_be_visible()

@allure.step("Create OX resource")
@screenshot_step
def step_create_object(page: Page, name: str, resource_email: str, resource_manager: str):
    step_fill_resource_name(page=page, name=name)
    step_fill_resource_manager(page=page, manager=resource_manager)
    step_fill_resource_email(page=page, email=resource_email)
    step_click_save(page=page)
    step_check_object_created(page=page, name=name)
    
    
