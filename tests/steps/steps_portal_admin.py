# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
from pages.base.base import expect
from pages.login.page import *
from pages.portal.page import *
from pages.management.page import *
from tests.utils import retry, screenshot_step
import allure

@allure.step("Open portal page logged in as admin")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    page.goto(url_portal)

    PortalPageAdminLoggedIn(page).validate()
    step_assure_logged_in(page=page)

@allure.step("Assure logged in")
@retry(trys=2, retry_timeout=0)
@screenshot_step
def step_assure_logged_in(page: Page):
    portal_page: PortalPageNotLoggedIn = PortalPageNotLoggedIn(page)
    portal_page.toggle_sidebar()
    expect(portal_page.sidebar).to_be_visible()
    try:
        expect(PortalPageLoggedIn(page).get_logout_button()).to_be_visible(timeout=500)
        portal_page.toggle_sidebar()
        expect(portal_page.sidebar).not_to_be_visible()
        return
    except:
        pass
    portal_page.toggle_sidebar()
    expect(portal_page.get_login_tile()).to_be_visible()
    portal_page.get_login_tile().click()
    expect(portal_page.sidebar).not_to_be_visible()
    expect(portal_page.sidebar_button).to_be_visible()
    portal_page.toggle_sidebar()
    expect(PortalPageLoggedIn(page).get_logout_button()).to_be_visible(timeout=500)
    portal_page.toggle_sidebar()
    expect(portal_page.sidebar).not_to_be_visible()

@allure.step("Check no login button")
@screenshot_step
def step_no_login_button(page: Page):
    portal_page: PortalPageNotLoggedIn = PortalPageNotLoggedIn(page)

    login_button: Locator = portal_page.get_login_tile()
    expect(login_button).not_to_be_visible()

@allure.step("Check all admin tiles")
@screenshot_step
def step_check_all_tiles(page: Page):
    portal_page: PortalPageAdminLoggedIn = PortalPageAdminLoggedIn(page)
    
    expect(portal_page.get_users_tile()).to_be_visible()
    expect(portal_page.get_groups_tile()).to_be_visible()
    expect(portal_page.get_functional_mailboxes_tile()).to_be_visible()
    expect(portal_page.get_resources_tile()).to_be_visible()
    expect(portal_page.get_announcements_tile()).to_be_visible()

@allure.step("Check users tile redirect")
@screenshot_step
def step_users_tile_redirect(page: Page):
    portal_page: PortalPageAdminLoggedIn = PortalPageAdminLoggedIn(page)
    
    user_tile: Locator = portal_page.get_users_tile()
    expect(user_tile).to_be_visible()
    
    with page.expect_popup() as page_manager:
        user_tile.click()
    
    users_page: Page = page_manager.value
    
    UsersPage(users_page).validate()
    
    return users_page

@allure.step("Check functional accounts tile redirect")
@screenshot_step
def step_functional_accounts_tile_redirect(page: Page):
    portal_page: PortalPageAdminLoggedIn = PortalPageAdminLoggedIn(page)
    
    functional_accounts_tile: Locator = portal_page.get_functional_mailboxes_tile()
    expect(functional_accounts_tile).to_be_visible()
    
    with page.expect_popup() as page_manager:
        functional_accounts_tile.click()
    
    functional_accounts_page: Page = page_manager.value
    
    FunctionalAccountsPage(functional_accounts_page).validate()
    
    return functional_accounts_page

@screenshot_step
def step_admin_login_wrong_otp(page: Page, url_portal, admin_username: str, admin_password: str):
    page.goto(url_portal)
    page.locator("[data-test=\"tileLink\"]").click()

    username_field: Locator = page.locator('#username')
    password_field: Locator = page.locator('#password')
    login_button: Locator = page.locator('[type="submit"]')

    username_field.fill(admin_username)
    password_field.fill(admin_password)
    login_button.click()

    section = page.locator(".card-pf")
    otp_field: Locator = section.locator("#otp")
    login_button = section.locator("#kc-login")

    otp_field.fill("wrong otp")
    login_button.click()

    section = page.locator(".alert-error pf-c-alert pf-m-inline pf-m-danger")



@screenshot_step
def step_get_user_valid_username(page: Page, url_portal: str, admin_username: str, admin_password: str):
    page.goto(url_portal)
    page.locator("[data-test=\"tileLink\"]").click()

    username_field: Locator = page.locator('#username')
    password_field: Locator = page.locator('#password')
    login_button: Locator = page.locator('[type="submit"]')

    username_field.fill(admin_username)
    password_field.fill(admin_password)
    login_button.click()

    section= page.locator(".portalCategories")
    user_button= page.locator(".portal-tile__box").locator('[src= "./icons/entries/swp.admin_user.svg"]')
    groups_button=section.locator(".portal-tile--box").locator('[src: "./icons/entries/swp.admin_group.svg"]')
    functionalMailbox_button=section.locator(".portal-tile-box").locator('[src: "./icons/entries/swp.admin_functionalmailbox.svg"]')
    resources_button=section.locator(".portal-tile-box").locator('[src: "./icons/entries/swp.admin_functionalmailbox.svg"]')

    user_button.click()
    benutzer_page:Page=page.wait_for_event("popup")

    section=benutzer_page.locator("#dijit_layout_StackContainer_0")
    search_username_field= section.locator("#umc_widgets_TextBox_0")
    lense_button= section.locator("#umc_widgets_SubmitButton_0")
    filter_button= section.locator("#umc_widgets_Button_11")
    page_main= section.locator("#umc_widgets_ContainerWidget_10")


    search_username_field.fill(admin_username)
    lense_button.click()

    name_list= benutzer_page.locator("#dgrid_0-row-uid=default.admin,cn=users,dc=swp-ldap,dc=internal")


@screenshot_step
def step_get_user_invalid_username(page: Page, url_portal: str, admin_username: str, admin_password: str):
    page.goto(url_portal)
    page.locator("[data-test=\"tileLink\"]").click()

    username_field: Locator = page.locator('#username')
    password_field: Locator = page.locator('#password')
    login_button: Locator = page.locator('[type="submit"]')

    username_field.fill(admin_username)
    password_field.fill(admin_password)
    login_button.click()

    section= page.locator(".portalCategories")
    user_button= page.locator(".portal-tile__box").locator('[src= "./icons/entries/swp.admin_user.svg"]')
    groups_button=section.locator(".portal-tile--box").locator('[src: "./icons/entries/swp.admin_group.svg"]')
    functionalMailbox_button=section.locator(".portal-tile-box").locator('[src: "./icons/entries/swp.admin_functionalmailbox.svg"]')
    resources_button=section.locator(".portal-tile-box").locator('[src: "./icons/entries/swp.admin_functionalmailbox.svg"]')

    user_button.click()
    benutzer_page: Page = page.wait_for_event("popup")

    section=benutzer_page.locator("#dijit_layout_StackContainer_0")
    search_username_field= section.locator("#umc_widgets_TextBox_0")
    lense_button= section.locator("#umc_widgets_SubmitButton_0")
    filter_button= section.locator("#umc_widgets_Button_11")
    page_main= section.locator("#umc_widgets_ContainerWidget_10")


    search_username_field.fill("invalid username")
    lense_button.click()

    error_message= benutzer_page.locator("#umc_widgets_Text_3")



