# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from pages.base.base import expect
from pages.portal.page import *
from pages.portal.self_service import *
from tests.utils import screenshot_step
import allure

@allure.step("Fill new password")
@screenshot_step
def step_fill_new_password(page: Page, password: str, retype_password: str):
    new_password_modal: NewPasswordModal = PortalPage(page).new_password_modal
    
    new_password_modal.new_password_input.type(password)
    new_password_modal.retype_new_password_input.type(retype_password)

@allure.step("Click change password")
@screenshot_step
def step_click_change_password(page: Page):
    new_password_modal: NewPasswordModal = PortalPage(page).new_password_modal
    new_password_modal.change_password_button.click()

@allure.step("Check password changed")
@screenshot_step
def step_check_password_changed(page: Page):
    password_change_successful_modal: PasswordChangeSuccessfulModal = PortalPage(page).password_change_successful_modal

    expect(password_change_successful_modal).to_be_visible()
    expect(password_change_successful_modal.header.filter(has_text=password_change_successful_modal.get_password_changed_response_success())).to_be_visible(timeout=30000)

@allure.step("Check password change failed")
@screenshot_step
def step_check_change_password_failed(page: Page):
    alert_modal: AlertModal = PortalPage(page).alert_modal

    expect(alert_modal).to_be_visible()
    expect(alert_modal.header.filter(has_text=alert_modal.get_password_changed_response_fail())).to_be_visible()