# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator, expect
from tests.utils import screenshot_step
import allure

@allure.step("Open OpenTalk url")
@screenshot_step
def step_open_page(page: Page, opentalk_url: str):
    page.goto(opentalk_url)

@allure.step("Check login page opened")
@screenshot_step
def step_check_login_page(page: Page):
    login_page: Locator = page.locator("div.login-pf-page")
    expect(login_page).to_be_visible(timeout=30*1000)

@allure.step("Check login form visible")
@screenshot_step
def step_check_login_form(page: Page):
    username_input: Locator = page.locator("input#username")
    password_input: Locator = page.locator("input#password")
    submit_button: Locator = page.locator("#kc-login")
    expect(username_input).to_be_visible()
    expect(password_input).to_be_visible()
    expect(submit_button).to_be_visible()

@allure.step("Fill login form")
@screenshot_step
def step_fill_login_form(page: Page, username: str, password: str):
    username_input: Locator = page.locator("input#username")
    password_input: Locator = page.locator("input#password")
    username_input.type(username)
    expect(username_input).to_have_value(username)
    password_input.type(password)
    expect(password_input).to_have_value(password)

@allure.step("Click login")
@screenshot_step
def step_click_login(page: Page):
    submit_button: Locator = page.locator("#kc-login")
    submit_button.click()

@allure.step("Check OpenTalk dashboard opened")
@screenshot_step
def step_check_opentalk_page(page: Page):
    opentalk_root: Locator = page.locator("#root")
    expect(opentalk_root).to_be_visible(timeout=10000)

@allure.step("Click start new button")
@screenshot_step
def step_click_start_new(page: Page):
    start_new_button: Locator = page.locator(".MuiButton-colorPrimary[href=\"/dashboard/meetings/meet-now\"]")
    expect(start_new_button).to_be_visible()
    start_new_button.click()

@allure.step("Click open video room button")
@screenshot_step
def step_click_open_video_room(page: Page) -> Page:
    open_video_room_button: Locator = page.locator(".MuiButton-colorSecondary[target=\"_blank\"][href^=\"/room/\"]")
    expect(open_video_room_button).to_be_visible()
    with page.expect_popup() as popup_manager:
        open_video_room_button.click()
    room_page: Page = popup_manager.value
    return room_page

@allure.step("Click join room button")
@screenshot_step
def step_click_join_room(page: Page):
    join_room_button: Locator = page.locator(".MuiButton-colorPrimary[type=\"submit\"]")
    expect(join_room_button).to_be_visible()
    join_room_button.click()

@allure.step("Check joined room")
@screenshot_step
def step_check_room_joined(page: Page):
    moderation_panel: Locator = page.locator(".MuiTabs-root.MuiTabs-vertical")
    expect(moderation_panel).to_be_visible()

@allure.step("Click plan new")
@screenshot_step
def step_click_plan_new(page: Page):
    main_view: Locator = page.locator("#root > div > div > main")
    plan_new_button: Locator = main_view.locator("[href=\"/dashboard/meetings/create\"]")
    expect(plan_new_button).to_be_visible()
    plan_new_button.click()

@allure.step("Fill title")
@screenshot_step
def step_fill_title(page: Page, title: str):
    main_view: Locator = page.locator("#root > div > div > main")
    title_input: Locator = main_view.locator("[name=\"title\"]")
    expect(title_input).to_be_visible()
    title_input.clear()
    title_input.type(title)

@allure.step("Click save")
@screenshot_step
def step_click_save(page: Page):
    main_view: Locator = page.locator("#root > div > div > main")
    save_button: Locator = main_view.locator("button.MuiButton-colorPrimary")
    expect(save_button).to_be_visible()
    save_button.click()

@allure.step("Click create anyway")
@screenshot_step
def step_click_create_anyway(page: Page):
    create_anyway_button: Locator = page.locator(".MuiDialog-container").locator(".MuiButton-colorPrimary")
    try:
        expect(create_anyway_button).to_be_visible()
        create_anyway_button.click()
    except:
        pass

@allure.step("Fill participant")
@screenshot_step
def step_fill_participant(page: Page, participant_mail: str):
    main_view: Locator = page.locator("#root > div > div > main")
    participant_input: Locator = main_view.locator("[data-testid=\"SelectParticipants\"]").locator("input[type=text]")
    expect(participant_input).to_be_visible()
    participant_input.clear()
    participant_input.type(participant_mail)

@allure.step("Select participant")
@screenshot_step
def step_select_participant(page: Page, participant_mail: str):
    auto_complete_list: Locator = page.locator(".MuiAutocomplete-popper").locator("ul.MuiAutocomplete-listbox")
    auto_complete_item: Locator = auto_complete_list.locator("> li", has=page.locator(".MuiTypography-root", has_text=participant_mail))
    expect(auto_complete_item).to_be_visible()
    auto_complete_item.click()

@allure.step("Click send invitations")
@screenshot_step
def step_click_send_invitations(page: Page):
    main_view: Locator = page.locator("#root > div > div > main")
    invite_button: Locator = main_view.locator("button.MuiButton-colorPrimary")
    expect(invite_button).to_be_visible()
    invite_button.click()

@allure.step("Open meetings tab")
@screenshot_step
def step_open_meetings_tab(page: Page):
    meetings_tab_button = page.locator("li.MuiListItem-root > a[data-testid=\"PrimaryNavItem\"][href=\"/dashboard/meetings\"]")
    expect(meetings_tab_button).to_be_visible()
    meetings_tab_button.click()

@allure.step("Click on Period Combobox")
@screenshot_step
def step_click_on_period_combobox(page: Page):
    time_combobox = page.locator("main > header").locator(".MuiInputBase-root > .MuiSelect-select.MuiInputBase-input").first
    expect(time_combobox).to_be_visible()
    time_combobox.click()

@allure.step("Select period in the future")
@screenshot_step
def step_select_period_in_the_future(page: Page):
    future_select = page.locator("#menu-").locator(".MuiMenuItem-root[data-value=\"future\"]")
    expect(future_select).to_be_visible()
    future_select.click()

@allure.step("Meeting tile visible")
@screenshot_step
def step_check_meeting_tile_visible(page: Page, meeting_title: str):
    current_meeting = page.locator(".MuiAccordionDetails-root > .MuiGrid-root.MuiGrid-container", has=page.locator(f"div > p[title=\"{meeting_title}\"]"))
    expect(current_meeting).to_be_visible()

@allure.step("Meeting tile not visible")
@screenshot_step
def step_check_meeting_tile_not_visible(page: Page, meeting_title: str):
    current_meeting = page.locator(".MuiAccordionDetails-root > .MuiGrid-root.MuiGrid-container", has=page.locator(f"div > p[title=\"{meeting_title}\"]"))
    expect(current_meeting).not_to_be_visible()

@allure.step("Click on More Settings button")
@screenshot_step
def step_click_on_more_settings_button(page: Page, meeting_title: str):
    current_meeting = page.locator(".MuiAccordionDetails-root > .MuiGrid-root.MuiGrid-container", has=page.locator(f"div > p[title=\"{meeting_title}\"]"))
    expect(current_meeting).to_be_visible()
    more_settings_button = current_meeting.locator("div > div > button.MuiButtonBase-root")
    expect(more_settings_button).to_be_visible()
    more_settings_button.click()

@allure.step("Click on edit")
@screenshot_step
def step_click_on_edit(page: Page):
    edit_button = page.locator(".MuiPaper-root > ul.MuiList-root > li.MuiButtonBase-root").first
    expect(edit_button).to_be_visible()
    edit_button.click()

@allure.step("Click on delete")
@screenshot_step
def step_click_delete(page: Page):
    delete_button: Locator = page.locator(".MuiPaper-root > ul.MuiList-root > li.MuiButtonBase-root").last
    expect(delete_button).to_be_visible()
    delete_button.click()

@allure.step("Delete meeting modal shows")
@screenshot_step
def step_check_meeting_delete_window(page: Page):
    delete_window_title: Locator = page.locator(".MuiPaper-root > .MuiDialogActions-root > .MuiButton-colorSecondary")
    expect(delete_window_title).to_be_visible()
    delete_window_title.click()

@allure.step("Click on the profile picture icon")
@screenshot_step
def step_click_profile_picture(page: Page):
    profile_picture_icon: Locator = page.locator("a[href=\"/dashboard/settings/profile\"]")
    expect(profile_picture_icon).to_be_visible()
    profile_picture_icon.click()

@allure.step("Fill new profile name")
@screenshot_step
def step_change_profile_name(page: Page, profile_name: str):
    profile_name_field: Locator = page.locator("input[name=displayName]")
    expect(profile_name_field).to_be_visible()
    profile_name_field.clear()
    profile_name_field.type(profile_name)

@allure.step("Save profile changes")
@screenshot_step
def step_click_save_changes(page: Page):
    save_button: Locator = page.locator("main").locator("form").locator("button.MuiButton-containedPrimary")
    expect(save_button).to_be_visible()
    save_button.click()
    
    
    
    
@allure.step("Open time period select")
@screenshot_step
def step_open_time_period_select(page: Page):
    time_period_select: Locator = page.locator("main > header .MuiInputBase-root > .MuiSelect-select.MuiInputBase-input").last
    expect(time_period_select).to_be_visible(timeout=3000)
    time_period_select.click()

@allure.step("Check if time period select expands")
@screenshot_step
def step_check_time_period_select_expands(page: Page):
    selection_box: Locator = page.locator(".MuiMenuItem-gutters[data-value=\"month\"]").first
    expect(selection_box).to_be_visible()

@allure.step("Select time period 'month'")
@screenshot_step
def step_select_time_period_month(page: Page):
    month_option: Locator = page.locator(".MuiMenuItem-gutters[data-value=\"month\"]").first
    expect(month_option).to_be_visible()
    month_option.click()



@allure.step("Click on 'Help' in the sidebar")
@screenshot_step
def step_click_help_sidebar(page: Page):
    help_button: Locator = page.locator("li.MuiListItem-root > a[data-testid=\"PrimaryNavItem\"][href=\"/dashboard/help\"]")
    expect(help_button).to_be_visible()
    help_button.click()

@allure.step("Check if the 'Documentation' page is opened")
@screenshot_step
def step_check_documentation_page(page: Page):
    documentation_page: Locator = page.locator("#root").locator("nav > div.MuiCollapse-root > div > div > div > ul > li:nth-child(1) > a > div > span")
    expect(documentation_page).to_be_visible()

@allure.step("Click the 'Open' button in the OpenTalk dashboard")
@screenshot_step
def step_click_open_button(page: Page):
    open_button: Locator = page.locator("#root").locator("main > div.MuiPaper-root > table > tbody > tr:nth-child(1) > td:nth-child(2) > button") # Bitte CSS Selector verwenden
    expect(open_button).to_be_visible()
    with page.expect_popup() as popup_manager:
        open_button.click()
    popup_manager.value.close()

@allure.step("Click join button")
@screenshot_step
def step_click_join_meeting_button(page: Page):
    start_new_button: Locator = page.locator(".MuiButton-colorPrimary[href=\"/dashboard/meetings/meet-now\"]")
    join_button: Locator = page.locator(".MuiStack-root", has=start_new_button).locator("> button")
    expect(join_button).to_be_visible()
    join_button.click()

@allure.step("Check if the dialog box appears")
@screenshot_step
def step_check_dialogbox(page: Page):
    conference_input: Locator = page.locator("input[name=\"roomId\"]")
    expect(conference_input).to_be_visible()

@allure.step("Enter the valid conference ID")
@screenshot_step
def step_enter_conference_id(page: Page, conference_id: str):
    conference_input: Locator = page.locator("input[name=\"roomId\"]")
    expect(conference_input).to_be_visible()
    conference_input.clear()
    conference_input.type(conference_id)

@allure.step("Click the 'Join' button")
@screenshot_step
def step_click_join_button(page: Page):
    join_button: Locator = page.locator("body > div.MuiDialog-root.MuiModal-root.css-126xj0f > div.MuiDialog-container.MuiDialog-scrollPaper.css-ekeie0 > form > div.MuiDialogActions-root.MuiDialogActions-spacing.css-1e2oxr1 > button")
    expect(join_button).to_be_visible()
    join_button.click()

@allure.step("Check if the meeting was successfully joined")
@screenshot_step
def step_check_successful_join(page: Page):
    meeting_room: Locator = page.locator("//*[@id=\"root\"]/div/div/div/nav/div/div[2]/div[2]/button") # Bitte CSS Selector verwenden
    expect(meeting_room).to_be_visible()

@allure.step("Click 'Add to favorites'")
@screenshot_step
def step_add_to_favorites(page: Page):
    add_to_favorites_button = page.locator("body > div.MuiPopover-root.MuiModal-root.css-jp7szo > div.MuiPaper-root.MuiPaper-elevation.MuiPaper-rounded.MuiPaper-elevation8.MuiPopover-paper.css-mjn206 > ul > li:nth-child(2)") # Bitte vereinfachen
    expect(add_to_favorites_button).to_be_visible()
    add_to_favorites_button.click()

@allure.step("Click the meeting link from the email")
@screenshot_step
def step_open_meeting_link(page: Page, meeting_link: str):
    page.goto(meeting_link)
    expect(page).to_be_visible()

@allure.step("Click 'Settings' in the sidebar")
@screenshot_step
def step_click_settings(page: Page):
    settings_button = page.locator("li.MuiListItem-root > a[data-testid=\"PrimaryNavItem\"][href=\"/dashboard/settings\"]")
    expect(settings_button).to_be_visible()
    settings_button.click()

@allure.step("Click the 'Language' box")
@screenshot_step
def step_click_language_box(page: Page):
    language_box = page.locator("//*[@id=\"root\"]/div/div/main/div[2]/div/div/form/div/div[1]/label") # Bitte CSS Selector verwenden
    language_box.click()

@allure.step("Check if the language selection box appears")
@screenshot_step
def step_check_language_selection_box(page: Page):
    selection_box = page.locator("//*[@id=\"root\"]/div/div/main/div[2]/div/div/form/div/div[2]") # Bitte CSS Selector verwenden
    expect(selection_box).to_be_visible()
    selection_box.click()

@allure.step("Click on a new language")
@screenshot_step
def step_select_english(page: Page):
    language_option = page.locator(f"[data-value=\"en-US\"]")
    expect(language_option).to_be_visible()
    language_option.click()

@allure.step("Click 'Save changes'")
@screenshot_step
def step_save_language_change(page: Page):
    save_button = page.locator("//*[@id=\"root\"]/div/div/main/div[2]/div/div/form/div/div[3]/button") # Bitte CSS Selector verwenden
    expect(save_button).to_be_visible()
    save_button.click()

@allure.step("Click 'Account management' in the sidebar")
@screenshot_step
def step_click_account_management(page: Page):
    account_management_button = page.locator("#root > div > div > div > nav > div > ul > li:nth-child(5) > a > div > div > div > div > span") 
    expect(account_management_button).to_be_visible()
    account_management_button.click()

@allure.step("Check if the password reset page is displayed")
@screenshot_step
def step_check_reset_password_page(page: Page, email: str):
    expect(page).to_have_url(f"https://businessadmin.opentalk.eu/de/subaccounts/reset-password/{email}")

@allure.step("Enter a new password and confirm the password")
@screenshot_step
def step_fill_new_password(page: Page, new_password: str):
    password_field = page.locator("#reset_password_newPassword_first")
    confirm_password_field = page.locator("#reset_password_newPassword_second")
    expect(password_field).to_be_visible()
    password_field.fill(new_password)
    confirm_password_field.fill(new_password)

@allure.step("Click 'Save' to set the new password")
@screenshot_step
def step_save_new_password(page: Page):
    save_button = page.locator("#submit")
    expect(save_button).to_be_visible()
    save_button.click()

@allure.step("Click 'Legal' in the sidebar")
@screenshot_step
def step_click_legal(page: Page):
    legal_button = page.locator("//*[@id=\"root\"]/div/div/div/nav/div[1]/ul/li[6]/a/div/div/div/div/span") # Bitte CSS Selector verwenden
    expect(legal_button).to_be_visible()
    legal_button.click()

@allure.step("Click 'Impressum'")
@screenshot_step
def step_click_impressum(page: Page):
    impressum_link = page.locator("//*[@id=\"root\"]/div/div/div/nav/div[2]/div/div/div/ul/li[1]/a/div/span") # Bitte CSS Selector verwenden
    expect(impressum_link).to_be_visible()
    impressum_link.click()

@allure.step("Click 'Privacy notice'")
@screenshot_step
def step_click_privacy_notice(page: Page):
    privacy_notice_link = page.locator("//*[@id=\"root\"]/div/div/div/nav/div[2]/div/div/div/ul/li[2]/a/div/span") # Bitte CSS Selector verwenden
    expect(privacy_notice_link).to_be_visible()
    privacy_notice_link.click()

@allure.step("Click 'Logout' in the sidebar")
@screenshot_step
def step_click_logout(page: Page):
    logout_button = page.locator("//*[@id=\"root\"]/div/div/div/nav/div[1]/ul/li[7]/button/div/div/div/div/span") # Bitte CSS Selector verwenden
    expect(logout_button).to_be_visible()
    logout_button.click()

@allure.step("Click 'Forgot password?'")
@screenshot_step
def step_click_forgot_password(page: Page):
    forgot_password_link = page.locator("[id=\"kc-form-login\"]").locator(".form-group.login-pf-settings > div > span > a")
    expect(forgot_password_link).to_be_visible()
    forgot_password_link.click()

@allure.step("Enter the email address and click 'Submit'")
@screenshot_step
def step_enter_email(page: Page, email: str):
    email_input = page.locator("#username")
    expect(email_input).to_be_visible()
    email_input.clear()
    email_input.type(email)

@allure.step("Click 'Submit'")
@screenshot_step
def step_click_submit(page: Page):
    submit_button = page.locator("#kc-form-buttons > input")
    expect(submit_button).to_be_visible()
    submit_button.click()

@allure.step("Click 'Register'")
@screenshot_step
def step_click_register(page: Page):
    register_link = page.locator("[id=\"kc-registration\"] > span > a")
    expect(register_link).to_be_visible()
    register_link.click()

@allure.step("Click the three dots in the camera window")
@screenshot_step
def step_click_three_dots(page: Page):
    three_dots_button = page.locator("#toolbar-more")
    expect(three_dots_button).to_be_visible(timeout=120000)
    three_dots_button.click()

@allure.step("Click 'Invite guest'")
@screenshot_step
def step_click_invite_guest(page: Page):
    invite_button = page.locator("body > div.MuiPopover-root.MuiModal-root.css-e6i3n5 > div.MuiPaper-root.MuiPaper-elevation.MuiPaper-rounded.MuiPaper-elevation8.MuiPopover-paper.css-mjn206 > ul > li:nth-child(1)")
    expect(invite_button).to_be_visible()
    invite_button.click()

@allure.step("Click 'Create'")
@screenshot_step
def step_click_create_button(page: Page):
    create_button = page.locator("body > div.MuiDialog-root.MuiModal-root.css-126xj0f > div.MuiDialog-container.MuiDialog-scrollPaper.css-ekeie0 > div > form > div.MuiDialogActions-root.MuiDialogActions-spacing.css-1e2oxr1 > button")
    expect(create_button).to_be_visible()
    create_button.click()

@allure.step("Click 'Copy link'")
@screenshot_step
def step_copy_link(page: Page):
    copy_link_button = page.locator("body > div.MuiDialog-root.MuiModal-root.css-126xj0f > div.MuiDialog-container.MuiDialog-scrollPaper.css-ekeie0 > div > div > div.MuiDialogActions-root.MuiDialogActions-spacing.css-1e2oxr1 > button")
    expect(copy_link_button).to_be_visible()
    copy_link_button.click()

@allure.step("Click the 'Leave room' button in the camera window")
@screenshot_step
def step_leave_meeting(page: Page):
    leave_button = page.locator("[id=\"toolbar-endcall\"]")
    expect(leave_button).to_be_visible()
    leave_button.click()

@allure.step("Click the arrow icon to return to the home page")
@screenshot_step
def step_click_arrow_to_home(page: Page):
    arrow_icon = page.locator("#root").locator("nav > div > div.MuiGrid-root.MuiGrid-item > button")
    expect(arrow_icon).to_be_visible()
    arrow_icon.click()

@allure.step("Click the icon with the four square tiles at the top center")
@screenshot_step
def step_click_view_options(page: Page):
    view_button = page.locator("#root").locator("header > div > div > div.MuiStack-root > button")
    expect(view_button).to_be_visible()
    view_button.click()

@allure.step("Switch to gallery view")
@screenshot_step
def step_switch_to_gallery_view(page: Page):
    gallery_view_item = page.locator("#view-popover-menu > li:nth-child(1)")
    expect(gallery_view_item).to_be_visible()
    gallery_view_item.click()

@allure.step("Switch to speaker view")
@screenshot_step
def step_switch_to_speaker_view(page: Page):
    speaker_view_item = page.locator("#view-popover-menu > li:nth-child(2)")
    expect(speaker_view_item).to_be_visible()
    speaker_view_item.click()

@allure.step("Switch to grid view")
@screenshot_step
def step_switch_to_grid_view(page: Page):
    grid_view_item = page.locator("#view-popover-menu > li:nth-child(3)")
    expect(grid_view_item).to_be_visible()
    grid_view_item.click()

@allure.step("Click the dropdown to change the meeting frequency")
@screenshot_step
def step_select_meeting_frequency(page: Page):
    frequency_dropdown = page.locator(".MuiInputBase-colorPrimary", has=page.locator("> input[name=\"recurrencePattern\"]"))
    expect(frequency_dropdown).to_be_visible()
    frequency_dropdown.click()

@allure.step("Select the 'daily' option for the recurring frequency")
@screenshot_step
def step_select_daily_option(page: Page):
    daily_option = page.locator("#menu-recurrencePattern").locator(".MuiMenu-list > .MuiMenuItem-root[data-value=\"RRULE:FREQ=DAILY\"]")
    expect(daily_option).to_be_visible()
    daily_option.click()


@allure.step("Click the button with the hand symbol to indicate your intention to speak")
@screenshot_step
def step_raise_hand(page: Page):
   
    raise_hand_button: Locator = page.locator("#toolbar-handraise")
    expect(raise_hand_button).to_be_visible()
    raise_hand_button.click()

@allure.step("Click the button with the hand symbol again to lower your hand and withdraw your request")
@screenshot_step
def step_lower_hand(page: Page):
    lower_hand_button: Locator = page.locator("#toolbar-handraise")
    expect(lower_hand_button).to_be_visible()
    lower_hand_button.click()
    
    

@allure.step("Check if 'Weekly' recurrence option is available")
@screenshot_step
def step_check_weekly_option(page: Page):
    weekly_option = page.locator("#menu-recurrencePattern").locator(".MuiMenu-list > .MuiMenuItem-root[data-value=\"RRULE:FREQ=WEEKLY\"]")
    expect(weekly_option).to_be_visible()
    weekly_option.click()
    
@allure.step("Click the 'Every 2 Weeks' recurrence option")
@screenshot_step
def step_click_every_2_weeks_option(page: Page):
    every_2_weeks_option = page.locator("#menu-recurrencePattern").locator(".MuiMenu-list > .MuiMenuItem-root[data-value=\"RRULE:FREQ=WEEKLY;INTERVAL=2\"]")
    expect(every_2_weeks_option).to_be_visible()
    every_2_weeks_option.click()
    
@allure.step("Click the 'Monthly' recurrence option")
@screenshot_step
def step_click_monthly_option(page: Page):
    monthly_option = page.locator("#menu-recurrencePattern").locator(".MuiMenu-list > .MuiMenuItem-root[data-value=\"RRULE:FREQ=MONTHLY\"]")
    expect(monthly_option).to_be_visible()
    monthly_option.click()
    
    
@allure.step("Click the microphone button to mute/unmute")
@screenshot_step
def step_click_microphone_button(page: Page):
    microphone_button = page.locator("#toolbar-audio")
    expect(microphone_button).to_be_visible()
    microphone_button.click()
    
    
@allure.step("Click the downward arrow to open the video source selection")
@screenshot_step
def step_click_video_source_dropdown(page: Page):
    video_source_dropdown = page.locator("#toolbar-video")
    expect(video_source_dropdown).to_be_visible()
    video_source_dropdown.click()
    
@allure.step("Click the 3-point menu to access moderation functions")
@screenshot_step
def step_click_3_point_menu(page: Page):
    three_point_menu = page.locator("#toolbar-more")
    expect(three_point_menu).to_be_visible()
    three_point_menu.click()


@allure.step("Click the button with the red phone icon to leave the meeting room")
@screenshot_step
def step_click_leave_room(page: Page):
    leave_button = page.locator("#toolbar-endcall") 
    expect(leave_button).to_be_visible()
    leave_button.click()


@allure.step("Check if the user can share a specific application window")
@screenshot_step
def step_share_application_window(page: Page):
    # Click the button to start screen sharing
    screen_share_button = page.locator('#toolbar-share-screen')  
    screen_share_button.click()