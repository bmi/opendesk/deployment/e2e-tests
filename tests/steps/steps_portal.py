# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
from pages.base.base import expect
from pages.element.page import *
from pages.jitsi.page import *
from pages.nextcloud.page import *
from pages.notes.page import NotesListPage
from pages.portal.page import *
from pages.ox.page import *
from pages.openproject.page import *
from pages.xwiki.page import *

from pages.element.page import ElementPage
from pages.nextcloud.page import NextcloudPage
from pages.openproject.page import OpenProjectPage
from pages.ox.page import MailPage, ContactsPage
from pages.portal.page import PortalPageLoggedIn
from pages.xwiki.page import XWikiPage
from tests.config.config import Config
from tests.utils import retry, screenshot_step
import allure
import re

@allure.step("Open Portal page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    PortalPage(page).open_page(url_portal)
    PortalPage(page).validate()
    
    PortalPageLoggedIn(page).validate()
    step_assure_logged_in(page=page)

@allure.step("Reload page")
@screenshot_step
def step_reload_page(page: Page):
    page.reload()
    PortalPage(page).validate()

@allure.step("Assure logged in")
@retry(trys=2, retry_timeout=0)
@screenshot_step
def step_assure_logged_in(page: Page):
    portal_page: PortalPageNotLoggedIn = PortalPageNotLoggedIn(page)
    portal_page.toggle_sidebar()
    expect(portal_page.sidebar).to_be_visible()
    try:
        expect(PortalPageLoggedIn(page).get_logout_button()).to_be_visible(timeout=500)
        portal_page.toggle_sidebar()
        expect(portal_page.sidebar).not_to_be_visible()
        return
    except:
        pass
    portal_page.toggle_sidebar()
    expect(portal_page.get_login_tile()).to_be_visible()
    portal_page.get_login_tile().click()
    expect(portal_page.sidebar).not_to_be_visible()
    expect(portal_page.sidebar_button).to_be_visible()
    portal_page.toggle_sidebar()
    expect(PortalPageLoggedIn(page).get_logout_button()).to_be_visible(timeout=500)
    portal_page.toggle_sidebar()
    expect(portal_page.sidebar).not_to_be_visible()

@allure.step("Check for no login button")
@screenshot_step
def step_no_login_button(page: Page):
    portal_page: PortalPageNotLoggedIn = PortalPageNotLoggedIn(page)

    login_button: Locator = portal_page.get_login_tile()
    expect(login_button).not_to_be_visible()

@allure.step("Check tiles: all visible")
@screenshot_step
def step_check_all_tiles(page: Page, config: Config):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    
    expect(portal_page.create_new_in_files_quick_draft).to_be_visible()
    
    expect(portal_page.get_files_tile()).to_be_visible()
    expect(portal_page.get_email_tile()).to_be_visible()
    expect(portal_page.get_calendar_tile()).to_be_visible()
    expect(portal_page.get_contacts_tile()).to_be_visible()
    expect(portal_page.get_tasks_tile()).to_be_visible()
    expect(portal_page.get_projects_tile()).to_be_visible()
    expect(portal_page.get_knowledge_tile()).to_be_visible()
    expect(portal_page.get_videoconference_tile()).to_be_visible()
    expect(portal_page.get_chat_tile()).to_be_visible()
    
    if config.env_notes_enabled and False: # Remove "and False" when Notes backend allows to block guest users
        expect(portal_page.get_notes_tile()).to_be_visible()
    

@allure.step("Check tiles: none visible")
@screenshot_step
def step_check_no_tiles(page: Page, config: Config):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    expect(portal_page.create_new_in_files_quick_draft).not_to_be_visible()
    
    expect(portal_page.get_email_tile()).not_to_be_visible()
    expect(portal_page.get_calendar_tile()).not_to_be_visible()
    expect(portal_page.get_contacts_tile()).not_to_be_visible()
    expect(portal_page.get_tasks_tile()).not_to_be_visible()
    expect(portal_page.get_files_tile()).not_to_be_visible()
    expect(portal_page.get_projects_tile()).not_to_be_visible()
    expect(portal_page.get_knowledge_tile()).not_to_be_visible()
    expect(portal_page.get_videoconference_tile()).not_to_be_visible()
    
    if config.env_notes_enabled and False: # Remove "and False" when Notes backend allows to block guest users
        expect(portal_page.get_notes_tile()).not_to_be_visible()

def step_tile_redirect_fail(page: Page):
    for context_page in page.context.pages:
        if context_page != page and "://files." in context_page.url:
            context_page.close()
        
@allure.step("Check tile visible")
@screenshot_step
def step_check_tile_visible(page: Page, tile_name: str):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    
    tile: Locator = portal_page.get_tile_by_name(tile_name)
    expect(tile).to_be_visible()

@allure.step("Check tile redirect")
@screenshot_step
def step_tile_redirect(page: Page, tile_name: str):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    
    tile: Locator = portal_page.get_tile_by_name(tile_name)
    expect(tile).to_be_visible()
    
    with page.expect_popup() as page_manager:
        tile.click()
        
    return page_manager.value

@allure.step("Check redirect url")
@screenshot_step
def step_check_redirect_url(page: Page, expected_url: str):
    expect(page).to_have_url(expected_url)

@allure.step("Check files tile redirects to Nextcloud")
@retry(trys=3, retry_timeout=3, on_fail=step_tile_redirect_fail)
@screenshot_step(timeout=0)
def step_files_tile_redirect(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    
    files_tile: Locator = portal_page.get_files_tile()
    
    with page.expect_popup() as page_manager:
        files_tile.click()
    
    nextcloud_page: Page = page_manager.value
    
    NextcloudPage(nextcloud_page).validate()
    
    return nextcloud_page

@allure.step("Check email tile redirects to OX Mail")
@screenshot_step
def step_email_tile_redirect(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    email_tile: Locator = portal_page.get_email_tile()
    expect(email_tile).to_be_visible()

    with page.expect_popup() as page_manager:
        email_tile.click()

    mail_page: Page = page_manager.value

    MailPage(mail_page).validate()

    return mail_page

@allure.step("Check calendar tile redirects to OX Calendar")
@screenshot_step
def step_calendar_tile_redirect(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    calendar_tile: Locator = portal_page.get_calendar_tile()
    expect(calendar_tile).to_be_visible()

    with page.expect_popup() as page_manager:
        calendar_tile.click()

    calendar_page: Page = page_manager.value

    CalendarPage(calendar_page).validate()

    return calendar_page

@allure.step("Check projects tile redirects to OpenProject")
@screenshot_step
def step_projects_tile_redirect(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    
    projects_tile: Locator = portal_page.get_projects_tile()
    expect(projects_tile).to_be_visible()
    
    with page.expect_popup() as page_manager:
        projects_tile.click()
        
    projects_page: Page = page_manager.value
    
    OpenProjectPage(projects_page).validate()
    
    return projects_page

@allure.step("Check wiki tile redirects to XWiki")
@screenshot_step
def step_wiki_tile_redirect(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    
    wiki_tile: Locator = portal_page.get_knowledge_tile()
    expect(wiki_tile).to_be_visible()
    
    with page.expect_popup() as page_manager:
        wiki_tile.click()
        
    wiki_page: Page = page_manager.value
    
    XWikiPage(wiki_page).validate()
    
    return wiki_page

@allure.step("Check chat tile redirects to Element")
@screenshot_step
def step_chat_tile_redirect(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    
    chat_tile: Locator = portal_page.get_chat_tile()
    expect(chat_tile).to_be_visible()
    
    with page.expect_popup() as page_manager:
        chat_tile.click()
        
    chat_page: Page = page_manager.value
    
    BaseElementPage(chat_page).validate()
    
    return chat_page

@allure.step("Check videoconference tile redirects to Jitsi")
@screenshot_step
def step_videoconference_tile_redirect(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    
    conference_tile: Locator = portal_page.get_videoconference_tile()
    expect(conference_tile).to_be_visible()
    
    with page.expect_popup() as page_manager:
        conference_tile.click()
        
    jitsi_page: Page = page_manager.value
    
    JitsiHomePage(jitsi_page).validate()
    
    return jitsi_page

@allure.step("Check no unpriviledged open of OX")
@screenshot_step
def step_check_no_webmail_open(page: Page, url_portal: str):
    OXPage(page).open_page(url_portal)
    
    expect(page).to_have_title("500 - Internal Server Error", timeout=30000)

@allure.step("Check no unpriviledged open of Nextcloud")
@screenshot_step
def step_check_no_files_open(page: Page, url_portal: str):
    NextcloudPage(page).open_page(url_portal)
    
    NextcloudPageNoPermission(page).validate()

@allure.step("Check no unpriviledged open of OpenProject")
@screenshot_step
def step_check_no_projects_open(page: Page, url_portal: str):
    OpenProjectPage(page).open_page(url_portal)
    
    OpenProjectPageNoPermission(page).validate()

@allure.step("Check no unpriviledged open of Wiki")
@screenshot_step
def step_check_no_wiki_open(page: Page, url_portal: str):
    XWikiPage(page).open_page(url_portal)
    
    expect(page).to_have_title(re.compile("^Error 500"), timeout=30000)

@allure.step("Check no unpriviledged open of Element")
@screenshot_step
def step_check_no_chat_open(page: Page, url_portal: str):
    ElementPage(page).open_page(url_portal)
    
    expect(page).to_have_title("Authentication failed", timeout=30000)

@allure.step("Check no unpriviledged open of Notes")
@screenshot_step
def step_check_no_notes_open(page: Page, url_portal: str):
    notes_page: NotesListPage = NotesListPage(page)
    notes_page.open_page(url_portal)
    
    expect(notes_page.note_list).not_to_be_visible()

@allure.step("Click sidebar button")
@screenshot_step
def step_click_sidebar_button(page: Page):
    portal_page: PortalPageNotLoggedIn = PortalPageNotLoggedIn(page)
    
    portal_page.toggle_sidebar()

@allure.step("Check sidebar open")
@screenshot_step
def step_check_sidebar_open(page: Page):
    portal_page: PortalPage = PortalPage(page)
    
    expect(portal_page.sidebar).to_be_visible()
    
@allure.step("Check sidebar closed")
@screenshot_step
def step_check_sidebar_closed(page: Page):
    portal_page: PortalPage = PortalPage(page)
    
    expect(portal_page.sidebar).not_to_be_visible()

@allure.step("Check logout button visible")
@screenshot_step
def step_check_logout_button_visible(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    
    expect(portal_page.get_logout_button()).to_be_visible()

@allure.step("Click user button")
@screenshot_step
def step_click_user_button(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    expect(portal_page.get_user_button()).to_be_visible()
    portal_page.get_user_button().click()

@allure.step("Click user settings button")
@screenshot_step
def step_click_user_settings_button(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    expect(portal_page.get_user_settings_button()).to_be_visible()
    portal_page.get_user_settings_button().click()

@allure.step("System information button not visible")
@screenshot_step
def step_systeminformation_button_not_visible(page: Page):
    portal_page: PortalPageAdminLoggedIn = PortalPageAdminLoggedIn(page)

    expect(portal_page.sidebar.systeminformation_button).not_to_be_visible()

@allure.step("Click systeminformation button")
@screenshot_step
def step_click_systeminformation_button(page: Page):
    portal_page: PortalPageAdminLoggedIn = PortalPageAdminLoggedIn(page)

    expect(portal_page.sidebar.systeminformation_button).to_be_visible()
    portal_page.sidebar.systeminformation_button.click()

@allure.step("Click edit portal button")
@screenshot_step
def step_click_edit_portal_button(page: Page):
    portal_page: PortalPageAdminLoggedIn = PortalPageAdminLoggedIn(page)

    expect(portal_page.sidebar.edit_portal_button).to_be_visible()
    portal_page.sidebar.edit_portal_button.click()

@allure.step("Check release information visible")
@screenshot_step
def step_check_release_information_visible(page: Page):
    portal_page: PortalPageAdminLoggedIn = PortalPageAdminLoggedIn(page)

    expect(portal_page.sidebar.systeminformation_menu).to_be_visible()
    expect(portal_page.sidebar.systeminformation_menu.release_button).to_be_visible()

@allure.step("Check deployed information visible")
@screenshot_step
def step_check_deployment_information_visible(page: Page):
    portal_page: PortalPageAdminLoggedIn = PortalPageAdminLoggedIn(page)

    expect(portal_page.sidebar.systeminformation_menu).to_be_visible()
    expect(portal_page.sidebar.systeminformation_menu.deployed_button).to_be_visible()

@allure.step("Check deployed information not visible")
@screenshot_step
def step_check_deployment_information_not_visible(page: Page):
    portal_page: PortalPageAdminLoggedIn = PortalPageAdminLoggedIn(page)

    expect(portal_page.sidebar.systeminformation_menu).to_be_visible()
    expect(portal_page.sidebar.systeminformation_menu.deployed_button).not_to_be_visible()

@allure.step("Click manage profile button")
@screenshot_step
def step_click_manage_profile_button(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    expect(portal_page.get_manage_profile_button()).to_be_visible()
    portal_page.get_manage_profile_button().click()
    
    expect(portal_page.user_settings_modal).to_be_visible(timeout=30000)

@allure.step("Upload profile picture")
@screenshot_step
def step_upload_profile_picture(page: Page, profile_pic_path: str):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    expect(portal_page.user_settings_modal.upload_button).to_be_visible(timeout=30000)
    expect(portal_page.user_settings_modal.upload_input).to_be_attached()
    portal_page.user_settings_modal.upload_input.set_input_files(profile_pic_path)


@allure.step("Save user settings")
@screenshot_step
def step_save_profile_settings(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    expect(portal_page.user_settings_modal.save_button).to_be_visible()
    portal_page.user_settings_modal.save_button.click()

def step_check_profile_changed_successful(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    expect(portal_page.success_notification.get_profile_changed_notification()).to_be_visible()

@allure.step("Click logout button")
@screenshot_step
def step_click_logout_button(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    expect(portal_page.get_logout_button()).to_be_visible()
    portal_page.get_logout_button().click()
    
@allure.step("Click create new in files button")
@screenshot_step
def step_click_create_new_in_files_button(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    expect(portal_page.create_new_in_files_quick_draft.toggle_button).to_be_visible()
    portal_page.create_new_in_files_quick_draft.toggle_button.click()
    
@allure.step("Click create new document button")
@screenshot_step
def step_click_create_document_button(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    expect(portal_page.create_new_in_files_quick_draft.create_document_button).to_be_visible()
    with page.expect_popup() as page_manager:
        portal_page.create_new_in_files_quick_draft.create_document_button.click()
        
    return page_manager.value
    
@allure.step("Click create new spreadsheet button")
@screenshot_step
def step_click_create_spreadsheet_button(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    expect(portal_page.create_new_in_files_quick_draft.create_spreadsheet_button).to_be_visible()
    with page.expect_popup() as page_manager:
        portal_page.create_new_in_files_quick_draft.create_spreadsheet_button.click()
        
    return page_manager.value
    
@allure.step("Click create new presentation button")
@screenshot_step
def step_click_create_presentation_button(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)

    expect(portal_page.create_new_in_files_quick_draft.create_presentation_button).to_be_visible()
    with page.expect_popup() as page_manager:
        portal_page.create_new_in_files_quick_draft.create_presentation_button.click()
        
    return page_manager.value
    
@allure.step("Check announcement is visible")
@retry(trys=5, retry_timeout=30, on_fail=lambda page, *_, **__: step_reload_page(page))
@screenshot_step
def step_check_announcement_visible(page: Page, title: str):
    portal_page: PortalPage = PortalPage(page)
    
    announcement: Locator = portal_page.find_announcement(title=title)
    
    expect(announcement).to_be_visible()

@allure.step("Check announcement is not visible")
@screenshot_step
def step_check_announcement_not_visible(page: Page, title: str):
    portal_page: PortalPage = PortalPage(page)
    
    announcement: Locator = portal_page.find_announcement(title=title)
    
    expect(announcement).not_to_be_visible()

@allure.step("Click view all")
@screenshot_step
def step_click_newsfeed_view_all(page: Page) -> Page:
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    expect(portal_page.newsfeed.view_all_button).to_be_visible()
    with page.expect_popup() as popup_manager:
        portal_page.newsfeed.view_all_button.click()
    return popup_manager.value
    
@allure.step("Check newsfeed no content message")
@screenshot_step
def step_check_newsfeed_no_content_message(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    expect(portal_page.newsfeed.no_content_message).to_be_visible()

@allure.step("Check newsfeed could not load message")
@screenshot_step
def step_check_newsfeed_could_not_load_message(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    expect(portal_page.newsfeed.could_not_load_message).to_be_visible()


@allure.step("Check contacts tile redirects to ContactsPage")
@screenshot_step
def step_contacts_tile_redirect(page: Page):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    
    contacts_tile: Locator = portal_page.get_contacts_tile()
    expect(contacts_tile).to_be_visible()
    
    with page.expect_popup() as page_manager:
        contacts_tile.click()
        
    contacts_page: Page = page_manager.value
    
    ContactsPage(contacts_page).validate()
    
    return contacts_page

@allure.step("Check newsfeed blog post visible")
@screenshot_step
def step_newsfeed_blog_visible(page: Page, title: str, content: str):
    portal_page: PortalPageLoggedIn = PortalPageLoggedIn(page)
    
    expect(portal_page.newsfeed.get_newsfeed_entry(title=title, content=content)).to_be_visible()

@allure.step("Check edit mode enabled")
@screenshot_step
def step_edit_mode_enabled(page: Page):
    EditPortalPage(page).validate()

@allure.step("Click new application tile")
@screenshot_step
def step_click_new_application_tile(page: Page):
    edit_porta_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_porta_page.last_portal_category_add_tile_tile).to_be_attached()
    edit_porta_page.last_portal_category_add_tile_tile.scroll_into_view_if_needed()
    expect(edit_porta_page.last_portal_category_add_tile_tile).to_be_visible()
    edit_porta_page.last_portal_category_add_tile_tile.click()

@allure.step("Check add entry modal visible")
@screenshot_step
def step_check_add_entry_modal_visible(page: Page):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.add_entry_modal).to_be_visible()

@allure.step("Click create new entry button")
@screenshot_step
def step_click_create_new_entry_button(page: Page):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.add_entry_modal.create_entry_button).to_be_visible()
    edit_portal_page.add_entry_modal.create_entry_button.click()

@allure.step("Check create new entry modal visible")
@screenshot_step
def step_check_create_new_entry_modal_visible(page: Page):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.create_new_entry_modal).to_be_visible()

@allure.step("Fill new entry internal name")
@screenshot_step
def step_fill_new_entry_internal_name(page: Page, internal_name: str):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.create_new_entry_modal.internal_name_input).to_be_visible()
    edit_portal_page.create_new_entry_modal.internal_name_input.clear()
    edit_portal_page.create_new_entry_modal.internal_name_input.type(internal_name)
    expect(edit_portal_page.create_new_entry_modal.internal_name_input).to_have_value(internal_name)

@allure.step("Fill new entry name")
@screenshot_step
def step_fill_new_entry_name(page: Page, name: str):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.create_new_entry_modal.name_input).to_be_visible()
    edit_portal_page.create_new_entry_modal.name_input.clear()
    edit_portal_page.create_new_entry_modal.name_input.type(name)
    expect(edit_portal_page.create_new_entry_modal.name_input).to_have_value(name)

@allure.step("Fill new entry description")
@screenshot_step
def step_fill_new_entry_description(page: Page, description: str):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.create_new_entry_modal.description_input).to_be_visible()
    edit_portal_page.create_new_entry_modal.description_input.clear()
    edit_portal_page.create_new_entry_modal.description_input.type(description)
    expect(edit_portal_page.create_new_entry_modal.description_input).to_have_value(description)

@allure.step("Click name locale options")
@screenshot_step
def step_click_name_locale_button(page: Page):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.create_new_entry_modal.name_locale_button).to_be_visible()
    edit_portal_page.create_new_entry_modal.name_locale_button.click()
    expect(edit_portal_page.translation_modal).to_be_visible()

@allure.step("Click description locale options")
@screenshot_step
def step_click_description_locale_button(page: Page):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.create_new_entry_modal.description_locale_button).to_be_visible()
    edit_portal_page.create_new_entry_modal.description_locale_button.click()
    expect(edit_portal_page.translation_modal).to_be_visible()
    
@allure.step("Click link locale options")
@screenshot_step
def step_click_link_locale_button(page: Page):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.create_new_entry_modal.links_locale_button).to_be_visible()
    edit_portal_page.create_new_entry_modal.links_locale_button.click()
    expect(edit_portal_page.translation_modal).to_be_visible()

@allure.step("Fill German locale value")
@screenshot_step
def step_fill_german_locale_value(page: Page, value: str):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.translation_modal.input_de_de).to_be_visible()
    edit_portal_page.translation_modal.input_de_de.clear()
    edit_portal_page.translation_modal.input_de_de.type(value)
    expect(edit_portal_page.translation_modal.input_de_de).to_have_value(value)

@allure.step("Fill English locale value")
@screenshot_step
def step_fill_english_locale_value(page: Page, value: str):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.translation_modal.input_en_us).to_be_visible()
    edit_portal_page.translation_modal.input_en_us.clear()
    edit_portal_page.translation_modal.input_en_us.type(value)
    expect(edit_portal_page.translation_modal.input_en_us).to_have_value(value)

@allure.step("Save translation")
@screenshot_step
def step_click_save_translation(page: Page):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.translation_modal.save_button).to_be_visible()
    edit_portal_page.translation_modal.save_button.click()
    expect(edit_portal_page.translation_modal).not_to_be_visible()

@allure.step("Select link target new window")
@screenshot_step
def step_select_link_target_new_window(page: Page):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.create_new_entry_modal.link_target_select).to_be_visible()
    expect(edit_portal_page.create_new_entry_modal.link_target_option_new_window).to_be_attached()
    new_window_option: str = edit_portal_page.create_new_entry_modal.link_target_option_new_window.get_attribute("value")
    edit_portal_page.create_new_entry_modal.link_target_select.select_option(new_window_option)
    expect(edit_portal_page.create_new_entry_modal.link_target_select).to_have_value(new_window_option)

@allure.step("Upload new entry icon")
@screenshot_step
def step_upload_new_entry_icon(page: Page, icon_path: str):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.create_new_entry_modal.icon_upload).to_be_attached()
    edit_portal_page.create_new_entry_modal.icon_upload.set_input_files(icon_path)

@allure.step("Click save new entry")
@screenshot_step
def step_click_save_new_entry(page: Page):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.create_new_entry_modal.save_button).to_be_visible()
    edit_portal_page.create_new_entry_modal.save_button.click()
    expect(edit_portal_page.create_new_entry_modal).not_to_be_visible(timeout=30000)

@allure.step("Close edit mode")
@screenshot_step
def step_click_close_edit_mode(page: Page):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    expect(edit_portal_page.header.close_edit_mode_button).to_be_visible()
    edit_portal_page.header.close_edit_mode_button.click()

@allure.step("Check new tile visible")
@screenshot_step
def step_check_new_tile_visible(page: Page, tile_name: str):
    edit_portal_page: EditPortalPage = EditPortalPage(page)
    
    tile: Locator = edit_portal_page.get_tile_by_name(tile_name)
    expect(tile).to_be_attached()
    tile.scroll_into_view_if_needed()
    expect(tile).to_be_visible()

@allure.step("Check tile icon")
@screenshot_step
def step_check_tile_icon_visible(page: Page, tile_name: str):
    portal_page: PortalPage = PortalPage(page)
    tile_button: Locator = portal_page.get_tile_by_name(tile_name)
    
    image: Locator = tile_button.locator("img.portal-tile__img")
    image_src: str | None = image.get_attribute("src")
    assert image_src is not None, "Image source is None"
    image_error: str | None = image.get_attribute("onerror")
    if image_error is None: return
    error_src: str = image_error[len("this.src='"):-1]
    assert image_src != error_src, "Tile has not icon."