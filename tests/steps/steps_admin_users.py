# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import base64
from playwright.sync_api import Page
import requests
from pages.base.base import expect
from pages.management.page import *
from pages.management.modal import *
from tests.config.config import Config
from tests.config.utils import UdmApi, UserImporter
from tests.utils import screenshot_step
import allure

@allure.step("Open Users page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    UsersPage(page).open_page(url_portal)
    UsersPage(page).validate()

@allure.step("Click add user button")
@screenshot_step
def step_users_click_add_user(page: Page):
    users_page: UsersPage = UsersPage(page)
    
    expect(users_page.add_button).to_be_visible(timeout=15000)
    users_page.add_button.click()
    expect(users_page.new_user_modal).to_be_visible(timeout=20000)
    expect(users_page.new_user_modal.container_node).to_be_visible()

@allure.step("Fill user data")
@screenshot_step
def step_fill_user_data(page: Page, firstname: str, lastname: str, username: str, base_domain: str):
    users_page: UsersPage = UsersPage(page)
    new_user_modal: NewUserModal = users_page.new_user_modal
    new_user_modal.firstname_field.type(firstname)
    new_user_modal.lastname_field.type(lastname)
    new_user_modal.username_field.type(username)
    
    expect(new_user_modal.email).to_have_value(f'{username}@{base_domain}')
        
@allure.step("Fill email for invitation mail")
@screenshot_step
def step_fill_invitation_email(page: Page, user_email: str):
    users_page: UsersPage = UsersPage(page)
    new_user_modal: NewUserModal = users_page.new_user_modal
    new_user_modal.mail_field.type(user_email)
    expect(new_user_modal.mail_field).to_have_value(user_email)

@allure.step("Check success notification")
@screenshot_step
def step_check_success_notification(page: Page):
    users_page: UsersPage = UsersPage(page)
    new_user_modal: NewUserModal = users_page.new_user_modal
    expect(new_user_modal.success_notification_text).to_be_visible(timeout=30000)
    
@allure.step("Click next")
@screenshot_step
def step_click_next(page: Page):
    users_page: UsersPage = UsersPage(page)
    new_user_modal: NewUserModal = users_page.new_user_modal
    new_user_modal.primary_button.click()

@allure.step("Click more options")
@screenshot_step
def step_click_more_options(page: Page):
    users_page: UsersPage = UsersPage(page)
    new_user_modal: NewUserModal = users_page.new_user_modal
    new_user_modal.more_button.click()
    
@allure.step("Fill password fields")
@screenshot_step
def step_fill_password_fields(page: Page, password: str):
    edit_user_page: EditUserPage = EditUserPage(page)
    for password_field in edit_user_page.new_password_fields.all():
        password_field.clear()
        password_field.type(password)
    
@allure.step("Disable change password")
@screenshot_step
def step_disable_change_password(page: Page):
    edit_user_page: EditUserPage = EditUserPage(page)
    edit_user_page.get_edit_user_account_tab().click()
    
    if edit_user_page.change_password_on_next_login_checkbox.is_checked():
        edit_user_page.change_password_on_next_login_checkbox.click()
    
@allure.step("Remove all permissions")
@screenshot_step
def step_remove_all_permission(page: Page):
    edit_user_page: EditUserPage = EditUserPage(page)
    edit_user_page.get_edit_user_opendesk_tab().click()
    
    for checkboxes in edit_user_page.get_all_checkboxes().all():
        checkboxes.set_checked(False)
    
@allure.step("Grant all permissions")
@screenshot_step
def step_grant_all_permission(page: Page):
    edit_user_page: EditUserPage = EditUserPage(page)
    edit_user_page.get_edit_user_opendesk_tab().click()
    
    for checkboxes in edit_user_page.get_all_checkboxes().all():
        checkboxes.set_checked(True)
    
@allure.step("Click create")
@screenshot_step
def step_users_click_create(page: Page):
    edit_user_page: EditUserPage = EditUserPage(page)
    edit_user_page.save_button.click()
    
    users_page: UsersPage = UsersPage(page)
    users_page.add_button.wait_for(state="visible")
    
@allure.step("Check if new guest user exists")
@screenshot_step
def step_users_check_user_exists(page: Page, username: str):
    users_page: UsersPage = UsersPage(page)
    users_page.search_field.fill(username)
    users_page.search_field.press("Enter")
    expect(users_page.search_field).to_have_value(username)
    page.wait_for_timeout(100)
    page.keyboard.press("Enter")
    expect(users_page.get_object_table_entry(username)).to_be_visible()


@allure.step("Create new user")
@screenshot_step
def step_users_create_user(page: Page, firstname: str, lastname: str, username: str, user_email: str, base_domain: str):
    step_click_next(page=page)
    step_fill_user_data(page=page, firstname=firstname, lastname=lastname, username=username, base_domain=base_domain)
    step_click_next(page=page)
    step_fill_invitation_email(page=page, user_email=user_email)
    step_click_next(page=page)
    step_check_success_notification(page=page)
    
@allure.step("Create new guest user")
@screenshot_step
def step_users_create_guest_user(page: Page, firstname: str, lastname: str, username: str, password: str, base_domain: str):
    step_click_next(page=page)
    step_fill_user_data(page=page, firstname=firstname, lastname=lastname, username=username, base_domain=base_domain)
    step_click_more_options(page=page)
    step_fill_password_fields(page=page, password=password)
    step_disable_change_password(page=page)
    step_remove_all_permission(page=page)
    step_users_click_create(page=page)
    step_users_check_user_exists(page=page, username=username)
    
@allure.step("Create new component admin user")
@screenshot_step
def step_users_create_component_admin_user(page: Page, firstname: str, lastname: str, username: str, password: str, base_domain: str):
    step_click_next(page=page)
    step_fill_user_data(page=page, firstname=firstname, lastname=lastname, username=username, base_domain=base_domain)
    step_click_more_options(page=page)
    step_fill_password_fields(page=page, password=password)
    step_disable_change_password(page=page)
    step_grant_all_permission(page=page)
    step_users_click_create(page=page)
    step_users_check_user_exists(page=page, username=username)

@allure.step("Import user")
@screenshot_step
def step_import_user(user: Config.User, user_importer: UserImporter):
    return user_importer.import_user(user)

@allure.step("Set user require password change on next login")
@screenshot_step
def step_require_password_change(username: str, udm_api: UdmApi):
    udm_api.enable_password_change_on_next_login(username)

@allure.step("Delete user")
@screenshot_step
def step_delete_user(username: str, udm_api: UdmApi):
    udm_api.delete_user(username)