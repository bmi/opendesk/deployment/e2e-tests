# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
from pages.base.base import expect
from pages.login.page import *
from pages.portal.page import *
from tests.utils import screenshot_step
import allure

@allure.step("Open portal page not logged in")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    page.goto(url_portal)
    
    PortalPageNotLoggedIn(page).validate()

@allure.step("Open portal page not logged in")
@screenshot_step
def step_open_page_no_validate(page: Page, url_portal: str):
    page.goto(url_portal)

@allure.step("Check login tile visible")
@screenshot_step
def step_login_tile_visible(page: Page):
    portal_page: PortalPageNotLoggedIn = PortalPageNotLoggedIn(page)
    
    login_button: Locator = portal_page.get_login_tile()
    expect(login_button).to_be_visible()
    
@allure.step("Check login button icon")
@screenshot_step
def step_login_icon(page: Page):
    portal_page: PortalPageNotLoggedIn = PortalPageNotLoggedIn(page)
    login_button: Locator = portal_page.get_login_tile()
    
    image: Locator = login_button.locator("img.portal-tile__img")
    image_src: str | None = image.get_attribute("src")
    assert image_src != None
    image_error: str | None = image.get_attribute("onerror")
    assert image_error != None
    error_src: str = image_error[len("this.src='"):-1]
    assert image_src != error_src

@allure.step("Check login button redirecting to login page")
@screenshot_step
def step_login_redirect(page: Page):
    portal_page: PortalPageNotLoggedIn = PortalPageNotLoggedIn(page)
    login_button: Locator = portal_page.get_login_tile()
    
    login_button.click()
    
    LoginPage(page).validate()
    
@allure.step("Check sidebar open")
@screenshot_step
def step_check_sidebar_open(page: Page):
    portal_page: PortalPage = PortalPage(page)
    
    expect(portal_page.sidebar).to_be_visible()
    
@allure.step("Check sidebar closed")
@screenshot_step
def step_check_sidebar_closed(page: Page):
    portal_page: PortalPage = PortalPage(page)
    
    expect(portal_page.sidebar).not_to_be_visible()

@allure.step("Click sidebar button")
@screenshot_step
def step_click_sidebar_button(page: Page):
    portal_page: PortalPageNotLoggedIn = PortalPageNotLoggedIn(page)
    
    portal_page.sidebar_button.click()

@allure.step("Check login button visible")
@screenshot_step
def step_check_login_button_visible(page: Page):
    portal_page: PortalPageNotLoggedIn = PortalPageNotLoggedIn(page)
    
    expect(portal_page.get_login_button()).to_be_visible()