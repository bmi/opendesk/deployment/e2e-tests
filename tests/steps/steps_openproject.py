# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator, Dialog
import pytest
from pages.base.base import allow_fail, expect
from pages.openproject.page import *
from tests.utils import screenshot_step
import allure
import os

@allure.step("Open Projects page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    OpenProjectPage(page).open_page(url_portal)
    OpenProjectPage(page).validate()

@allure.step("Open OpenProject admin page")
@screenshot_step
def step_open_admin_page(page: Page, url_portal: str):
    AdminPage(page).open_page(url_portal)
    AdminPage(page).validate()

@allure.step("Expand admin files settings")
@screenshot_step
def step_expand_admin_files_settings(page: Page):
    admin_page: AdminPage = AdminPage(page)
    
    expect(admin_page.sidebar.files_toggler).to_be_visible()
    admin_page.sidebar.files_toggler.click()
    expect(admin_page.sidebar.external_file_storages_button).to_be_visible()

@allure.step("Open external file storages")
@screenshot_step
def step_open_external_file_storages(page: Page):
    admin_page: AdminPage = AdminPage(page)
    
    expect(admin_page.sidebar.external_file_storages_button).to_be_visible()
    admin_page.sidebar.external_file_storages_button.click()
    
    ExternalFileStoragePage(page).validate()

@allure.step("Open Nextcloud storage")
@screenshot_step
def step_open_nextcloud_storage(page: Page):
    external_file_storage_page: ExternalFileStoragePage = ExternalFileStoragePage(page)
    
    expect(external_file_storage_page.nextcloud_storage_entry).to_be_visible()
    expect(external_file_storage_page.nextcloud_storage_name).to_be_visible()
    external_file_storage_page.nextcloud_storage_name.click()

@allure.step("Check storage health")
@screenshot_step
def step_check_storage_health(page: Page):
    step_check_general_info_section_health(page)
    step_check_oauth_section_health(page)
    step_check_oauth_client_section_health(page)
    step_check_automatically_managed_project_folders_section_health(page)
    step_check_health_status_component_health(page)

@allure.step("Expand admin authentication settings")
@screenshot_step
def step_expand_admin_authentication_settings(page: Page):
    admin_page: AdminPage = AdminPage(page)
    
    expect(admin_page.sidebar.authentication_toggler).to_be_visible()
    admin_page.sidebar.authentication_toggler.click()
    expect(admin_page.sidebar.ldap_authentication_button).to_be_visible()

@allure.step("Open LDAP sources")
@screenshot_step
def step_open_ldap_sources(page: Page):
    admin_page: AdminPage = AdminPage(page)
    
    expect(admin_page.sidebar.ldap_authentication_button).to_be_visible()
    admin_page.sidebar.ldap_authentication_button.click()

@allure.step("Check LDAP visible")
@screenshot_step
def step_check_ldap_visible(page: Page, name: str):
    ldap_sources_page: LdapSourcesPage = LdapSourcesPage(page)
    
    expect(ldap_sources_page.get_entry_by_name(name)).to_be_visible()

@allure.step("Check LDAP health")
@screenshot_step
def step_check_ldap_health(page: Page, name: str):
    ldap_sources_page: LdapSourcesPage = LdapSourcesPage(page)
    
    ldap_entry: LdapSourceEntry = ldap_sources_page.get_entry_by_name(name)
    expect(ldap_entry.test_button).to_be_visible()
    ldap_entry.test_button.click()
    expect(ldap_sources_page.success_flash_message).to_be_visible(timeout=15000)

@allure.step("Check general info section health")
@screenshot_step
def step_check_general_info_section_health(page: Page):
    external_file_storage_entry_page: ExternalFileStorageEntryPage = ExternalFileStorageEntryPage(page)
    expect(external_file_storage_entry_page.general_info_section.filter(has=external_file_storage_entry_page.success_label)).to_be_visible()

@allure.step("Check OAuth section health")
@screenshot_step
def step_check_oauth_section_health(page: Page):
    external_file_storage_entry_page: ExternalFileStorageEntryPage = ExternalFileStorageEntryPage(page)
    expect(external_file_storage_entry_page.oauth_section.filter(has=external_file_storage_entry_page.success_label)).to_be_visible()

@allure.step("Check OAuth client section health")
@screenshot_step
def step_check_oauth_client_section_health(page: Page):
    external_file_storage_entry_page: ExternalFileStorageEntryPage = ExternalFileStorageEntryPage(page)
    expect(external_file_storage_entry_page.oauth_client_section.filter(has=external_file_storage_entry_page.success_label)).to_be_visible()

@allure.step("Check automatically managed project folders section health")
@screenshot_step
def step_check_automatically_managed_project_folders_section_health(page: Page):
    external_file_storage_entry_page: ExternalFileStorageEntryPage = ExternalFileStorageEntryPage(page)
    expect(external_file_storage_entry_page.automatically_managed_project_folders_section.filter(has=external_file_storage_entry_page.success_label)).to_be_visible()

@allure.step("Check health status component health")
@screenshot_step
def step_check_health_status_component_health(page: Page):
    external_file_storage_entry_page: ExternalFileStorageEntryPage = ExternalFileStorageEntryPage(page)
    expect(external_file_storage_entry_page.health_status_component.filter(has=external_file_storage_entry_page.success_label)).to_be_visible()

@allure.step("Expand admin mail and notifications settings")
@screenshot_step
def step_expand_admin_mail_and_notifications_settings(page: Page):
    admin_page: AdminPage = AdminPage(page)
    
    expect(admin_page.sidebar.mail_and_notifications_toggler).to_be_visible()
    admin_page.sidebar.mail_and_notifications_toggler.click()
    expect(admin_page.sidebar.mail_notifications_button).to_be_visible()

@allure.step("Open mail notifications")
@screenshot_step
def step_open_mail_notifications(page: Page):
    admin_page: AdminPage = AdminPage(page)
    
    expect(admin_page.sidebar.mail_notifications_button).to_be_visible()
    admin_page.sidebar.mail_notifications_button.click()

@allure.step("Send test mail")
@screenshot_step
def step_send_test_mail(page: Page):
    mail_notifications_page: MailNotificationsPage = MailNotificationsPage(page)
    
    expect(mail_notifications_page.send_test_mail_button).to_be_visible()
    mail_notifications_page.send_test_mail_button.click()
    
    expect(mail_notifications_page.success_flash_message).to_be_visible(timeout=15000)

@allure.step("Checking OpenProject header")
@screenshot_step
def step_check_openproject_header(page: Page):
    openproject_page: OpenProjectPage = OpenProjectPage(page)
    
    expect(openproject_page.header).to_be_visible()
    expect(openproject_page.header.logo).to_be_visible()
    expect(openproject_page.header.project_select_button).to_be_visible()
    
    # TODO: Add more

@allow_fail
@allure.step("Click through welcome modals")
@screenshot_step
def step_click_through_welcome_modals(page: Page):
    openproject_page: OpenProjectPage = OpenProjectPage(page)

    expect(openproject_page.first_login_modal).to_be_visible(timeout=5000)
    openproject_page.first_login_modal.submit_button.click()
    openproject_page.skip_intro_button.click()

@allure.step("Open project select")
@screenshot_step
def step_open_project_select(page: Page):
    openproject_page: OpenProjectPage = OpenProjectPage(page)
    
    expect(openproject_page.header.project_select_button).to_be_visible()
    openproject_page.header.project_select_button.click()
    
    expect(openproject_page.project_list_modal).to_be_visible()

@allure.step("Open demo project")
@screenshot_step
def step_open_demo_project(page: Page):
    openproject_page: OpenProjectPage = OpenProjectPage(page)
    
    expect(openproject_page.project_list_modal).to_be_visible()
    
    demo_project: Locator = openproject_page.project_list_modal.find_project("Demo-Projekt")
    expect(demo_project).to_be_visible()
    demo_project.click()

@allure.step("Open project")
@screenshot_step
def step_open_project(page: Page, project_name: str):
    openproject_page: OpenProjectPage = OpenProjectPage(page)
    
    expect(openproject_page.project_list_modal).to_be_visible()
    
    demo_project: Locator = openproject_page.project_list_modal.find_project(project_name)
    expect(demo_project).to_be_visible()
    demo_project.click()

@allure.step("New project: Click new project")
@screenshot_step
def step_click_new_project(page: Page):
    openproject_page: OpenProjectPage = OpenProjectPage(page)
    
    expect(openproject_page.project_list_modal).to_be_visible()
    expect(openproject_page.project_list_modal.add_button).to_be_visible()
    
    openproject_page.project_list_modal.add_button.click()

@allure.step("New project: Fill project name")
@screenshot_step
def step_new_project_fill_name(page: Page, name: str):
    new_project_page: NewProjectPage = NewProjectPage(page)
    
    expect(new_project_page.project_name_input).to_be_visible()
    new_project_page.project_name_input.type(name)
    expect(new_project_page.project_name_input).to_have_value(name)

@allure.step("New project: Unselect parent project")
@screenshot_step
def step_new_project_unselect_parent_project(page: Page):
    new_project_page: NewProjectPage = NewProjectPage(page)
    
    if new_project_page.clear_parent_button.is_visible():
        new_project_page.clear_parent_button.click()

@allure.step("New project: Check no parent selected")
@screenshot_step
def step_new_project_check_no_parent_selected(page: Page):
    new_project_page: NewProjectPage = NewProjectPage(page)
    
    expect(new_project_page.current_parent_project).not_to_be_visible()

@allure.step("New project: Click create")
@screenshot_step
def step_new_project_click_create(page: Page):
    new_project_page: NewProjectPage = NewProjectPage(page)
    
    expect(new_project_page.create_button).to_be_visible()
    new_project_page.create_button.click()

@allure.step("Check project open")
@screenshot_step
def step_check_project_open(page: Page, project_name: str):
    openproject_page: OpenProjectPage = OpenProjectPage(page)
    
    expect(openproject_page.header.project_select_button).to_have_text(project_name)

@allure.step("Open files menu")
@screenshot_step
def step_open_files_menu(page: Page):
    openproject_page: ProjectPage = ProjectPage(page)
    
    expect(openproject_page.sidebar.settings_toggler).to_be_visible()
    openproject_page.sidebar.settings_toggler.click()
    
    expect(openproject_page.sidebar.project_storage_button).to_be_visible()
    openproject_page.sidebar.project_storage_button.click()

@allure.step("Click add storage")
@screenshot_step
def step_click_add_storage(page: Page):
    storage_page: StoragePage = StoragePage(page)
    
    storage_page.new_storage_button.click()
    expect(storage_page.storage_select_nextcloud).to_be_visible()
    
@allure.step("Click submit button")
@screenshot_step
def step_click_submit_button(page: Page):
    storage_page: StoragePage = StoragePage(page)
    
    expect(storage_page.storage_submit_button).to_be_visible()
    storage_page.storage_submit_button.click()
    
@allure.step("Check storage type is automatic")
@screenshot_step
def step_check_automatic_checked(page: Page):
    storage_page: StoragePage = StoragePage(page)
    
    expect(storage_page.storage_mode_automatic_radio).to_be_visible()
    storage_page.storage_mode_automatic_radio.check()
    expect(storage_page.storage_mode_automatic_radio).to_be_checked()

@allure.step("Grant access to Nextcloud")
@screenshot_step
def step_grant_access_to_nextcloud(page: Page):
    e = allow_fail(step_click_request_access)(page)
    if e and isinstance(e, AssertionError):
        return
    step_click_grant_access_login(page)
    step_click_grant_access(page)

@allure.step("Check file storage exists")
@screenshot_step
def step_check_file_storage_exists(page: Page):
    storage_page: StoragePage = StoragePage(page)
    
    expect(storage_page.table_nextcloud_entry).to_be_visible()

@allure.step("Click request to grant access")
@screenshot_step
def step_click_request_access(page: Page):
    storage_page: StoragePage = StoragePage(page)

    expect(storage_page.request_access_button).to_be_visible()
    storage_page.request_access_button.click()
    
@allure.step("Click login for granting access")
@screenshot_step
def step_click_grant_access_login(page: Page):
    grant_access_page: GrantAccessPage = GrantAccessPage(page)
    
    expect(grant_access_page.login_button).to_be_visible()
    grant_access_page.login_button.click()
    
@allure.step("Click grant access")
@screenshot_step
def step_click_grant_access(page: Page):
    grant_access_page: GrantAccessPage = GrantAccessPage(page)
    
    expect(grant_access_page.grant_button).to_be_visible()
    grant_access_page.grant_button.click()
    
    expect(grant_access_page.success_dialog).to_be_visible()

@allure.step("Open work packages")
@screenshot_step
def step_open_work_packages(page: Page):
    openproject_page: ProjectPage = ProjectPage(page)
    
    expect(openproject_page.sidebar.work_packages_button).to_be_visible()
    openproject_page.sidebar.work_packages_button.click()

@allure.step("Click new work package button")
@screenshot_step
def step_click_new_work_package(page: Page):
    work_packages_page: WorkPackagesPage = WorkPackagesPage(page)
    
    expect(work_packages_page.create_button).to_be_visible()
    work_packages_page.create_button.click()

@allure.step("Click work package task")
@screenshot_step
def step_click_work_package_task(page: Page):
    work_packages_page: WorkPackagesPage = WorkPackagesPage(page)
    
    expect(work_packages_page.create_button_dropdown).to_be_visible()
    expect(work_packages_page.create_button_dropdown_item_task).to_be_visible()
    work_packages_page.create_button_dropdown_item_task.click()

@allure.step("Check edit work package view is visible")
@screenshot_step
def step_check_edit_work_package_view(page: Page):
    work_package_page: WorkPackagePage = WorkPackagePage(page)
    
    expect(work_package_page.edit_form).to_be_visible()

@allure.step("Fill work package subject")
@screenshot_step
def step_fill_work_package_subject(page: Page, subject: str):
    work_package_page: WorkPackagePage = WorkPackagePage(page)
    
    expect(work_package_page.edit_form.subject_input).to_be_visible()
    work_package_page.edit_form.subject_input.clear()
    work_package_page.edit_form.subject_input.type(subject)
    expect(work_package_page.edit_form.subject_input).to_have_value(subject)

@allure.step("Fill work package project")
@screenshot_step
def step_fill_work_package_project(page: Page, project_name: str):
    work_package_page: WorkPackagePage = WorkPackagePage(page)
    
    expect(work_package_page.edit_form.project_input).to_be_visible()
    work_package_page.edit_form.project_input.clear()
    work_package_page.edit_form.project_input.fill(project_name[:-5])

@allure.step("Select work package project")
@screenshot_step
def step_select_work_package_project(page: Page, project_name: str):
    work_package_page: WorkPackagePage = WorkPackagePage(page)
    
    project_autocomplete = work_package_page.edit_form.get_project_autocomplete(project_name)
    expect(project_autocomplete).to_be_attached()
    project_autocomplete.scroll_into_view_if_needed()
    expect(project_autocomplete).to_be_visible()
    project_autocomplete.click()

@allure.step("Upload attachments")
@screenshot_step
def step_upload_attachments(page: Page, folder: str, files: list[str]):
    work_package_page: WorkPackagePage = WorkPackagePage(page)
    
    expect(work_package_page.edit_form.attachments_input).to_be_attached()
    work_package_page.edit_form.attachments_input.set_input_files([f"{folder}{os.sep}{file}" for file in files])
    
    for file in files:
        step_check_attachment_uploaded(page=page, file=file)
    
def step_check_attachment_uploaded(page: Page, file: str):
    @allure.step(f"Check {file} uploaded")
    @screenshot_step
    def _step_check_attachment_uploaded(page: Page, file: str):
        work_package_page: WorkPackagePage = WorkPackagePage(page)
        
        expect(work_package_page.edit_form.get_attachment(file)).to_be_visible()
    
    _step_check_attachment_uploaded(page=page, file=file)

@allure.step("Click save")
@screenshot_step
def step_click_save(page: Page):
    work_package_page: WorkPackagePage = WorkPackagePage(page)
    
    global dialog_shows
    dialog_shows = False
    page.on("dialog", fail_on_dialog)
    
    expect(work_package_page.edit_form.save_button).to_be_visible()
    work_package_page.edit_form.save_button.click()
    
    page.wait_for_timeout(3000)
    assert not dialog_shows, "Dialog was shown"

def fail_on_dialog(dialog: Dialog):
    dialog.accept()
    global dialog_shows
    dialog_shows = True

@allure.step("Check work package created")
@screenshot_step
def step_check_work_package_created(page: Page):
    work_package_page: WorkPackagePage = WorkPackagePage(page)
    
    expect(work_package_page.edit_form).not_to_be_visible(timeout=30000)
    

@allure.step("Check work package exists")
@screenshot_step
def step_check_work_package_exists(page: Page, subject: str):
    work_packages_page: WorkPackagesPage = WorkPackagesPage(page)
    
    expect(work_packages_page.get_work_package_by_subject(subject)).to_be_visible()

@allure.step("Click quick add button")
@screenshot_step
def step_click_quick_add(page: Page):
    openproject_page: OpenProjectPage = OpenProjectPage(page)

    expect(openproject_page.header.quick_add_button).to_be_visible()
    openproject_page.header.quick_add_button.click()
    expect(openproject_page.header.quick_add_menu).to_be_visible()

@allure.step("Click quick add task")
@screenshot_step
def step_click_quick_add_task(page: Page):
    openproject_page: OpenProjectPage = OpenProjectPage(page)

    expect(openproject_page.header.quick_add_menu_new_task).to_be_visible()
    openproject_page.header.quick_add_menu_new_task.click()

@allure.step("Close work package")
@screenshot_step
def step_close_work_package(page: Page):
    work_package_page: WorkPackagePage = WorkPackagePage(page)
    
    expect(work_package_page.close_button).to_be_visible()
    work_package_page.close_button.click()