# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_login, steps_portal
import allure
import pytest

@allure.epic("Regression")
@allure.feature("Login")
@allure.story("User", "Login")
@allure.title("Login: Logging in with invalid username (failing)")
@pytest.mark.users_dependency(["user_admin"])
def test_login_page_login_invalid_username(page: Page, url_portal: str, user_admin: Config.User):
    username: str = "WrongUsername"
    password: str = user_admin.password
    
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_check_login_page(page=page)
    steps_login.step_fill_login(page=page, username=username, password=password)
    steps_login.step_click_login(page=page)
    steps_login.step_check_failed_invalid_username(page=page)

@allure.epic("Regression")
@allure.feature("Login")
@allure.story("User", "Login")
@allure.title("Login: Logging in with invalid password (failing)")
@pytest.mark.users_dependency(["user_admin"])
def test_login_page_login_invalid_password(page: Page, url_portal: str, user: Config.User):
    username: str = user.username
    password: str = "WrongPassword!"

    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_check_login_page(page=page)
    steps_login.step_fill_login(page=page, username=username, password=password)
    steps_login.step_click_login(page=page)
    steps_login.step_check_failed_invalid_password(page=page)

@allure.epic("Smoke", "Regression")
@allure.feature("Login")
@allure.story("Admin", "Login", "Profile")
@allure.title("Login: Logging in admin")
@pytest.mark.dependency(name="admin_login")
@pytest.mark.users_dependency(["user_admin"])
def test_admin_login(page: Page, url_portal: str, user_admin: Config.User, config: Config):
    username: str = user_admin.username
    password: str = user_admin.password
    
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_check_login_page(page=page)
    steps_login.step_check_form_fields(page=page)
    steps_login.step_fill_login(page=page, username=username, password=password)
    steps_login.step_click_login(page=page)
    if config.env_admin_2fa:
        steps_login.step_login_2fa(page=page, config=config, user=user_admin)
    steps_login.step_check_login_successful(page=page)
    
@allure.epic("Smoke")
@allure.feature("Login")
@allure.story("User", "Login", "Profile")
@allure.title("Login: Logging in with 2FA account")
@pytest.mark.users_dependency(["user_2fa"])
def test_login_2fa(page: Page, url_portal: str, user_2fa: Config.User, config: Config):
    username: str = user_2fa.username
    password: str = user_2fa.password
    
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_check_login_page(page=page)
    steps_login.step_check_form_fields(page=page)
    steps_login.step_fill_login(page=page, username=username, password=password)
    steps_login.step_click_login(page=page)
    steps_login.step_login_2fa(page=page, config=config, user=user_2fa)
    steps_login.step_check_login_successful(page=page)
    
@allure.epic("Smoke", "Regression")
@allure.feature("Login")
@allure.story("User", "Login")
@allure.title("Login: Logging in user")
@pytest.mark.dependency(name="user_login")
@pytest.mark.users_dependency(["user"])
def test_login(page: Page, url_portal: str, user: Config.User):
    username: str = user.username
    password: str = user.password
    
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_check_login_page(page=page)
    steps_login.step_check_form_fields(page=page)
    steps_login.step_fill_login(page=page, username=username, password=password)
    steps_login.step_click_login(page=page)
    steps_login.step_check_login_successful(page=page)

    
@allure.epic("Smoke", "Regression")
@allure.feature("Login")
@allure.story("User", "Login", "Logout")
@allure.title("Login: User login and logout")
@pytest.mark.dependency(name="user_login_logout")
@pytest.mark.users_dependency(["user"])
def test_login_logout(page: Page, url_portal: str, user: Config.User):
    username: str = user.username
    password: str = user.password
    
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_check_login_page(page=page)
    steps_login.step_check_form_fields(page=page)
    steps_login.step_fill_login(page=page, username=username, password=password)
    steps_login.step_click_login(page=page)
    steps_login.step_check_login_successful(page=page)
    steps_portal.step_click_user_button(page=page)
    steps_portal.step_click_logout_button(page=page)
    steps_login.step_user_logout_successful(page=page)

