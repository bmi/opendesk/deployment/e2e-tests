# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from pages.base.base import allow_fail
from tests.config.config import Config
from tests.config.utils import UdmApi
from tests.steps import steps_admin_users, steps_jitsi, steps_login, steps_nextcloud, steps_portal, steps_portal_not_logged_in
import allure
import pytest
import re
import os

@allure.epic("Regression", "Smoke")
@allure.feature("Portal")
@allure.story("UI", "Profile")
@allure.title("Portal: General availability (Not logged in)")
@pytest.mark.env_setting("env_enforce_login", False)
def test_portal_page_not_logged_in(page: Page, url_portal: str):
    steps_portal_not_logged_in.step_open_page(page=page, url_portal=url_portal)
    steps_portal_not_logged_in.step_login_tile_visible(page=page)
    steps_portal_not_logged_in.step_login_icon(page=page)
    steps_portal_not_logged_in.step_login_redirect(page=page)

@allure.epic("Regression", "Smoke")
@allure.feature("Portal")
@allure.story("UI", "Profile")
@allure.title("Portal: Redirect to login (Not logged in)")
@pytest.mark.env_setting("env_enforce_login", True)
def test_portal_page_not_logged_in_enforce_login(page: Page, url_portal: str):
    steps_portal_not_logged_in.step_open_page_no_validate(page=page, url_portal=url_portal)
    steps_login.step_check_login_page(page=page)
    
@allure.epic("Regression", "Smoke")
@allure.feature("Portal")
@allure.story("User", "UI")
@allure.title("Portal: General availability")
@pytest.mark.user("user")
def test_portal_page(logged_in_page: Page, url_portal: str, config: Config):
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_no_login_button(page=logged_in_page)
    steps_portal.step_check_all_tiles(page=logged_in_page, config=config)

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("Guest", "Security")
@allure.title("Portal: No unpriviledged access to components")
@pytest.mark.dependency(name="check_guest_user")
@pytest.mark.user("guest")
def test_check_none_priviledged_guest_user(logged_in_page: Page, url_portal: str, run_id: str, config: Config):
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_portal.step_no_login_button(page=logged_in_page)
    steps_portal.step_check_no_tiles(page=logged_in_page, config=config)
    steps_portal.step_check_no_webmail_open(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_check_no_files_open(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_check_no_projects_open(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_check_no_wiki_open(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_check_no_chat_open(page=logged_in_page, url_portal=url_portal)
    if config.env_notes_enabled and False: # Remove "and False" when Notes backend allows to block guest users
        steps_portal.step_check_no_notes_open(page=logged_in_page, url_portal=url_portal)
    
    conference_name: str = f"up{run_id}"
    steps_jitsi.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_jitsi.step_fill_conference_name(page=logged_in_page, conference_name=conference_name)
    steps_jitsi.step_click_start_conference_button(page=logged_in_page)
    steps_jitsi.step_click_join_conference(page=logged_in_page)
    steps_jitsi.step_check_wait_for_moderator_dialog(page=logged_in_page)
    

@allure.epic("Regression", "Smoke")
@allure.feature("Portal")
@allure.story("UI")
@allure.title("Portal: Open all components via tiles")
@pytest.mark.user("user")
def test_portal_component_from_every_tile(logged_in_page: Page, url_portal: str):
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    test_functions = [
        steps_portal.step_files_tile_redirect, 
        steps_portal.step_email_tile_redirect, 
        steps_portal.step_contacts_tile_redirect, 
        steps_portal.step_chat_tile_redirect,
        steps_portal.step_wiki_tile_redirect, 
        steps_portal.step_calendar_tile_redirect, 
        steps_portal.step_videoconference_tile_redirect, 
        steps_portal.step_projects_tile_redirect
    ]
    failures: list[Exception] = []
    
    for func in test_functions:
        ret_value: Page | Exception = allow_fail(func)(page=logged_in_page)
        if ret_value and isinstance(ret_value, Page):
            ret_value.close()
        elif ret_value and isinstance(ret_value, Exception):
            failures.append(ret_value)
    
    assert len(failures) == 0, f"Failures: {failures}"
        

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("UI")
@allure.title("Portal: Create document from quick draft")
@pytest.mark.user("user")
def test_portal_create_document_quick_draft(logged_in_page: Page, url_portal: str):
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_click_create_new_in_files_button(page=logged_in_page)
    nextcloud_page: Page = steps_portal.step_click_create_document_button(page=logged_in_page)
    steps_nextcloud.step_check_office_viewer_opened(page=nextcloud_page)
    steps_nextcloud.step_close_document(page=nextcloud_page)
    nextcloud_page.close()

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("UI")
@allure.title("Portal: Create spreadsheet from quick draft")
@pytest.mark.user("user")
def test_portal_create_spreadsheet_quick_draft(logged_in_page: Page, url_portal: str):
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_click_create_new_in_files_button(page=logged_in_page)
    nextcloud_page: Page = steps_portal.step_click_create_spreadsheet_button(page=logged_in_page)
    steps_nextcloud.step_check_office_viewer_opened(page=nextcloud_page)
    steps_nextcloud.step_close_document(page=nextcloud_page)
    nextcloud_page.close()

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("UI")
@allure.title("Portal: Create presentation from quick draft")
@pytest.mark.user("user")
def test_portal_create_presentation_quick_draft(logged_in_page: Page, url_portal: str):
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_click_create_new_in_files_button(page=logged_in_page)
    nextcloud_page: Page = steps_portal.step_click_create_presentation_button(page=logged_in_page)
    steps_nextcloud.step_check_office_viewer_opened(page=nextcloud_page)
    steps_nextcloud.step_close_document(page=nextcloud_page)
    nextcloud_page.close()

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("UI")
@allure.title("Portal: Set profile picture")
@pytest.mark.user("user")
@pytest.mark.files({re.compile(r"profile_\d+\.jpeg"): 1})
def test_portal_set_profile_picture(logged_in_page: Page, url_portal: str, temp_files: tuple[str, list]):
    folder: str = temp_files[0]
    profile_pic: str = temp_files[1][0]
    path : str = f"{folder}{os.sep}{profile_pic}"

    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_click_user_button(page=logged_in_page)
    steps_portal.step_click_user_settings_button(page=logged_in_page)
    steps_portal.step_click_manage_profile_button(page=logged_in_page)
    steps_portal.step_upload_profile_picture(page=logged_in_page, profile_pic_path=path)
    steps_portal.step_save_profile_settings(page=logged_in_page)
    steps_portal.step_check_profile_changed_successful(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("UI", "Profile")
@allure.title("Portal: Deployment information visible")
@pytest.mark.env_setting("env_deployment_timestamp", True)
@pytest.mark.user("user_admin")
def test_deployment_timestamp(logged_in_page: Page, url_portal: str):
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_click_user_button(page=logged_in_page)
    steps_portal.step_click_systeminformation_button(page=logged_in_page)
    steps_portal.step_check_release_information_visible(page=logged_in_page)
    steps_portal.step_check_deployment_information_visible(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("UI", "Profile")
@allure.title("Portal: Deployment information not visible")
@pytest.mark.env_setting("env_deployment_timestamp", False)
@pytest.mark.user("user_admin")
def test_no_deployment_timestamp(logged_in_page: Page, url_portal: str):
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_click_user_button(page=logged_in_page)
    steps_portal.step_click_systeminformation_button(page=logged_in_page)
    steps_portal.step_check_release_information_visible(page=logged_in_page)
    steps_portal.step_check_deployment_information_not_visible(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("UI")
@allure.title("Portal: Newsfeed available")
def test_portal_newsfeed_available(temp_logged_in_page: Page, url_portal: str):
    steps_portal.step_open_page(page=temp_logged_in_page, url_portal=url_portal)
    steps_portal.step_check_newsfeed_could_not_load_message(page=temp_logged_in_page)
    wiki_page: Page = steps_portal.step_click_newsfeed_view_all(page=temp_logged_in_page)
    wiki_page.close()
    steps_portal.step_open_page(page=temp_logged_in_page, url_portal=url_portal)
    steps_portal.step_check_newsfeed_no_content_message(page=temp_logged_in_page)


@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("User")
@allure.title("Portal: Password renewal enforced")
def test_update_user_data(page: Page, url_portal: str, udm_api: UdmApi, temp_user: Config.User):
    password: str = temp_user.password
    new_password: str = password[::-1]
        
    steps_admin_users.step_require_password_change(username=temp_user.username, udm_api=udm_api)
    
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_check_login_page(page=page)
    steps_login.step_fill_login(page=page, username=temp_user.username, password=password)
    steps_login.step_click_login(page=page)
    steps_login.step_check_password_renewal_required(page=page)
    steps_login.step_fill_password_renewal(page=page, password=password, new_password=new_password)
    steps_login.step_click_submit_password_renewal(page=page)
    steps_login.step_check_login_successful(page=page)
    
@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("User")
@allure.title("Portal: Password renewal enforced (2FA)")
@pytest.mark.temp_user(enable_2fa=True)
def test_update_user_data_2fa(page: Page, url_portal: str, udm_api: UdmApi, temp_user: Config.User, config: Config):
    password: str = temp_user.password
    new_password: str = password[::-1]
        
    steps_admin_users.step_require_password_change(username=temp_user.username, udm_api=udm_api)
    
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_check_login_page(page=page)
    steps_login.step_fill_login(page=page, username=temp_user.username, password=password)
    steps_login.step_click_login(page=page)
    steps_login.step_login_2fa(page=page, config=config, user=temp_user)
    steps_login.step_check_password_renewal_required(page=page)
    steps_login.step_fill_password_renewal(page=page, password=password, new_password=new_password)
    steps_login.step_click_submit_password_renewal(page=page)
    steps_login.step_check_login_successful(page=page)
    
@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("User")
@allure.title("Portal: Password renewal enforced (2FA before password reset)")
@pytest.mark.temp_user(enable_2fa=True)
def test_update_user_data_2fa_already_enabled(page: Page, url_portal: str, udm_api: UdmApi, temp_user: Config.User, config: Config):
    password: str = temp_user.password
    new_password: str = password[::-1]
        
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_check_login_page(page=page)
    steps_login.step_fill_login(page=page, username=temp_user.username, password=password)
    steps_login.step_click_login(page=page)
    steps_login.step_login_2fa(page=page, config=config, user=temp_user)
    steps_login.step_check_login_successful(page=page)
    steps_portal.step_click_user_button(page=page)
    steps_portal.step_click_logout_button(page=page)
    steps_login.step_user_logout_successful(page=page)
    
    steps_admin_users.step_require_password_change(username=temp_user.username, udm_api=udm_api)
    
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_check_login_page(page=page)
    steps_login.step_fill_login(page=page, username=temp_user.username, password=password)
    steps_login.step_click_login(page=page)
    steps_login.step_login_2fa(page=page, config=config, user=temp_user)
    steps_login.step_check_password_renewal_required(page=page)
    steps_login.step_fill_password_renewal(page=page, password=password, new_password=new_password)
    steps_login.step_click_submit_password_renewal(page=page)
    steps_login.step_check_login_successful(page=page)
    
@allure.epic("Smoke")
@allure.feature("Portal")
@allure.story("User")
@allure.title("Portal: Create new tile")
@pytest.mark.user("user_admin")
@pytest.mark.dependency(name="create_new_tile", scope="session")
@pytest.mark.xdist_group("new_portal_tile")
@pytest.mark.files({"opendesk-logo.png": 1})
def test_portal_create_new_tile(logged_in_page: Page, url_portal: str, run_id: str, locale: str, temp_files: tuple[str, list[str]], config: Config):
    folder, files = temp_files
    icon_path: str = f"{folder}{os.sep}{files[0]}"
    
    internal_name: str = f"tile_{run_id}"
    name: str = f"tile {run_id}"
    
    base_link: str = "https://opendesk.eu/"
    
    description: str = "This is a new tile"
    german_link: str = f"{base_link}#de-DE"
    english_link: str = f"{base_link}#en-US"
    
    current_link: str = f"{base_link}#{locale}"
    
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_click_user_button(page=logged_in_page)
    steps_portal.step_click_edit_portal_button(page=logged_in_page)
    steps_portal.step_edit_mode_enabled(page=logged_in_page)
    steps_portal.step_click_new_application_tile(page=logged_in_page)
    steps_portal.step_check_add_entry_modal_visible(page=logged_in_page)
    steps_portal.step_click_create_new_entry_button(page=logged_in_page)
    steps_portal.step_check_create_new_entry_modal_visible(page=logged_in_page)
    
    steps_portal.step_fill_new_entry_internal_name(page=logged_in_page, internal_name=internal_name)
    
    steps_portal.step_fill_new_entry_name(page=logged_in_page, name=name)
    
    steps_portal.step_fill_new_entry_description(page=logged_in_page, description=description)
    
    steps_portal.step_click_link_locale_button(page=logged_in_page)
    steps_portal.step_fill_german_locale_value(page=logged_in_page, value=german_link)
    steps_portal.step_fill_english_locale_value(page=logged_in_page, value=english_link)
    steps_portal.step_click_save_translation(page=logged_in_page)
    
    steps_portal.step_select_link_target_new_window(page=logged_in_page)
    
    steps_portal.step_upload_new_entry_icon(page=logged_in_page, icon_path=icon_path)
    
    steps_portal.step_click_save_new_entry(page=logged_in_page)
    
    steps_portal.step_click_close_edit_mode(page=logged_in_page)
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    
    steps_portal.step_check_new_tile_visible(page=logged_in_page, tile_name=name)
    steps_portal.step_check_tile_icon_visible(page=logged_in_page, tile_name=name)
    
    config.save("NewTileName", name)
    config.save("NewTileLink", current_link)