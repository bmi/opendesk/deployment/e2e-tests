# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from time import sleep
from typing import Generator
import pytest
from tests.config.config import Config
from playwright.sync_api import Browser, BrowserContext, Page
import csv
import allure
from tests.config.utils import UdmApi, UserImporter
from tests.steps import steps_login, steps_nextcloud, steps_webdav, steps_admin_users
import random
import utils
from webdav3.client import Client
import os

@pytest.fixture(scope="session")
def url_portal(config: Config) -> str:
    return config.url_portal

@pytest.fixture(scope="session")
def base_domain(config: Config) -> str:
    return config.base_domain

@pytest.fixture(scope="session")
def user(config: Config) -> Config.User:
    return config.get_test_user("user")

@pytest.fixture(scope="session")
def user_admin(config: Config) -> Config.User:
    return config.get_test_user("user_admin")

@pytest.fixture(scope="session")
def user_2fa(config: Config) -> Config.User:
    return config.get_test_user("user_2fa")

@pytest.fixture(scope="session")
def guest(config: Config) -> Config.User:
    return config.get_test_user("guest")

@pytest.fixture(scope="session")
def component_admin(config: Config) -> Config.User:
    return config.get_test_user("component_admin")

@pytest.fixture(scope="session")
def new_user(config: Config) -> Config.User:
    return config.get_test_user("new_user")

@pytest.fixture(scope="session")
def udm_api_username(config: Config) -> str:
    return config.udm_api_username

@pytest.fixture(scope="session")
def udm_api_password(config: Config) -> str:
    return config.udm_api_password

@pytest.fixture(scope="session")
def user_importer(config: Config) -> Config.User:
    return config.user_importer

@pytest.fixture(scope="session")
def udm_api(config: Config) -> str:
    return config.udm_api

@pytest.fixture(scope="session")
def pod_restart_thresholds(config: Config) -> dict[str, int]:
    """Reads pod restart thresholds from file and returns them as dictionary.

    Args:
        config (Config): Current configuration

    Returns:
        dict: Dictionary with pod names as keys and restart thresholds as values
    """
    with open(config.pod_restart_threshold_file, "r", encoding="utf-8") as f:
        reader = csv.DictReader(f, delimiter=";")
        return {row["pod"]: int(row["threshold"]) for row in reader}

@pytest.fixture(scope="session")
def browser_contexts() -> dict[str, BrowserContext]:
    """Creates a dictionary to store browser contexts for users.

    Returns:
        dict: Dictionary with user keys as keys and browser contexts as values
    """
    return {}

@pytest.fixture()
def logged_in_contexts(request: pytest.FixtureRequest, config: Config, browser: Browser, url_portal: str, browser_context_args: dict, browser_contexts: dict[str, BrowserContext]) -> Generator[list[BrowserContext], None, None]:
    """Generates a list of browser contexts for users.

    Args:
        request (pytest.FixtureRequest): Current fixture request
        config (Config): Current configuration
        browser (Browser): Current browser
        url_portal (str): Portal URL
        browser_context_args (dict): Browser context arguments (for e.g. locale)
        browser_contexts (dict[str, BrowserContext]): Dictionary to store browser contexts for users and reuse them

    Raises:
        Exception: When no valid 'users' mark is found or user is not found by key

    Yields:
        list[BrowserContext]: List of browser contexts for users
    """
    
    marker = None
    if not (marker := request.node.get_closest_marker("users")):
        raise Exception("Cannot create contexts. No 'users' mark found.")
    if len(marker.args) != 1:
        raise Exception("Cannot create contexts. 'users' mark does not contain user key.")
    keys = marker.args[0]
    contexts = []
    for key in keys:
        user = config.get_test_user(key)
        if not user:
            raise Exception(f"Cannot create context. User not found by key '{key}'")
        
        context = _get_user_context(browser, user, config, url_portal, browser_context_args, browser_contexts)
        
        contexts.append(context)
    
    yield contexts
    
@pytest.fixture()
def logged_in_context(request: pytest.FixtureRequest, config: Config, browser: Browser, url_portal: str, browser_context_args: dict, browser_contexts: dict[str, BrowserContext]) -> Generator[BrowserContext, None, None]:
    """Generates a browser context for a user.

    Args:
        request (pytest.FixtureRequest): Current fixture request
        config (Config): Current configuration
        browser (Browser): Current browser
        url_portal (str): Portal URL
        browser_context_args (dict): Browser context arguments (for e.g. locale)
        browser_contexts (dict[str, BrowserContext]): Dictionary to store browser contexts for users and reuse them

    Raises:
        Exception: When no valid 'user' mark is found or user is not found by key

    Yields:
        BrowserContext: Browser context for a user
    """
    marker = None
    if not (marker := request.node.get_closest_marker('user')):
        raise Exception("Cannot create context. No 'user' mark found.")
    if len(marker.args) != 1:
        raise Exception("Cannot create context. 'user' mark does not contain user key.")
    key = marker.args[0]
    user: Config.User | None = config.get_test_user(key)
    if not user:
        raise Exception(f"Cannot create context. User not found by key '{key}'")
    
    context = _get_user_context(browser, user, config, url_portal, browser_context_args, browser_contexts)
    
    yield context
    
@pytest.fixture()
def logged_in_page(logged_in_context: BrowserContext) -> Generator[Page, None, None]:
    """Creates a new page in a logged in context.

    Args:
        logged_in_context (BrowserContext): Browser context for a user

    Yields:
        Page: Page in a logged in context
    """
    page: Page = logged_in_context.new_page()
    
    yield page
    
    page.close()

@pytest.fixture()
def logged_in_pages(logged_in_contexts: list[BrowserContext]) -> Generator[list[Page], None, None]:
    """Creates a list of pages in logged in contexts.

    Args:
        logged_in_contexts (list[BrowserContext]): List of browser contexts for users

    Yields:
        list[Page]: List of pages in logged in contexts
    """
    
    pages = []
    for logged_in_context in logged_in_contexts:
        page: Page = logged_in_context.new_page()
        pages.append(page)
    
    yield pages
    
    for page in pages:
        page.close()

@pytest.fixture()
def webdav_client(request: pytest.FixtureRequest, url_portal: str, logged_in_context: BrowserContext, config: Config) -> Generator[Client, None, None]:
    """Creates a WebDAV client for a user.

    Args:
        request (pytest.FixtureRequest): Current fixture request
        url_portal (str): Portal URL
        logged_in_context (BrowserContext): Browser context for a user
        config (Config): Current configuration

    Raises:
        Exception: When no valid 'user' mark is found or user is not found by key

    Yields:
        webdavclient3.client.Client: WebDAV client for a user
    """
    
    marker = None
    if not (marker := request.node.get_closest_marker('user')):
        raise Exception("Cannot create context. No 'user' mark found.")
    if len(marker.args) != 1:
        raise Exception("Cannot create context. 'user' mark does not contain user key.")
    key = marker.args[0]
    user: Config.User | None = config.get_test_user(key)
    if not user:
        raise Exception(f"Cannot create WebDAV client. User not found by key '{key}'")
    
    webdav_client_options: str = f"webdav-client-{user.username}"
    
    options: dict | None = config.load(webdav_client_options)
    
    if not options:
        options = _create_webdav_client_options(logged_in_context=logged_in_context, url_portal=url_portal)
        config.save(webdav_client_options, options)
    
    client = Client(options=options)
    client.verify = True
    
    yield client

@pytest.fixture()
def nextcloud_files(webdav_client: Client, temp_files: tuple[str, list[str]]) -> Generator[tuple[str, list], None, None]:
    """Uses WebDAV client to upload and delete files to/from Nextcloud.

    Args:
        webdav_client (Client): WebDAV client
        temp_files (tuple[str, list]): Tuple with temporary folder and list of files

    Yields:
        tuple[str, list]: Tuple with temporary folder and list of files
    """
    
    temp_folder: str = temp_files[0]
    files: list = temp_files[1]
    
    for file in files:
        steps_webdav.step_upload_file(client=webdav_client, path=file, input_file=f"{temp_folder}{os.sep}{file}")
    
    yield temp_files
    
    for file in files:
        steps_webdav.step_delete_file(client=webdav_client, path=file)

@pytest.fixture()
def temp_user(request: pytest.FixtureRequest, user_importer: UserImporter, udm_api: UdmApi, config: Config) -> Generator[Config.User, None, None]:
    """Creates a temporary user and deletes it after the test.

    Args:
        request (pytest.FixtureRequest): Current fixture request
        user_importer (UserImporter): User importer
        udm_api (UdmApi): UDM API
        config (Config): Current configuration

    Yields:
        Config.User: Temporary user
    """
    temp_user_mark = request.node.get_closest_marker("temp_user")
    custom_data = temp_user_mark.kwargs if temp_user_mark else {}
    
    user_data: dict = {
        "key": f"temp_user{config.generate_number_sequence()}",
        "firstname": "Temp",
        "lastname": f"User{config.generate_number_sequence()}",
        "username": f"tu{config.run_id}-{config.generate_number_sequence()}",
        "password": config.generate_password(),
        "email_domain": config.base_domain
    }
    user_data.update(custom_data)
    
    user: Config.User = Config.User(
        **user_data
    )
    
    if not user.skip_import:
        user = steps_admin_users.step_import_user(user, user_importer)
        
        user_import_grace_period: int = int(config.user_import_grace_period)
        if user_import_grace_period > 0:
            print("User import grace period")
            print(f"Grace period for user import: {user_import_grace_period}s")
            sleep(user_import_grace_period)
    
    yield user
    
    if not user.skip_import:
        steps_admin_users.step_delete_user(user.username, udm_api)

@pytest.fixture()
def temp_logged_in_page(browser: Browser, browser_context_args: dict, url_portal: str, temp_user: Config.User, config: Config) -> Generator[Page, None, None]:
    page: Page = browser.new_page(**browser_context_args)
    login_user(page=page, user=temp_user, url_portal=url_portal, config=config)
    
    yield page
    
    if page and not page.is_closed():
        page.close()

# Helper functions

def _get_user_context(browser: Browser, user: Config.User, config: Config, url_portal: str, browser_context_args: dict, browser_contexts: dict) -> BrowserContext:
    """Generate a user context from a user and store it in a dictionary for reuse.

    Args:
        browser (Browser): Current browser
        user (Config.User): User to generate context for
        config (Config): Current configuration
        url_portal (str): Portal URL
        browser_context_args (dict): Browser context arguments (for e.g. locale)
        browser_contexts (dict): Dictionary to store browser contexts for users and reuse them

    Returns:
        BrowserContext: Browser context for a user
    """
    
    allure.dynamic.parameter(f"{user.key}_username", user.username)
    allure.dynamic.parameter(f"{user.key}_password", user.password)
    
    context = browser_contexts.get(user.key, None)
    if context == None:
        context = browser.new_context(**browser_context_args)
        page = context.new_page()
        login_user(page, user, url_portal, config)
        browser_contexts[user.key] = context
        page.close()
    return context

@allure.step("Login user")
@utils.screenshot_step
def login_user(page: Page, user: Config.User, url_portal: str, config: Config):
    """Logs in a user.

    Args:
        page (Page): Page to execute steps on
        user (Config.User): User to log in
        url_portal (str): Portal URL
        config (Config): Current configuration
    """
    
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_fill_login(page=page, username=user.username, password=user.password)
    steps_login.step_click_login(page=page)
    if (user.is_admin and config.env_admin_2fa) or user.enable_2fa:
        steps_login.step_login_2fa(page=page, config=config, user=user)
    steps_login.step_check_login_successful(page=page)
    
@allure.step("Create WebDAV client options")
@utils.retry(trys=3, retry_timeout=3)
def _create_webdav_client_options(logged_in_context: BrowserContext, url_portal: str) -> dict:
    """Creates WebDAV client options for a user.

    Args:
        logged_in_context (BrowserContext): Browser context for a user
        url_portal (str): Portal URL

    Raises:
        Exception: Any exception that occurs during the process

    Returns:
        dict: WebDAV client options
    """
    page: Page = logged_in_context.new_page()
    app_name: str = f"app-{random.randint(0, 9999)}"

    try:
        steps_nextcloud.step_open_security_settings_page(page=page, url_portal=url_portal)
        steps_nextcloud.step_fill_app_password_name(page=page, name=app_name)
        steps_nextcloud.step_click_create_app_password(page=page)
        app_name: str = steps_nextcloud.step_get_app_name(page=page)
        app_password: str = steps_nextcloud.step_get_app_password(page=page)
        steps_nextcloud.step_open_page(page=page, url_portal=url_portal)
        steps_nextcloud.step_open_file_settings(page=page)
        webdav_url: str = steps_nextcloud.step_get_webdav_url(page=page)
    except Exception as exception:
        page.close()
        raise exception
    
    page.close()

    options = {
        "webdav_hostname": webdav_url,
        "webdav_login": app_name,
        "webdav_password": app_password
    }

    return options

