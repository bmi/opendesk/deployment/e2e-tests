# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from typing import Generator
import pytest
from tests.config.config import Config
from playwright.sync_api import Browser, BrowserContext, Page

from tests.steps import steps_opentalk

@pytest.fixture(scope="session")
def opentalk_url(config: Config):
    return config.opentalk_url

@pytest.fixture(scope="session")
def opentalk_username(config: Config):
    return config.opentalk_username

@pytest.fixture(scope="session")
def opentalk_firstname(config: Config):
    return config.opentalk_firstname

@pytest.fixture(scope="session")
def opentalk_lastname(config: Config):
    return config.opentalk_lastname

@pytest.fixture(scope="session")
def opentalk_email(config: Config):
    return config.opentalk_email

@pytest.fixture(scope="session")
def opentalk_password(config: Config):
    return config.opentalk_password

@pytest.fixture(scope="session")
def opentalk_context(browser: Browser, opentalk_url: str, opentalk_username: str, opentalk_password: str) -> Generator[BrowserContext, None, None]:
    """Create a new browser context with the opentalk user logged in.

    Args:
        browser (Browser): Current browser instance
        opentalk_url (str): OpenTalk URL
        opentalk_username (str): OpenTalk account username
        opentalk_password (str): OpenTalk account password

    Returns:
        BrowserContext: Browser context with the OpenTalk user logged in
    """
    
    page: Page = browser.new_page()
    __login_opentalk_user(page=page, opentalk_url=opentalk_url, opentalk_username=opentalk_username, opentalk_password=opentalk_password)
    state = page.context.storage_state()
    page.close()
    return state

@pytest.fixture()
def opentalk_page(browser: Browser, opentalk_context) -> Generator[Page, None, None]:
    """Create a new page with the OpenTalk user logged in.

    Args:
        browser (Browser): Current browser instance
        opentalk_context (_type_): OpenTalk user context

    Yields:
        Page: Page with the OpenTalk user logged in
    """
    
    page = browser.new_page(storage_state=opentalk_context)
    
    yield page
    
    if page and not page.is_closed():
        page.close()

# Helper functions

def __login_opentalk_user(page: Page, opentalk_url: str, opentalk_username: str, opentalk_password: str):
    """Login to OpenTalk with the given credentials.

    Args:
        page (Page): Page to use for login process
        opentalk_url (str): OpenTalk URL
        opentalk_username (str): OpenTalk account username
        opentalk_password (str): OpenTalk account password
    """
    
    steps_opentalk.step_open_page(page=page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_login_page(page=page)
    steps_opentalk.step_check_login_form(page=page)
    steps_opentalk.step_fill_login_form(page=page, username=opentalk_username, password=opentalk_password)
    steps_opentalk.step_click_login(page=page)
    steps_opentalk.step_check_opentalk_page(page=page)