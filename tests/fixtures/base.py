# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import imaplib
from pathlib import Path
import random
import shutil
import stat
import pytest
import dill as pickle

from tests.config.config import config_parser_init, Config
from typing import Any, Generator
from playwright.sync_api import Page, BrowserContext
import utils
import smtplib
from pages.portal.page import *
from imap_tools import MailBox
from pytest_dependency import depends
import pytest
from filelock import FileLock
import os
import xdist

def pytest_sessionstart(session: pytest.Session):
    """Runs on session start. Deletes tmp and config folders before all tests.
    The xdist_worker_id is checked to only delete the folders on the master node, which runs before all threads when running tests in parallel. 

    Args:
        session (pytest.Session): Current session object.
    """
    if xdist.get_xdist_worker_id(session) != "master":
        return
    
    nukedir("tmp")

def pytest_sessionfinish(session: pytest.Session):
    """Runs on session finish. Executes the cleanup function of the configuration.

    Args:
        session (pytest.Session): Current session object.
    """
    if xdist.get_xdist_worker_id(session) != "master":
        return
    
    config = _get_config(session=session)
    config.cleanup()

@pytest.hookimpl(wrapper=True)
def pytest_collection_modifyitems(session: pytest.Session, config: pytest.Config, items: list[pytest.Item]):
    """Modifies the items list to deselect tests with the env_setting marker if "--deselect-env-tests" is set.
    @pytest.hook_impl(wrapper=True): To make sure this runs after the pytest_collection_modifyitems of the other plugins.
    
    Args:
        session (pytest.Session): Current session object.
        config (pytest.Config): Current configuration object.
        items (list[pytest.Item]): List of collected test items.
    """
    
    yield
    
    custom_config = _get_config(session=session)
    
    if custom_config.deselect_env_tests:
        selected = list(items)
        deselected = []

        for test_item in items:
            if _check_deselect(item=test_item, config=custom_config):
                selected.remove(test_item)
                deselected.append(test_item)

        items[:] = selected
        config.hook.pytest_deselected(items=deselected)

@pytest.fixture(autouse=True)
def deselect_env_settings(request: pytest.FixtureRequest, config: Config):
    """Skips test if it is deselected by the env_setting marker.

    Args:
        request (pytest.FixtureRequest): Current fixture request.
        config (Config): Configuration object with environment settings.
    """
    
    if _check_deselect(item=request.node, config=config):
        pytest.skip("Environment settings deselected for this test.")

def _check_deselect(item: pytest.Item, config: Config) -> bool:
    """Checks if the test should be deselected based on the env_setting marker.

    Args:
        item (pytest.Item): Test item to check.
        config (Config): Configuration object containing environment settings.

    Returns:
        bool: True if it should be deselected/skipped
    """
    
    if not (markers := item.iter_markers("env_setting")):
        return False
    
    for marker in markers:
        if len(marker.args) < 2:
            continue
        
        setting: str = marker.args[0]
        value = marker.args[1]
        
        actual_value = getattr(config, setting)
        
        if callable(value):
            if value(actual_value):
                continue
        elif actual_value == value:
            continue
        
        return True

@pytest.fixture(scope="session")
def config(request: pytest.FixtureRequest) -> Generator[Config, None, None]:
    """Fixture to create a Config object for the session.

    Args:
        request (pytest.FixtureRequest): Current fixture request.

    Yields:
        Generator[Config, None, None]: Configuration object.
    """
    
    config = _get_config(session=request.session)
    config.register_env_args()
    
    yield config

def _get_config(session: pytest.Session) -> Config:
    """Gets the Config object from the file or creates a new one.

    Args:
        session (pytest.Session): Current session object.

    Returns:
        Config: Configuration object.
    """
    
    fn = Path("tmp") / "config.bin"
    with FileLock(str(fn) + ".lock"):
        if fn.is_file():
            with open(fn, "rb") as file:
                config = pickle.loads(file.read())
        else:
            config = Config(session)
            with open(fn, "wb") as file:
                file.write(pickle.dumps(config))
    
    return config

@pytest.fixture(autouse=True)
def dependency_or(request: pytest.FixtureRequest, pytestconfig: pytest.Config):
    """Skips test if none of the tests in dependency_or list pass.

    Args:
        request (pytest.FixtureRequest): Current fixture request
        pytestconfig (pytest.Config): _description_
    """
    
    if not (marker := request.node.get_closest_marker('dependency_or')):
        return
    
    # Checks if "dependency" plugin is disabled. If yes: no dependency check.
    if "no:dependency" in (pytestconfig.invocation_params.args or []):
        return
    if len(marker.args) == 0:
        return
    
    dependency_list: list[str] = marker.args[0]
    if not dependency_list:
        return
    
    item = request.node
    for o in dependency_list:
        try:
            depends(request, [o])
        except pytest.skip.Exception:
            continue
        else:
            return
    pytest.skip("%s depends on any of %s" % (item.name, ", ".join(dependency_list)))

@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    if call.when == 'call':
        rep = outcome.get_result()
        try:
            config: Config = item._request.getfixturevalue("config")
            do_screenshot: str = config.screenshot_test
            if do_screenshot == "yes" or (do_screenshot == "on_success" and rep.success) or (do_screenshot == "on_fail" and rep.failed):
                    contexts: set = set()
                    for fixture in item.fixturenames:
                        obj = item.funcargs[fixture]
                        if isinstance(obj, Page):
                            contexts.add(obj.context)
                        elif isinstance(obj, BrowserContext):
                            contexts.add(obj)
                    for context in contexts:
                        utils.screenshot_context(context, "after:TEST")
        except Exception as e:
            print('Error while creating screenshot: {}'.format(e))
    
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)

@pytest.fixture(scope="session")
def test_start_timestamp(config: Config):
    return config.test_start_timestamp

@pytest.fixture(scope="session")
def autotest_mail(config: Config) -> str:
    return config.autotest_mail

@pytest.fixture(scope="session")
def autotest_server(config: Config) -> str:
    return config.autotest_server

@pytest.fixture(scope="session")
def autotest_imap_port(config: Config) -> str:
    return config.autotest_imap_port

@pytest.fixture(scope="session")
def autotest_password(config: Config) -> str:
    return config.autotest_password

@pytest.fixture(scope="session")
def autotest_element_username(config: Config) -> str:
    return config.autotest_element_username

@pytest.fixture(scope="session")
def demo_files_folder(config: Config) -> str:
    return config.demo_files_folder

@pytest.fixture(scope="session")
def run_id(config: Config) -> str:
    return config.run_id

@pytest.fixture(scope="session")
def locale(config: Config) -> str:
    return config.locale

@pytest.fixture(scope="session")
def mailbox(config: Config) -> Generator[MailBox, None, None]:
    # Fix for imaplib bug
    imaplib.Untagged_status = imaplib.re.compile(br'\*[ ]{1,2}(?P<data>\d+) (?P<type>[A-Z-]+)( (?P<data2>.*))?')
    mail_box: MailBox = MailBox(host=config.autotest_server, port=config.autotest_imap_port).login(config.autotest_mail, config.autotest_password)
    
    yield mail_box
    
    mail_box.client.close()

@pytest.fixture(scope="session")
def smtp(config: Config) -> Generator[smtplib.SMTP_SSL, None, None]:
    smtp = smtplib.SMTP_SSL(host=config.autotest_server, port=config.autotest_smtp_port)
    smtp.login(user=config.autotest_mail, password=config.autotest_password)
    
    yield smtp
    
    smtp.quit()
    smtp.close()

@pytest.fixture(scope="session")
def temp_folder(config: Config) -> str:
    return f"{config.temp_folder}{os.sep}{random.randint(0, 9999)}"

@pytest.fixture()
def test_temp_number(config: Config) -> int:
    used_numbers: list[int] = config.load("used_temp_numbers") or []
    numbers: list[int] = list(range(1000))
    
    for number in used_numbers:
        if number in numbers: numbers.remove(number)
    
    num = random.choice(numbers)
    
    used_numbers.append(num)
    config.save("used_temp_numbers", used_numbers)
    
    return num

@pytest.fixture()
def files_dict(request: pytest.FixtureRequest) -> dict:
    if not (marker := request.node.get_closest_marker('files')):
        return {}
    if len(marker.args) != 1:
        return {}
    
    files_dict: dict = marker.args[0]
    if not isinstance(files_dict, dict):
        return {}
    
    return files_dict

@pytest.fixture()
def temp_files(run_id: str, demo_files_folder: str, temp_folder: str, files_dict: dict, test_temp_number: int) -> Generator[tuple[str, list[str]], None, None]:
    files: list = []
    
    for file_type, amount in files_dict.items():
        files.extend(_generate_files(file_type, amount, run_id, demo_files_folder, temp_folder, test_temp_number))
    
    yield (temp_folder, files)
    
    nukedir(temp_folder)

@pytest.fixture(scope="session")
def browser_context_args(
        browser_context_args: dict,
        config: Config
) -> dict:
    browser_context_args["locale"] = config.locale
    browser_context_args["viewport"] = {"width": 1536, "height": 776}
    return browser_context_args

@pytest.fixture(scope="session")
def browser_type_launch_args(
        browser_type_launch_args: Any
) -> dict: 
    browser_type_launch_args = browser_type_launch_args or {}
    launch_settings = {
        "args": [
            "--use-fake-device-for-media-stream",
            "--auto-accept-camera-and-microphone-capture"
        ],
        "firefox_user_prefs": 
            {
            "permissions.default.microphone": 1,
            "permissions.default.camera": 1,
            "media.navigator.streams.fake": True,
            "media.navigator.permission.disabled": False,
            "media.gstreamer.enabled": False,
        }
    }
    return browser_type_launch_args | launch_settings

# Helper functions
def _generate_files(file_type: str | Pattern, amount: int, run_id: str, demo_files_folder: str, temp_folder: str, test_temp_number: int) -> tuple[str, list[str]]:
    """Generates files for testcases.
    
    Args:
        file_type (str | Pattern): Which file type to generate file from. If str it will be used as suffix, if Pattern it will be used as regex.
        amount (int): How many files to generate.
        run_id (str): Current run id.
        demo_files_folder (str): Folder where test files are located.
        temp_folder (str): Folder where files will be copied to.

    Returns:
        tuple[str, list[str]]: Returns the temp folder and a list of generated files.
    """
    possible_files: list = [file for file in os.listdir(demo_files_folder) if os.path.isfile(f"{demo_files_folder}{os.sep}{file}")]
    
    if isinstance(file_type, str):
        possible_files = list(filter(lambda s: s.endswith(file_type), possible_files))
    else:
        possible_files = list(filter(lambda s: re.findall(file_type, s), possible_files))
    
    if len(possible_files) == 0:
        print(f"No files found for generation of '{file_type}' files.")
        return []
    
    os.makedirs(temp_folder, exist_ok=True)
    
    files: list = []
    for i in range(amount):
        random_file: str = random.choice(possible_files)
        filename: str = f"{run_id}-{test_temp_number}-{i}-{random_file}"
        demo_file: str = f"{demo_files_folder}{os.sep}{random_file}"
        temp_file: str = f"{temp_folder}{os.sep}{filename}"
        shutil.copyfile(demo_file, temp_file)
        os.utime(temp_file, (os.path.getctime(demo_file), os.path.getmtime(demo_file)))
        
        files.append(filename)
    
    return files

def nukedir(dir: str):
    """Recursively deletes a directory and all its content.

    Args:
        dir (str): Path of the directory to delete.
    """
    if not os.path.exists(dir): return
    if dir[-1] == os.sep: dir = dir[:-1]
    files = os.listdir(dir)
    for file in files:
        if file == '.' or file == '..': continue
        path = dir + os.sep + file
        if os.path.isdir(path):
            nukedir(path)
        else:
            os.chmod(path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)  
            os.unlink(path)
    os.rmdir(dir)