# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_login
import allure
import pytest

# Integration tests for creating a new user:

@allure.epic("Smoke")
@allure.feature("Login", "Portal")
@allure.story("Login")
@allure.title("Login: Logging in new user")
@pytest.mark.dependency(name="new_user_login", depends=["new_user_invitation_mail"], scope="session")
@pytest.mark.xdist_group("new_user-group")
def test_check_user_login_and_components(page: Page, url_portal: str, new_user: Config.User):
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_check_login_page(page=page)
    steps_login.step_fill_login(page=page, username=new_user.username, password=new_user.password)
    steps_login.step_click_login(page=page)
    steps_login.step_check_login_successful(page=page)