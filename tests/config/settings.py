# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from datetime import datetime
import os
import re
import tests.conftest as conftest
from .config import Config
from .utils import Setting, EnvironmentSetting, UserImporter, UdmApi

def create_temp_folder(config: Config):
    os.makedirs("tmp", exist_ok=True)
    return "tmp"

def cleanup(config: Config):
    conftest.nukedir(config.temp_folder)

test_users: list[Config.User] = []

def register_test_users(config: Config):
    user: Config.User = Config.User(key="user", firstname="User",
                           lastname=f"Autotest{config.generate_number_sequence()}",
                           username=f"u{config.run_id}",
                           password=config.generate_password(),
                           email_domain=config.base_domain,
                           create_admin=True
                           )
    test_users.append(user)
    test_users.append(user.to_admin())
    test_users.append(Config.User(key="new_user", firstname="NewUser",
                           lastname=f"Autotest{config.generate_number_sequence()}",
                           username=f"n{config.run_id}",
                           password=config.generate_password(),
                           email_domain=config.base_domain,
                           skip_import=True))
    test_users.append(Config.User(key="user_2fa", firstname="user_2fa",
                           lastname=f"Autotest{config.generate_number_sequence()}",
                           username=f"u2fa{config.run_id}",
                           password=config.generate_password(),
                           email_domain=config.base_domain).set("enable_2fa", True))
    test_users.append(Config.User(key="guest", firstname="Guest", lastname=f"Autotest{config.generate_number_sequence()}", username=f"g{config.run_id}", password=config.generate_password(), email_domain=config.base_domain, disable_components=True))
    test_users.append(Config.User(key="component_admin", firstname="ComponentAdmin", lastname=f"Autotest{config.generate_number_sequence()}", username=f"ca{config.run_id}", password=config.generate_password(), email_domain=config.base_domain, component_admin=True))

settings: list[Setting] = [
    # Global
    Setting(name="config_temp_folder", cli_name="--config-temp-folder"),
    Setting(name="config_file", cli_name="--config"),
    Setting(name="language", cli_name="--language", ini_section="Environment", ini_key="language", choices=["de", "en"], default="de"),
    Setting(name="locale", default=(lambda config: 
        {
            "de": "de-DE",
            "en": "en-US"
        }[config.language]
        )),
    Setting(name="autotest_mail", cli_name="--autotest-mail", ini_section="Autotest", ini_key="mail", default="zendis_automate@open-de.sk"),
    Setting(name="autotest_server", cli_name="--autotest-server", ini_section="Autotest", ini_key="server", default="mail.brained.io"),
    Setting(name="autotest_imap_port", cli_name="--autotest-imap-port", ini_section="Autotest", ini_key="imap_port", default="993"),
    Setting(name="autotest_smtp_port", cli_name="--autotest-smtp-port", ini_section="Autotest", ini_key="smtp_port", default="465"),
    Setting(name="autotest_password", cli_name="--autotest-password", ini_section="Autotest", ini_key="password"),
    Setting(name="autotest_element_username", cli_name="--autotest-element-username", ini_section="Autotest", ini_key="element_username", default="@zendis_automate:matrix.org"),
    Setting(name="screenshot_test", cli_name="--screenshot-test", ini_section="Screenshot", ini_key="test", choices=["yes", "no", "on_success", "on_fail"], default="yes"),
    Setting(name="screenshot_before_step", cli_name="--screenshot-before-step", ini_section="Screenshot", ini_key="before_step", choices=["yes", "no"], default="yes"),
    Setting(name="screenshot_after_step", cli_name="--screenshot-after-step", ini_section="Screenshot", ini_key="after_step", choices=["yes", "no"], default="yes"),
    Setting(name="screenshot_redirect_step", cli_name="--screenshot-redirect-step", ini_section="Screenshot", ini_key="redirect_step", choices=["yes", "no"], default="yes"),
    Setting(name="demo_files_folder", cli_name="--demo-files-folder", ini_section="Demo", ini_key="folder", default="demo_files", sanitize=os.path.normpath, cleanup=lambda config: conftest.nukedir(f"{config.demo_files_folder}/temp")),
    Setting(name="test_start_timestamp", default=datetime.now()),
    Setting(name="test_start", default=(lambda config: config.test_start_timestamp.strftime("%Y-%m-%d_%H:%M:%S"))),
    Setting(name="timestamp", default=(lambda config: config.test_start_timestamp.strftime("%Y%m%d%H%M"))),
    Setting(name="run_id", default=(lambda config: f"{config.timestamp}-{config.generate_number_sequence()}")),
    Setting(name="temp_folder", default=create_temp_folder, cleanup=cleanup),
    Setting(name="test_export_folder", cli_name="--test-export-folder", default=None),
    
    # OpenDesk
    Setting(name="url_portal", cli_name="--url-portal", ini_section="Environment", ini_key="url", default="https://portal.nightly.opendesk.run/"),
    Setting(name="base_domain", default=(lambda config:
        re.findall(r"https?://portal\.([a-zA-Z-_0-9.]*)/?", config.url_portal)[0]
    )),
    Setting(name="operator", cli_name="--operator", ini_section="Environment", ini_key="operator", default="zendis"),
    Setting(name="cluster", cli_name="--cluster", ini_section="Environment", ini_key="cluster", default="run"),
    Setting(name="namespace", cli_name="--namespace", ini_section="Environment", ini_key="namespace", default="nightly"),
    Setting(name="enterprise", cli_name="--enterprise", ini_section="Environment", ini_key="enterprise", default="false"),
    Setting(name="profile", cli_name="--profile", ini_section="Environment", ini_key="profile", choices=["Default", "Custom", "Namespace"], default="Namespace"),
    Setting(name="gitlab_functional_yaml", cli_name="--gitlab-functional-yaml", ini_section="Environment", ini_key="gitlab_functional_yaml", default="https://gitlab.opencode.de/api/v4/projects/1317/repository/files/helmfile%2Fenvironments%2Fdefault%2Ffunctional.yaml.gotmpl?ref=develop"),
    Setting(name="gitlab_env_namespace_template", cli_name="--gitlab-env-namespace-template", ini_section="Environment", ini_key="gitlab_env_namespace_template", default="https://gitlab.opencode.de/api/v4/projects/1564/repository/files/{file}?ref=main"),
    Setting(name="gitlab_env_namespace_file", cli_name="--gitlab-env-namespace-file", ini_section="Environment", ini_key="gitlab_env_namespace_file", default="environments%2F{operator}%2F{cluster}%2F{namespace}.yaml.gotmpl"),
    Setting(name="gitlab_env_certificate_file", cli_name="--gitlab-env-certificate-file", ini_section="Environment", ini_key="gitlab_env_certificate_file", default="environments%2F{operator}%2F{cluster}%2F{cluster}-ca.crt"),
    Setting(name="gitlab_default_env_namespace", cli_name="--gitlab-default-env-namespace", ini_section="Environment", ini_key="gitlab_default_env_namespace", default="values"),
    Setting(name="gitlab_token", cli_name="--gitlab-token", ini_section="Environment", ini_key="gitlab_token"),
    Setting(name="udm_api_username", cli_name="--udm-api-username", ini_section="Admin", ini_key="username", default="default.admin"),
    Setting(name="udm_api_password", cli_name="--udm-api-password", ini_section="Admin", ini_key="password"),
    Setting(name="pod_restart_threshold_file", cli_name="--pod-restart-threshold-file", default="pod_restart_threshold.csv"),
    Setting(name="user_import_repository_url", cli_name="--user-import-repo-url", ini_section="Environment", ini_key="user_import_repository_url", default="https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/user-import.git"),
    Setting(name="user_import_grace_period", cli_name="--user-import-grace-period", default="0"),
    Setting(name="self_signed_certificate", cli_name="--self-signed-certificate", default=None),
    Setting(name="disable_population", cli_name="--disable-population", default=False, is_boolean=True),
    Setting(name="deselect_env_tests", cli_name="--deselect-env-tests", default=False, is_boolean=True),
    
    # OpenTalk
    Setting(name="opentalk_url", cli_name="--opentalk-url", ini_section="OpenTalk", ini_key="url", default=None),
    Setting(name="opentalk_firstname", cli_name="--opentalk-firstname", ini_section="OpenTalk", ini_key="firstname", default=None),
    Setting(name="opentalk_lastname", cli_name="--opentalk-lastname", ini_section="OpenTalk", ini_key="lastname", default=None),
    Setting(name="opentalk_username", cli_name="--opentalk-username", ini_section="OpenTalk", ini_key="username", default=None),
    Setting(name="opentalk_email", cli_name="--opentalk-email", ini_section="OpenTalk", ini_key="email", default=None),
    Setting(name="opentalk_password", cli_name="--opentalk-password", ini_section="OpenTalk", ini_key="password", default=None),
]

environment_settings: list[EnvironmentSetting] = [
    EnvironmentSetting(
        name="env_admin_2fa",
        cli_name="--env-admin-2fa",
        default_value=False,
        env_path="functional.authentication.twoFactor.groups",
        expected_value=lambda groups: "Domain Admins" in groups
    ),
    EnvironmentSetting(
        name="env_external_sharing",
        cli_name="--env-external-sharing",
        default_value=False,
        env_path="functional.filestore.sharing.external.enabled",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_self_signed_ca",
        cli_name="--env-self-signed-ca",
        default_value=False,
        env_path="certificate.selfSigned",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_enforce_login",
        cli_name="--env-enforce-login",
        default_value=False,
        env_path="functional.portal.enforceLogin",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_send_password_mail",
        cli_name="--env-send-password-mail",
        default_value=False,
        env_path="functional.filestore.sharing.external.sendPasswordMail",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_enforce_password",
        cli_name="--env-enforce-password",
        default_value=False,
        env_path="functional.filestore.sharing.external.enforcePasswords",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_expiry_active_default",
        cli_name="--env-expiry-active-default",
        default_value=False,
        env_path="functional.filestore.sharing.external.expiry.activeByDefault",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_expiry_enforced",
        cli_name="--env-expiry-enforced",
        default_value=False,
        env_path="functional.filestore.sharing.external.expiry.enforced",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_expiry_default_days",
        cli_name="--env-expiry-default-days",
        default_value=None,
        env_path="functional.filestore.sharing.external.expiry.defaultDays"
    ),
    EnvironmentSetting(
        name="env_deployment_timestamp",
        cli_name="--env-deployment-timestamp",
        default_value=False,
        env_path="functional.admin.portal.deploymentTimestamp.enabled",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_allow_changing_displayname",
        cli_name="--env-allow-changing-displayname",
        default_value=True,
        env_path="functional.chat.matrix.profile.allowUsersToUpdateDisplayname",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_immutable_matrixid_localpart",
        cli_name="--env-immutable-matrixid-localpart",
        default_value=False,
        env_path="functional.chat.matrix.profile.useImmutableIdentifierForLocalpart",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_federation_enabled",
        cli_name="--env-federation-enabled",
        default_value=False,
        env_path="functional.chat.matrix.federation.enabled",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_notes_enabled",
        cli_name="--env-notes-enabled",
        default_value=False,
        env_path="apps.notes.enabled",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_udm_api_available",
        cli_name="--env-udm-api-available",
        default_value=True,
        env_path="functional.externalServices.nubus.udmRestApi.enabled",
        expected_value=True
    ),
    EnvironmentSetting(
        name="env_new_device_notification",
        cli_name="--env-new-device-notification",
        default_value=True,
        env_path="functional.authentication.newDeviceLoginNotification.enabled",
        expected_value=True
    ),
]