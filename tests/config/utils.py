# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import argparse
import random
import re
from typing import Any, Callable, Literal
from odsgenerator import odsgenerator
import pytest
import config.config as config_file
import os
import subprocess
import base64
import requests

class Setting:
    
    def __init__(self, name: str, cli_name: str | None = None, ini_section: str | None = None, ini_key: str | None = None, default: Any | Callable[[config_file.Config], str] | None = None, sanitize: Any | Callable[[Any], Any] | None = None, cleanup: Callable[[Any], Any] | None = None, choices: list[str] | None = None, required: bool = False, is_boolean: bool = False) -> None:
        self.name = name
        self.cli_name = cli_name
        self.ini_section = ini_section
        self.ini_key = ini_key
        self.default = default
        self.sanitize = sanitize
        self.cleanup = cleanup
        self.choices = choices
        self.required = required
        self.is_boolean = is_boolean

    def set_config_option(self, parser: pytest.Parser):
        if not self.cli_name:
            return
        if self.is_boolean:
            parser.addoption(self.cli_name, action=argparse.BooleanOptionalAction, dest=self.name)
        else:
            parser.addoption(self.cli_name, action="store", dest=self.name, choices=self.choices)

    def get_default(self, config: config_file.Config):
        if self.default == None:
            return None
        if callable(self.default):
            return self.default(config)
        return self.default

    def get_cli_value(self, config_options):
        return getattr(config_options, self.name, None)

    def get_ini_value(self, config_parser):
        try:
            return config_parser[self.ini_section][self.ini_key]
        except:
            return None

    def do_cleanup(self, config: config_file.Config):
        if self.cleanup:
            self.cleanup(config)

class EnvironmentSetting:
    
    def __init__(self, name: str, cli_name: str, default_value: bool, env_path: str, expected_value: Any | Callable[[Any], bool] | None = None) -> None:
        self.name = name
        self.cli_name = cli_name
        self.default_value = default_value
        self.env_path = env_path
        self.expected_value = expected_value
    
    def set_config_option(self, parser: pytest.Parser):
        if not self.cli_name:
            return
        parser.addoption(self.cli_name, action="store_true", dest=self.name)
    
    def get_default(self):
        return self.default_value

    def get_cli_value(self, config_options):
        return getattr(config_options, self.name, None)

    def get_ini_value(self, config_parser):
        try:
            return config_parser["Environment"][self.name]
        except:
            return None
    
    def get_namespace_value(self, namespace_profile: dict) -> bool:
        path: list[str] = self.env_path.split(".")
        
        current_section: dict = namespace_profile.copy()
        for key in path[:-1]:
            current_section = current_section.get(key, {})
        
        value = current_section.get(path[-1], None)
        
        if not self.expected_value:
            return value

        if value and callable(self.expected_value):
            return self.expected_value(value)
        else:
            return value == self.expected_value

class UserImporter:
    
    @staticmethod
    def from_config(config: config_file.Config) -> "UserImporter":
        return UserImporter(user_import_repository_url=config.user_import_repository_url, base_domain=config.base_domain, autotest_mail=config.autotest_mail, udm_api_username=config.udm_api_username, udm_api_password=config.udm_api_password, namespace=config.namespace)
    
    def __init__(self, user_import_repository_url: str, base_domain: str, autotest_mail: str, udm_api_username: str, udm_api_password: str, namespace: str) -> None:
        self.user_import_repository_url: str = user_import_repository_url
        self.base_domain: str = base_domain
        self.autotest_mail: str = autotest_mail
        self.udm_api_username: str = udm_api_username
        self.udm_api_password: str = udm_api_password
        self.namespace: str = namespace 
        self.localhost_port: int | None = None
        
        self.load_user_import_repository()
    
    def use_localhost_port(self, port: int):
        self.localhost_port = port
    
    def get_user_importer_path(self) -> str:
        return os.environ["user_importer_path"] if "user_importer_path" in os.environ.keys() else "git_clone/user-import"
    
    def load_user_import_repository(self):
        path: str = self.get_user_importer_path()
        
        if not path:
            raise Exception("Cannot find git repository path.")
        
        if not os.path.exists(path) or len(os.listdir(path)) == 0:
            print("Cloning user import repository...")
            os.system(f"git clone {self.user_import_repository_url} {path}")
            
            if not os.path.exists(path) or len(os.listdir(path)) == 0:
                raise Exception("Error when cloning user import repository.")
        
        requirements_path: str = f"{os.path.normpath(path)}{os.sep}requirements.txt"
        os.system(f"pip install -r {requirements_path}")
    
    def _import_user(self, user: config_file.Config.User):
        path: str = self.get_user_importer_path()
        if not path or not os.path.exists(path):
            raise Exception("Cannot find git repository path.")
        
        groups = []
        if user.enable_2fa:
            groups.append("2FA Testing")
        
        ods_bytes = odsgenerator.ods_bytes([[["Vorname", "Nachname", "Externe E-Mail", "Username", "Passwort", "LDAP-Gruppen"], 
                                     [user.firstname, user.lastname, self.autotest_mail, user.username, user.password, ";".join(groups)]]])
        
        random_number: int = random.randint(1000, 9999)
        filename: str = f"tmp{os.sep}import_{random_number}.ods"
        output_filename: str = f"tmp{os.sep}import_output_{random_number}.csv"
        
        with open(filename, "wb") as file:
            file.write(ods_bytes)
        
        if os.path.exists(output_filename):
            os.remove(output_filename)
        
        args = [
            f"--import_filename {filename}",
            f"--import_domain {self.base_domain}",
            f"--udm_api_username {self.udm_api_username}",
            f"--udm_api_password {self.udm_api_password}",
            f"--component_disable_groupware {user.disable_components}",
            f"--component_disable_fileshare {user.disable_components}",
            f"--component_disable_projectmanagement {user.disable_components}",
            f"--component_disable_knowledgemanagement {user.disable_components}",
            f"--component_disable_livecollaboration {user.disable_components}",
            f"--component_disable_videoconference {user.disable_components}",
            f"--admin_enable_fileshare {user.component_admin}",
            f"--admin_enable_projectmanagement {user.component_admin}",
            f"--admin_enable_knowledgemanagement {user.component_admin}",
            f"--output_accounts_filename {output_filename}"
        ]
        
        if user.create_admin:
            args.append("--create_admin_accounts True")
        
        if self.localhost_port is not None:
            args.append("--localhost_port " + str(self.localhost_port))
            
        arg_string: str = " ".join(args)
        os.system(f"python {path}{os.sep}user_import_udm_rest_api.py {arg_string}")
        os.remove(filename)
        
        if os.path.exists(output_filename):    
            with open(output_filename) as output:
                content = output.read()
                if m := re.search(f"{user.username}\t(.*)", content):
                    user.password = m.groups()[0]
                else:
                    user = None
                output.close()
            os.remove(output_filename)
        
        return user
    
    def _import_user_forwarded_port(self, user: config_file.Config.User):
        with PortForwarding(self.namespace, self.localhost_port):
            return self._import_user(user)

    def import_user(self, user: config_file.Config.User):
        if self.localhost_port is not None and self.namespace is not None:
            return self._import_user_forwarded_port(user)
        return self._import_user(user)
    
class UdmApi:
    
    @staticmethod
    def from_config(config: config_file.Config) -> "UdmApi":
        return UdmApi(udm_api_username=config.udm_api_username, udm_api_password=config.udm_api_password, base_domain=config.base_domain, namespace=config.namespace)
    
    def __init__(self, udm_api_username: str, udm_api_password: str, base_domain: str, namespace: str) -> None:
        self.udm_api_username: str = udm_api_username
        self.udm_api_password: str = udm_api_password
        self.base_domain: str = base_domain
        self.namespace: str = namespace
        
        self.local_port: int | None = None
        
        basic_auth: str = "Basic " + base64.b64encode(f"{self.udm_api_username}:{self.udm_api_password}".encode("utf-8")).decode("ascii")
        
        self.headers: dict = {
            "Authorization": basic_auth,
            "Accept": "application/json",
        }
        
        self.base_url = f"https://portal.{self.base_domain}/univention"
    
    def use_localhost_port(self, port: int):
        self.local_port = port
        self.base_url = f"http://localhost:{port}"
    
    def _update_user(self, username: str, method: str = Literal["get", "put", "delete"], properties: dict | None = None):
        dn: str = f"uid={username},cn=users,dc=swp-ldap,dc=internal"
        
        user_url = f"{self.base_url}/udm/users/user/{dn}"
        
        data: dict | None = None
        if properties:
            data = {
                "properties": properties,
                "position": "cn=users,dc=swp-ldap,dc=internal"
            }
        
        response = requests.request(method=method, url=user_url, headers=self.headers, json=data)

        if response.status_code >= 400:
            raise Exception(f"Failed to update user data: {response.text}")

        try:
            return response.json()
        except:
            return response.text

    def _update_user_forwarded_port(self, username: str, method: Literal["get", "post", "put", "delete"] = "get", properties: dict | None = None):
        with PortForwarding(self.namespace, self.local_port):
            return self._update_user(username=username, method=method, properties=properties)
    
    def update_user(self, username: str, method: Literal["get","post", "put", "delete"] = "get", properties: dict | None = None):
        if self.local_port is not None:
            return self._update_user_forwarded_port(username=username, method=method, properties=properties)
        return self._update_user(username=username, method=method, properties=properties)
    
    def add_user_to_group(self, username: str, group: str):
        properties: dict = {
            "groups": [
                f"cn={group},cn=groups,dc=swp-ldap,dc=internal"
            ]
        }
        
        self.update_user(username, "put", properties)
    
    def enable_password_change_on_next_login(self, username: str):
        properties: dict = {
            "pwdChangeNextLogin": True
        }
        
        self.update_user(username, "put", properties)
    
    def set_user_password(self, username: str, password: str):
        properties: dict = {
            "password": password
        }
        
        self.update_user(username, "put", properties)
    
    def delete_user(self, username: str):
        self.update_user(username, "delete")

class PortForwarding:
    
    def __init__(self, namespace: str, port: int) -> None:
        self.namespace = namespace
        self.port = port
        self.subprocess = None
    
    def __enter__(self):
        self.subprocess = subprocess.Popen(["kubectl", "-n", self.namespace, "port-forward", f"svc/ums-udm-rest-api", f"{self.port}:9979"], stdout=subprocess.PIPE)
        self.subprocess.stdout.readline()
        print("Port forwarding started.")
    
    def __exit__(self, exc_type, exc_value, traceback):
        if self.subprocess and self.subprocess.poll() is None:
            self.subprocess.terminate()
            print("Terminated port forwarding.")